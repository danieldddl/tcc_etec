USE bdNextSys;

CREATE TABLE tbUsuario(
	codUsuario INT AUTO_INCREMENT 
	, nomeUsuario VARCHAR (100) NOT NULL
	, sobrenomeUsuario VARCHAR (100) 
	, apelidoUsuario VARCHAR (100)
	, loginUsuario VARCHAR (16) NOT NULL
	, senhaUsuario VARCHAR (16) NOT NULL 
	, primeiraVezLogado BOOL 
	, PRIMARY KEY (`codUsuario`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;

CREATE TABLE tbFase (
	codFase INT AUTO_INCREMENT 
	, descFase INT NOT NULL
	, numEstrelas INT NOT NULL
	, numEstrelasCon INT NOT NULL
	, numEstrelasReq INT NOT NULL
	, codUsuario INT NOT NULL
	, PRIMARY KEY (`codFase`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
ALTER TABLE tbFase
	ADD CONSTRAINT FK_tbFase
	FOREIGN KEY (codUsuario) REFERENCES tbUsuario (codUsuario)
	ON UPDATE CASCADE
	ON DELETE CASCADE;

CREATE TABLE tbJogo (
	codJogo INT AUTO_INCREMENT 
	, codFase INT NOT NULL
	, numIdentificador INT NOT NULL
	, isConquistado INT NOT NULL 
	, PRIMARY KEY (`codJogo`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 AUTO_INCREMENT = 1;
ALTER TABLE tbJogo
	ADD CONSTRAINT FK_tbJogo
	FOREIGN KEY (codFase) REFERENCES tbFase (codFase)
	ON UPDATE CASCADE
	ON DELETE CASCADE;

