package Classes;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

public class CaixaMensagem extends JDialog implements ActionListener{

    private JButton btnConfirmar;
    private JButton btnAlternativaUm;
    private JButton btnAlternativaDois;
    private JLabel lblTextoMensagem;
    
    private JButton btnFechar;
    
    private int escolhido = 0;
    private boolean acao = true;
    
    private Icon iconBotaoConfirmar, iconBotaoConfirmarPressed;
    
    private Icon iconOpcaoSim, iconOpcaoSimPressed;
    private Icon iconOpcaoNao, iconOpcaoNaoPressed;
    
    private Icon iconBotaoFechar, iconBotaoFecharPressed;
    
    public int getEscolhido(){
        return escolhido;
    }
    
    public void carregaImagens (){
        //imagens
        try {
            Image imageBotaoConfirmar = ImageIO.read(getClass().getResource(
                "/arquivosComplementares/imagens/menus/botoes/botao_confirmar.png"));
            iconBotaoConfirmar = new ImageIcon(
                    imageBotaoConfirmar.getScaledInstance(70, 70, Image.SCALE_DEFAULT));
            
            Image imageBotaoConfirmarPressed = ImageIO.read(getClass().getResource(
                "/arquivosComplementares/imagens/menus/botoes/botao_confirmar_pressed.png"));
            iconBotaoConfirmarPressed = new ImageIcon(
                    imageBotaoConfirmarPressed.getScaledInstance(70, 70, Image.SCALE_DEFAULT));
            
            Image imageOpcaoSim =  ImageIO.read(getClass().getResource(
                "/arquivosComplementares/imagens/menus/botoes/botao_confirmarSim.png"));
            iconOpcaoSim = new ImageIcon (
                imageOpcaoSim.getScaledInstance(138, 52, Image.SCALE_DEFAULT));
            
            Image imageOpcaoSimPressed =  ImageIO.read(getClass().getResource(
                "/arquivosComplementares/imagens/menus/botoes/botao_confirmarSim_pressed.png"));
            iconOpcaoSimPressed = new ImageIcon (
                imageOpcaoSimPressed.getScaledInstance(138, 52, Image.SCALE_DEFAULT));
            
            Image imageOpcaoNao =  ImageIO.read(getClass().getResource(
                "/arquivosComplementares/imagens/menus/botoes/botao_confirmarNao.png"));
            iconOpcaoNao = new ImageIcon (
                imageOpcaoNao.getScaledInstance(138, 52, Image.SCALE_DEFAULT));
            
            Image imageOpcaoNaoPressed =  ImageIO.read(getClass().getResource(
                "/arquivosComplementares/imagens/menus/botoes/botao_confirmarNao_pressed.png"));
            iconOpcaoNaoPressed = new ImageIcon (
                imageOpcaoNaoPressed.getScaledInstance(138, 52, Image.SCALE_DEFAULT));
            
            Image imageFechar = ImageIO.read(getClass().getResource(
                "/arquivosComplementares/imagens/menus/botoes/close.png"));
            iconBotaoFechar = new ImageIcon(
                    imageFechar.getScaledInstance(30, 30, Image.SCALE_DEFAULT));
            
            Image imageFecharPressed = ImageIO.read(getClass().getResource(
                "/arquivosComplementares/imagens/menus/botoes/close-pressed.png"));
            iconBotaoFecharPressed = new ImageIcon(
                    imageFecharPressed.getScaledInstance(30, 30, Image.SCALE_DEFAULT));
            
        } catch (Exception e){
            
        }
    }
    
    public void mostrarMensagem (String mensagemTexto){

        carregaImagens();
        
        lblTextoMensagem = new JLabel(mensagemTexto);
        lblTextoMensagem.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 18));
        lblTextoMensagem.setForeground(new Color(1,133,170));
        lblTextoMensagem.setSize(lblTextoMensagem.getPreferredSize()); 
        
        btnConfirmar = new JButton ();
        btnConfirmar.setBorder(null); 
        btnConfirmar.setContentAreaFilled(false);
        btnConfirmar.setIcon(iconBotaoConfirmar);
        btnConfirmar.setPressedIcon(iconBotaoConfirmarPressed);
        btnConfirmar.setSize(btnConfirmar.getPreferredSize());
        btnConfirmar.addActionListener(this);
               
        int larguraTexto = lblTextoMensagem.getWidth();
        
        //determinando um tamanho mínimo
        if (larguraTexto > 500){
            super.setSize(larguraTexto + 50, 250);
        } else {
            super.setSize(500,250);
        }
        
        super.setLayout(null);
        super.setUndecorated(true);
        setBackground(new Color(1.0f,1.0f,1.0f,0f)); //deixando o fundo invisivel
        super.setModalityType(ModalityType.APPLICATION_MODAL);
        super.setLocationRelativeTo(null);
        
        try {
            //botoes de fechar e minimizar
            Image background = ImageIO.read (getClass().getResource(
                    "/arquivosComplementares/imagens/menus/backgrounds/background_mensagem.png"));
            Icon backgroundIcon = new ImageIcon(background.getScaledInstance(
                    super.getWidth(), super.getHeight(), Image.SCALE_DEFAULT));
            
            setContentPane (new JLabel (backgroundIcon));
        } catch (IOException ex) {
            
        }
        
        lblTextoMensagem.setLocation((super.getWidth() - lblTextoMensagem.getWidth())/2, 
                                      100);
        add(lblTextoMensagem);
        
        btnConfirmar.setLocation((super.getWidth() - btnConfirmar.getWidth())/2,
                                  super.getHeight() - btnConfirmar.getHeight() - 20);
        add(btnConfirmar);
        
        btnFechar = new JButton();
        btnFechar.setBorder(null);
        btnFechar.setContentAreaFilled(false);
        btnFechar.setIcon(iconBotaoFechar);
        btnFechar.setPressedIcon(iconBotaoFecharPressed);
        btnFechar.setSize(btnFechar.getPreferredSize());
        btnFechar.setLocation(super.getWidth() - btnFechar.getWidth() - 10, 15);
        btnFechar.addActionListener(this);
        add(btnFechar);
        
        super.setVisible(true);
    }
    
    public void mostrarPergunta(String mensagemTexto){
        carregaImagens();
        
        lblTextoMensagem = new JLabel(mensagemTexto);
        lblTextoMensagem.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 18));
        lblTextoMensagem.setForeground(new Color(1,133,170));
        lblTextoMensagem.setSize(lblTextoMensagem.getPreferredSize()); 
                
        btnAlternativaUm = new JButton ();
        btnAlternativaUm.setBorder(null);
        btnAlternativaUm.setContentAreaFilled(false);
        btnAlternativaUm.setIcon(iconOpcaoSim);
        btnAlternativaUm.setPressedIcon(iconOpcaoSimPressed);
        btnAlternativaUm.setSize(btnAlternativaUm.getPreferredSize());
        btnAlternativaUm.addActionListener(this);
        
        btnAlternativaDois = new JButton ();
        btnAlternativaDois.setBorder(null);
        btnAlternativaDois.setContentAreaFilled(false);
        btnAlternativaDois.setIcon(iconOpcaoNao);
        btnAlternativaDois.setPressedIcon(iconOpcaoNaoPressed);
        btnAlternativaDois.setSize(btnAlternativaDois.getPreferredSize());
        btnAlternativaDois.addActionListener(this);
               
        int larguraTexto = lblTextoMensagem.getWidth();
        
        //determinando um tamanho mínimo
        if (larguraTexto > 500){
            super.setSize(larguraTexto + 50, 250);
        } else {
            super.setSize(500,250);
        }
        
        super.setLayout(null);
        super.setUndecorated(true);
        setBackground(new Color(1.0f,1.0f,1.0f,0f)); //deixando o fundo invisivel
        super.setModalityType(ModalityType.APPLICATION_MODAL);
        super.setLocationRelativeTo(null);
        
        try {
            //botoes de fechar e minimizar
            Image background = ImageIO.read (getClass().getResource(
                    "/arquivosComplementares/imagens/menus/backgrounds/background_mensagem.png"));
            Icon backgroundIcon = new ImageIcon(background.getScaledInstance(
                    super.getWidth(), super.getHeight(), Image.SCALE_DEFAULT));
            
            setContentPane (new JLabel (backgroundIcon));
        } catch (IOException ex) {
            
        }
        
        lblTextoMensagem.setLocation((super.getWidth() - lblTextoMensagem.getWidth())/2, 
                                      100);
        add(lblTextoMensagem);
        
        //arrumando a posição em relação ao tamanho que o jdialog adquiriu
        btnAlternativaUm.setLocation(
            (getWidth() - (btnAlternativaUm.getWidth() + btnAlternativaDois.getWidth() + 50))/2, 
             getHeight() - btnAlternativaUm.getHeight() - 40);
        
        add(btnAlternativaUm);
        
        btnAlternativaDois.setLocation(
                btnAlternativaUm.getX() + btnAlternativaUm.getWidth() +  50,
                btnAlternativaUm.getY());
        
        add(btnAlternativaDois);
        
        btnFechar = new JButton();
        btnFechar.setBorder(null);
        btnFechar.setContentAreaFilled(false);
        btnFechar.setIcon(iconBotaoFechar);
        btnFechar.setPressedIcon(iconBotaoFecharPressed);
        btnFechar.setSize(btnFechar.getPreferredSize());
        btnFechar.setLocation(super.getWidth() - btnFechar.getWidth() - 10, 15);
        btnFechar.addActionListener(this);
        add(btnFechar);
        
        super.setVisible(true);
    }
    
    //eventos de botões
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnConfirmar){
            dispose();
        } 
        
        else if (e.getSource() == btnAlternativaUm){
            escolhido = 1;
            dispose();
        }
        
        else if (e.getSource() == btnAlternativaDois){
            escolhido = 2;
            dispose();
        }
        
        else if (e.getSource() == btnFechar){
            if (acao){
                dispose();
            } else {
                //fechar o jdialog é a mesma coisa que 
                //responder não, certo?
                escolhido = 2;
                dispose();
            }
        }
    }

}