
package View;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;

public class Botao extends JButton{
    
    private BufferedImage icone;
    private String texto;
    private int tamanho; 
    public  void setTexto(String tex){
        texto = tex;
    }
    
    public String getTexto(){
        return texto;
    }
    
    public Botao(String caminho){
        
        try {
            icone = ImageIO.read(getClass().getResource(caminho));
        } catch (IOException ex) {
            
        }
        
        inicializa();
    }
    
    public void inicializa(){
        setSize(50, 90);
        JLabel labelUso = new JLabel(texto);
        labelUso.setSize(labelUso.getPreferredSize());
 
        int tamanhoLabel = labelUso.getWidth();
        tamanho = (labelUso.getWidth() - tamanhoLabel)/2;

    }
    
    @Override
    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(icone,0, 0, getWidth(),getHeight(), null);
        g2.drawString(texto, 25, 25);
    }
            
  
}
