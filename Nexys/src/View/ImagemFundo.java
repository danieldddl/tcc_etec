package View;

import java.awt.*;  
import java.awt.geom.Rectangle2D;  
import java.awt.image.BufferedImage;
import java.io.IOException; 
import javax.imageio.ImageIO;

public class ImagemFundo extends javax.swing.JPanel {
 
        private BufferedImage b;  
        private Rectangle2D rect;  
        
        public ImagemFundo(int descFase){   
            try {  
                 //se você pegar uma imagem dentro do mesmo jar/projeto  
            	
                //ou, se você pegar uma imagem direto do sistema, use este                 
                // b = ImageIO.read(new File("<caminho da imagem>"));  
                 
                if (descFase == 1){
                	b = ImageIO.read(getClass().getResourceAsStream("/Imagensp/background_facil.png"));
       
            	} else if (descFase == 2){
            		b = ImageIO.read(getClass().getResourceAsStream("/Imagensp/background_medio.png"));
            		
            	} else if (descFase == 3){
            		b = ImageIO.read(getClass().getResourceAsStream("/Imagensp/background_dificil.png"));
            		
            	}
                
                //cria uma imagem do tamanho 130x130,   
                //que vai se repetir ao longo do fundo, o tamanho é você quem escolhe.  
                rect  = new Rectangle(0,0,130,130);                                   
                  
            } catch (IOException ex) {  
                ex.printStackTrace(System.err);  
            }  
        }  
          
        @Override   
        public void paintComponent(Graphics g){    
            /* 
             * Se você quiser que a imagem seja uma só (extendida ao tamanho da tela, não replicada 
             * tire os comentários da proxima linha 
             */  
              
          rect = new Rectangle(0,0,this.getWidth(),this.getHeight());  
              
              
            TexturePaint p = new TexturePaint(b,rect);  
            Graphics2D g2 = (Graphics2D) g;  
            g2.setPaint(p);  
            g2.fillRect(0,0,this.getWidth(),this.getHeight());  
   
        }  
    }     

