 package View;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import nextSys.sistemaGeral.formularios.BotaoFechar;
import nextSys.sistemaGeral.formularios.CaixaMensagem;
import nextSys.sistemaGeral.formularios.TelaFaseDois;
import nextSys.sistemaGeral.formularios.TelaFaseTres;
import nextSys.sistemaGeral.formularios.TelaFaseUm;
import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;

public class JanelaAnimais extends JFrame implements ActionListener{    
    private JLabel img = new JLabel();
    private JLabel img2 = new JLabel();
    private JLabel acertosLabel = new JLabel("00/10");
    private Botao alternativa1 = new Botao("/Imagensp/botao_escolha.png");
    private Botao alternativa2 = new Botao("/Imagensp/botao_escolha.png");
    private Botao alternativa3 = new Botao("/Imagensp/botao_escolha.png");
    private Botao alternativa4 = new Botao("/Imagensp/botao_escolha.png");
    private JButton BotaoOuvirAudios1 = new JButton();
    private JButton BotaoOuvirAudios2 = new JButton();
    private JButton BotaoOuvirAudios3 = new JButton();
    private JButton BotaoOuvirAudios4 = new JButton();
    private JButton voltar = new JButton();
    private JButton minimizar = new JButton();
    private BotaoFechar fechar;

    private String Resposta;
    private int segundos=0;
    private int pegandoElemento;    
    private int acertos=0;
    private int erros=0;
    private int descFase;
    private Timer tempoDaImagemColorida;
    
    PerguntasAnimais pi = new PerguntasAnimais();
    PerguntasAnimais.PerguntaImagem pim = pi.Random();
   
        private Image som = null;
        private Image voltarAoMenu = null;
        private Image minimize = null;
        private Image minimizePress = null;
        private Image alternativaPress = null;

        
        public void inicializaComponentes(){
            try {
            som = ImageIO.read(getClass().getResource("/Imagensp/som.png"));
            voltarAoMenu = ImageIO.read(getClass().getResource("/Imagensp/back_to_menu.png"));
            minimize = ImageIO.read(getClass().getResource("/Imagensp/minimize.png"));
            minimizePress = ImageIO.read(getClass().getResource("/Imagensp/minimize-pressed.png"));
            alternativaPress = ImageIO.read(getClass().getResource("/Imagensp/botao_escolha_pressionado.png"));
            
         } catch (IOException ex) {
            Logger.getLogger(JanelaAnimais.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            setContentPane(new ImagemFundo(descFase));  
            super.setSize(1030,660);
            super.setTitle("Quiz");
            super.setDefaultCloseOperation(EXIT_ON_CLOSE);
            super.setLocationRelativeTo(null);
            super.setLayout(null);
            super.setUndecorated(true);
            
            fechar = new BotaoFechar(this, "Are you sure?",1 , 40, 40, 10, 10);
            fechar.removeActionListener(fechar);
            fechar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {         
                        
                    CaixaMensagem c = new CaixaMensagem ();
                    c.mostrarPergunta("Are you sure?");
                    
                    if(c.getEscolhido()==1){
                    
                    switch(descFase){
                        case 1:
                            TelaFaseUm tfu = new TelaFaseUm(descFase);
                            tfu.setVisible(true);
                            break;
                        case 2:
                            TelaFaseDois tfd = new TelaFaseDois(descFase);
                            tfd.setVisible(true);
                            break;
                        case 3:
                            TelaFaseTres tft = new TelaFaseTres(descFase);
                            tft.setVisible(true);
                            break;
                    }
                    dispose();
                    }
                    else{}                    
      }
            });
            super.add(fechar);
            
            super.add(minimizar);
            minimizar.setIcon(new ImageIcon(minimize.getScaledInstance(40, 40, Image.SCALE_DEFAULT)));
            minimizar.setPressedIcon(new ImageIcon(minimizePress));
            minimizar.setBounds(930,20,40,40);
            minimizar.setBorder(null);
            minimizar.setContentAreaFilled(false);
            minimizar.setFocusable(true);
            minimizar.addActionListener(this); 

            
            super.add(BotaoOuvirAudios1);
            BotaoOuvirAudios1.setBounds(275,500,30,30);
            BotaoOuvirAudios1.setIcon(new ImageIcon(som.getScaledInstance(BotaoOuvirAudios1.getWidth(), BotaoOuvirAudios1.getHeight(), Image.SCALE_DEFAULT)));
            BotaoOuvirAudios1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    AudioBotao1();   
                }
            });
            BotaoOuvirAudios1.setLayout(null);
            BotaoOuvirAudios1.setBorder(null);
            BotaoOuvirAudios1.setContentAreaFilled(false);
            BotaoOuvirAudios1.setFocusable(true);
            
            super.add(BotaoOuvirAudios2);
            BotaoOuvirAudios2.setBounds(405,500,30,30);
            BotaoOuvirAudios2.setIcon(new ImageIcon(som.getScaledInstance(BotaoOuvirAudios2.getHeight(), BotaoOuvirAudios2.getWidth(), Image.SCALE_DEFAULT)));
            BotaoOuvirAudios2.addActionListener(new ActionListener() {
                 public void actionPerformed(ActionEvent e) {
                     AudioBotao2(); 
                }
            });
            BotaoOuvirAudios2.setLayout(null);
            BotaoOuvirAudios2.setBorder(null);
            BotaoOuvirAudios2.setContentAreaFilled(false);
            BotaoOuvirAudios2.setFocusable(true);
            
            super.add(BotaoOuvirAudios3);
            BotaoOuvirAudios3.setBounds(525,500,30,30);
            BotaoOuvirAudios3.setIcon(new ImageIcon(som.getScaledInstance(BotaoOuvirAudios3.getHeight(), BotaoOuvirAudios3.getWidth(), Image.SCALE_DEFAULT)));
            BotaoOuvirAudios3.addActionListener(new ActionListener(){
                 public void actionPerformed(ActionEvent e) {
                    AudioBotao3();
                }
            });
            BotaoOuvirAudios3.setLayout(null);
            BotaoOuvirAudios3.setBorder(null);
            BotaoOuvirAudios3.setContentAreaFilled(false);
            BotaoOuvirAudios3.setFocusable(true);
            
            super.add(BotaoOuvirAudios4);
            BotaoOuvirAudios4.setBounds(645,500,30,30);
            BotaoOuvirAudios4.setIcon(new ImageIcon(som.getScaledInstance(BotaoOuvirAudios4.getHeight(), BotaoOuvirAudios4.getWidth(), Image.SCALE_DEFAULT)));
            BotaoOuvirAudios4.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    AudioBotao4();
                }
            });
            BotaoOuvirAudios4.setLayout(null);
            BotaoOuvirAudios4.setBorder(null);
            BotaoOuvirAudios4.setContentAreaFilled(false);
            BotaoOuvirAudios4.setFocusable(true);
            
            super.add(voltar);
            voltar.setBounds(440, 580, 150, 50);
            voltar.setIcon(new ImageIcon(voltarAoMenu.getScaledInstance(150, 50, Image.SCALE_DEFAULT)));
            voltar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    switch(descFase){
                        case 1:
                            TelaFaseUm tfu = new TelaFaseUm(descFase);
                            tfu.setVisible(true);
                            break;
                        case 2:
                            TelaFaseDois tfd = new TelaFaseDois(descFase);
                            tfd.setVisible(true);
                            break;
                        case 3:
                            TelaFaseTres tft = new TelaFaseTres(descFase);
                            tft.setVisible(true);
                            break;
                    }
                    dispose();
                }
            });
            
            super.add(img);
            img.setBounds(400,130, 250, 250);
            img.setIcon(new ImageIcon(pim.imagens.getImage().getScaledInstance(img.getWidth(), img.getHeight(), Image.SCALE_DEFAULT)));    
            super.setLayout(null);
            
            super.add(img2);
            img2.setBounds(400, 130, 250, 250);
            super.setLayout(null);
                        
            super.add(acertosLabel);
            acertosLabel.setBounds(10,10,100,100);
            acertosLabel.setFont(new Font("Freestyle Script",Font.ITALIC,50));
            acertosLabel.setForeground(Color.yellow);
            super.setLayout(null);
            
           super.add(alternativa1);
           alternativa1.setBounds(250,450, 90, 48);
           alternativa1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                alternativa1actionPerformed(evt);     
            }
        });
           alternativa1.setPressedIcon(new ImageIcon(alternativaPress));
           
           super.add(alternativa2);
           alternativa2.setBounds(380,450, 90, 48);
           alternativa2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                alternativa2ActionPerformed(evt);
            }
       });
           alternativa2.setPressedIcon(new ImageIcon(alternativaPress));
           
           super.add(alternativa3);
           alternativa3.setBounds(500,450, 90, 48);
           alternativa3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                alternativa3ActionPerformed(evt);
            }
        });
           alternativa3.setPressedIcon(new ImageIcon(alternativaPress));
         
           super.add(alternativa4);
           alternativa4.setBounds(620,450, 90, 48);
           alternativa4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                alternativa4ActionPerformed(evt);
            }
        });
           alternativa4.setPressedIcon(new ImageIcon(alternativaPress));
           
        tempoDaImagemColorida = new Timer(50,new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                segundos++;
              
                 if(segundos==30){
                     img.setVisible(true);
                     chamarImagemEBotoes();
                     tempoDaImagemColorida.stop();
                     
                        if(segundos != 0){
                            segundos=0;
                     }    
      }
                 if(segundos==1){
                     
                     if(pegandoElemento==0){
                        img.setVisible(false);
                        Image bull;
                         try {
                            bull = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/bull.jpg"));
                            img2.setIcon(new ImageIcon (bull.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==1){
                       img.setVisible(false);
                        Image bunny;
                         try {
                            bunny = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/bunny.jpg"));
                            img2.setIcon(new ImageIcon (bunny.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==2){
                       img.setVisible(false);
                        Image camel;
                         try {
                            camel = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/camel.jpg"));
                            img2.setIcon(new ImageIcon (camel.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==3){
                       img.setVisible(false);
                        Image cat;
                         try {
                            cat = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/cat.jpg"));
                            img2.setIcon(new ImageIcon (cat.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==4){
                       img.setVisible(false);
                        Image cow;
                         try {
                            cow = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/cow.jpg"));
                            img2.setIcon(new ImageIcon (cow.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==5){
                       img.setVisible(false);
                        Image dog;
                         try {
                            dog = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/dog.jpg"));
                            img2.setIcon(new ImageIcon (dog.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==6){
                       img.setVisible(false);
                        Image donkey;
                         try {
                            donkey = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/donkey.jpg"));
                            img2.setIcon(new ImageIcon (donkey.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==7){
                       img.setVisible(false);
                        Image lion;
                         try {
                            lion = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/lion.jpg"));
                            img2.setIcon(new ImageIcon (lion.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==8){
                       img.setVisible(false);
                        Image monkey;
                         try {
                            monkey = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/monkey.jpg"));
                            img2.setIcon(new ImageIcon (monkey.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==9){
                       img.setVisible(false);
                        Image pig;
                         try {
                            pig = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/pig.jpg"));
                            img2.setIcon(new ImageIcon (pig.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==10){
                       img.setVisible(false);
                        Image rat;
                         try {
                            rat = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/rat.jpg"));
                            img2.setIcon(new ImageIcon (rat.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                      }
                     if(pegandoElemento==11){
                       img.setVisible(false);
                        Image rhino;
                         try {
                            rhino = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/rhino.jpg"));
                            img2.setIcon(new ImageIcon (rhino.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                      }
                     if(pegandoElemento==12){
                       img.setVisible(false);
                        Image sheep;
                         try {
                            sheep = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/sheep.jpg"));
                            img2.setIcon(new ImageIcon (sheep.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                      }
                     if(pegandoElemento==13){
                       img.setVisible(false);
                        Image turtle;
                         try {
                            turtle = ImageIO.read(getClass().getResource("/Imagens/originais/Animals/turtle.jpg"));
                            img2.setIcon(new ImageIcon (turtle.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
            }
            }
        });
        
        
        chamarImagemEBotoes();
        }
        
        public JanelaAnimais(int descFase){
        	this.descFase = descFase;
            inicializaComponentes();
        }
  
        public void AudioBotao1(){
            if(alternativa1.getTexto().equals("Bull")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Bull.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Bunny")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Bunny.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Camel")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Camel.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Cat")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Cat.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Cow")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Cow.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Dog")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Dog.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Donkey")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Donkey.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Lion")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Lion.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Monkey")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Monkey.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Pig")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Pig.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Rat")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Rat.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();    
            }
            if(alternativa1.getTexto().equals("Rhino")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Rhino.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Sheep")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Sheep.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Turtle")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Turtle.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
        }
        
        public void AudioBotao2(){
            if(alternativa2.getTexto().equals("Bull")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Bull.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Bunny")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Bunny.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Camel")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Camel.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Cat")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Cat.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Cow")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Cow.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Dog")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Dog.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Donkey")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Donkey.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Lion")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Lion.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Monkey")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Monkey.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Pig")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Pig.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Rat")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Rat.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();    
            }
            if(alternativa2.getTexto().equals("Rhino")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Rhino.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Sheep")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Sheep.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Turtle")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Turtle.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
        }
    }
        
        public void AudioBotao3(){
            if(alternativa3.getTexto().equals("Bull")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Bull.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Bunny")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Bunny.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Camel")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Camel.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Cat")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Cat.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Cow")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Cow.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Dog")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Dog.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Donkey")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Donkey.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Lion")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Lion.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Monkey")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Monkey.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Pig")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Pig.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Rat")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Rat.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();    
            }
            if(alternativa3.getTexto().equals("Rhino")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Rhino.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Sheep")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Sheep.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Turtle")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Turtle.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
        }
        }
        
        public void AudioBotao4(){
            if(alternativa4.getTexto().equals("Bull")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Bull.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Bunny")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Bunny.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Camel")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Camel.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Cat")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Cat.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Cow")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Cow.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Dog")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Dog.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Donkey")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Donkey.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Lion")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Lion.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Monkey")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Monkey.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Pig")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Pig.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Rat")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Rat.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();    
            }
            if(alternativa4.getTexto().equals("Rhino")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Rhino.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Sheep")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Sheep.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Turtle")){
            URL url = JanelaAnimais.class.getResource("/Audios/Animais/Turtle.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
        }
        }
        
        public void alternativa1actionPerformed(ActionEvent evt){
            if (alternativa1.getTexto() == Resposta)
      {
          acertos++;
          acertosLabel.setText("0"+acertos+"/10");
            if(acertos==10){
                acertosLabel.setText(acertos+"/10");
            }
          tempoDaImagemColorida.start();
        }else{
            erros++;
            new CaixaMensagem ().mostrarMensagem("Sorry. It's wrong :C");
            URL url = JanelaAnimais.class.getResource("/Audios/Error.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
        }
        
        public void alternativa2ActionPerformed(ActionEvent evt) {
            if (alternativa2.getTexto() == Resposta)
      {
          acertos++;
          acertosLabel.setText("0"+acertos+"/10");
            if(acertos==10){
                acertosLabel.setText(acertos+"/10");
            }
            tempoDaImagemColorida.start();
        }else{
                erros++;
                new CaixaMensagem ().mostrarMensagem("Sorry. It's wrong :C");
            URL url = JanelaAnimais.class.getResource("/Audios/Error.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
}
        
        public void alternativa3ActionPerformed(ActionEvent evt) {                                         
            if (alternativa3.getTexto() == Resposta)
      {
          acertos++;
          acertosLabel.setText("0"+acertos+"/10");
            if(acertos==10){
                acertosLabel.setText(acertos+"/10");
            }
        tempoDaImagemColorida.start();
        }else{
                erros++;
                new CaixaMensagem ().mostrarMensagem("Sorry. It's wrong :C");
            URL url = JanelaAnimais.class.getResource("/Audios/Error.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
}
        
        public void alternativa4ActionPerformed(ActionEvent evt){
            if (alternativa4.getTexto() == Resposta)
      {
          acertos++;
          acertosLabel.setText("0"+acertos+"/10");
            if(acertos==10){
                acertosLabel.setText(acertos+"/10");
            }          
            tempoDaImagemColorida.start();
      }else{
                erros++;
                new CaixaMensagem ().mostrarMensagem("Sorry. It's wrong :C");
            URL url = JanelaAnimais.class.getResource("/Audios/Error.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }     
    }

        public void chamarImagemEBotoes(){
            if(acertos==10){
            	new CaixaMensagem().mostrarMensagem("Congratulations! You have won.");
                new GerenciamentoFases().terminoJogo(descFase, 2);
                
                if (descFase == 1){
                	TelaFaseUm t = new TelaFaseUm(1);
                	t.setVisible(true);
                	
                } else if (descFase == 2){
                	TelaFaseDois t = new TelaFaseDois(2);
                	t.setVisible(true);
                	
                } else if (descFase == 3){
                	TelaFaseTres t = new TelaFaseTres(3);
                	t.setVisible(true);
                	
                }
                
                dispose();
                
            }
            
            PerguntasAnimais pi = new PerguntasAnimais();
            PerguntasAnimais.PerguntaImagem pim = pi.Random();
            
img.setIcon(new ImageIcon(pim.imagens.getImage().getScaledInstance(img.getWidth(), img.getHeight(), Image.SCALE_DEFAULT)));    
           

           PerguntasAnimais.Resposta r1 = pim.Respostas.get(0);
           alternativa1.setTexto(r1.Texto);
           if (r1.Correta) this.Resposta = r1.Texto;
           
           PerguntasAnimais.Resposta r2 = pim.Respostas.get(1);
           alternativa2.setTexto(r2.Texto);
           if (r2.Correta) this.Resposta = r2.Texto;
            
           PerguntasAnimais.Resposta r3 = pim.Respostas.get(2);
           alternativa3.setTexto(r3.Texto);
           if (r3.Correta) this.Resposta = r3.Texto;
            
           PerguntasAnimais.Resposta r4 = pim.Respostas.get(3);
           alternativa4.setTexto(r4.Texto);
           if (r4.Correta) this.Resposta = r4.Texto;
           
           alternativa1.repaint();
           alternativa2.repaint();
           alternativa3.repaint();
           alternativa4.repaint();
           
           pegandoElemento = pi.PI.indexOf(pim);

        }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==minimizar){
            super.setState(JFrame.ICONIFIED);
        }
    }
       
}
