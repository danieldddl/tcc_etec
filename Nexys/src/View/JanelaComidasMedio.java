package View;

import java.applet.Applet;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import javax.swing.*;
import java.awt.event.*;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;  
import nextSys.sistemaGeral.formularios.BotaoFechar;
import nextSys.sistemaGeral.formularios.CaixaMensagem;
import nextSys.sistemaGeral.formularios.TelaFaseDois;
import nextSys.sistemaGeral.formularios.TelaFaseTres;
import nextSys.sistemaGeral.formularios.TelaFaseUm;
import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;

    public class JanelaComidasMedio extends JFrame implements ActionListener{
        
    private JLabel img = new JLabel();
    private JLabel img2 = new JLabel();
    private JLabel acertosLabel = new JLabel("00/20");
    private JLabel tempodeRespostaLabel = new JLabel();
    private Botao alternativa1 = new Botao("/Imagensp/botao_escolha.png");
    private Botao alternativa2 = new Botao("/Imagensp/botao_escolha.png");
    private Botao alternativa3 = new Botao("/Imagensp/botao_escolha.png");
    private Botao alternativa4 = new Botao("/Imagensp/botao_escolha.png");
    private JButton BotaoOuvirAudios1 = new JButton();
    private JButton BotaoOuvirAudios2 = new JButton();
    private JButton BotaoOuvirAudios3 = new JButton();
    private JButton BotaoOuvirAudios4 = new JButton();
    private JButton voltar = new JButton();
    private JButton minimizar = new JButton();
    private BotaoFechar fechar;
   
    private int tempoDeResposta=10;
    private int acertos=0;
    private int erros=0;
    private int segundos=0;
    private int pegandoElemento;
    private int descFase;
    
    private Timer tempoDaImagemColorida;
    private Timer tempoDeRespostaTimer;
    private String Resposta;
    
    PerguntasComida pi = new PerguntasComida();
    PerguntasComida.PerguntaImagem pim = pi.Random();
   
        private Image som = null;
        private Image voltarAoMenu = null;
        private Image minimize = null;
        private Image minimizePress = null;
        private Image alternativaPress = null;
        
        public void inicializaComponentes(){
            try {
            som = ImageIO.read(getClass().getResource("/Imagensp/som.png"));
            voltarAoMenu = ImageIO.read(getClass().getResource("/Imagensp/back_to_menu.png"));
            minimize = ImageIO.read(getClass().getResource("/Imagensp/botao_minimizar_azul.png"));
            minimizePress = ImageIO.read(getClass().getResource("/Imagensp/botao_minimizar_azul_press.png"));
            alternativaPress = ImageIO.read(getClass().getResource("/Imagensp/botao_escolha_pressionado.png"));
            
        } catch (IOException ex) {
            Logger.getLogger(JanelaAnimais.class.getName()).log(Level.SEVERE, null, ex);
        }
        
            setContentPane(new ImagemFundo(descFase));   
            super.setSize(1030,660);
            super.setTitle("Quiz");
            super.setDefaultCloseOperation(EXIT_ON_CLOSE);
            super.setLocationRelativeTo(null);
            super.setLayout(null);
            super.setUndecorated(true);
        
            fechar = new BotaoFechar(this, "Are you sure?",2 , 40, 40, 10, 10);
            fechar.removeActionListener(fechar);
            fechar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {         
                        
                    CaixaMensagem c = new CaixaMensagem ();
                    c.mostrarPergunta("Are you sure?");
                    
                    if(c.getEscolhido()==1){
             
                    switch(descFase){
                        case 1:
                            TelaFaseUm tfu = new TelaFaseUm(descFase);
                            tfu.setVisible(true);
                            break;
                        case 2:
                            TelaFaseDois tfd = new TelaFaseDois(descFase);
                            tfd.setVisible(true);
                            break;
                        case 3:
                            TelaFaseTres tft = new TelaFaseTres(descFase);
                            tft.setVisible(true);
                            break;
                    }
                           tempoDeRespostaTimer.stop();
                    dispose();
                    }
                    else{}                    
      }
            });
            super.add(fechar);
            
            super.add(minimizar);
            minimizar.setIcon(new ImageIcon(minimize.getScaledInstance(40, 40, Image.SCALE_DEFAULT)));
            minimizar.setPressedIcon(new ImageIcon(minimizePress));
            minimizar.setBounds(930,20,40,40);
            minimizar.setBorder(null);
            minimizar.setContentAreaFilled(false);
            minimizar.setFocusable(true);
            minimizar.addActionListener(this); 
            
            super.add(BotaoOuvirAudios1);
            BotaoOuvirAudios1.setBounds(250,500,30,30);
            BotaoOuvirAudios1.setIcon(new ImageIcon(som.getScaledInstance(BotaoOuvirAudios1.getWidth(), BotaoOuvirAudios1.getHeight(), Image.SCALE_DEFAULT)));
            BotaoOuvirAudios1.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    AudioBotao1();   
                }
            });
            BotaoOuvirAudios1.setLayout(null);
            
            super.add(BotaoOuvirAudios2);
            BotaoOuvirAudios2.setBounds(380,500,30,30);
            BotaoOuvirAudios2.setIcon(new ImageIcon(som.getScaledInstance(BotaoOuvirAudios2.getHeight(), BotaoOuvirAudios2.getWidth(), Image.SCALE_DEFAULT)));
            BotaoOuvirAudios2.addActionListener(new ActionListener() {
                 public void actionPerformed(ActionEvent e) {
                     AudioBotao2(); 
                }
            });
            BotaoOuvirAudios2.setLayout(null);
            
            super.add(BotaoOuvirAudios3);
            BotaoOuvirAudios3.setBounds(500,500,30,30);
            BotaoOuvirAudios3.setIcon(new ImageIcon(som.getScaledInstance(BotaoOuvirAudios3.getHeight(), BotaoOuvirAudios3.getWidth(), Image.SCALE_DEFAULT)));
            BotaoOuvirAudios3.addActionListener(new ActionListener(){
                 public void actionPerformed(ActionEvent e) {
                    AudioBotao3();
                }
            });
            BotaoOuvirAudios3.setLayout(null);
            
            super.add(BotaoOuvirAudios4);
            BotaoOuvirAudios4.setBounds(620,500,30,30);
            BotaoOuvirAudios4.setIcon(new ImageIcon(som.getScaledInstance(BotaoOuvirAudios4.getHeight(), BotaoOuvirAudios4.getWidth(), Image.SCALE_DEFAULT)));
            BotaoOuvirAudios4.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    AudioBotao4();
                }
            });
            BotaoOuvirAudios4.setLayout(null);
            
            super.add(voltar);
            voltar.setBounds(440, 580, 150, 50);
            voltar.setIcon(new ImageIcon(voltarAoMenu.getScaledInstance(150, 50, Image.SCALE_DEFAULT)));
            voltar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    tempoDeRespostaTimer.stop();
                    dispose();
                    switch(descFase){
                        case 1:
                            TelaFaseUm tfu = new TelaFaseUm(descFase);
                            tfu.setVisible(true);
                            break;
                        case 2:
                            TelaFaseDois tfd = new TelaFaseDois(descFase);
                            tfd.setVisible(true);
                            break;
                        case 3:
                            TelaFaseTres tft = new TelaFaseTres(descFase);
                            tft.setVisible(true);
                            break;
                    }
                    dispose();
                }
            });

            super.add(tempodeRespostaLabel);
            tempodeRespostaLabel.setBounds(525,20,50,50);
            tempodeRespostaLabel.setText(Integer.toString(tempoDeResposta));
            tempodeRespostaLabel.setFont(new Font("Freestyle Script",Font.ITALIC,100));
            tempodeRespostaLabel.setForeground(Color.blue);
            super.setLayout(null);
            
            super.add(img);
            img.setBounds(400,130, 250, 250);
            img.setIcon(new ImageIcon(pim.imagens.getImage().getScaledInstance(img.getWidth(), img.getHeight(), Image.SCALE_DEFAULT)));    
            super.setLocationRelativeTo(null);
            super.setLayout(null);
            
            super.add(img2);
            img2.setBounds(400, 130, 250, 250);
            super.setLocationRelativeTo(null);
            super.setLayout(null);
            
            super.add(acertosLabel);
            acertosLabel.setBounds(10,10,100,100);
            acertosLabel.setFont(new Font("Freestyle Script",Font.ITALIC,50));
            acertosLabel.setForeground(new Color(0, 150, 0));
            super.setLayout(null);
                        
           super.add(alternativa1);
           alternativa1.setBounds(250,450, 90, 48);
           alternativa1.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
                alternativa1actionPerformed(evt);
            }
        });
           alternativa1.setPressedIcon(new ImageIcon(alternativaPress));
           
           super.add(alternativa2);
           alternativa2.setBounds(380,450, 90, 48);
           alternativa2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                alternativa2ActionPerformed(evt);
            }
       });
           alternativa2.setPressedIcon(new ImageIcon(alternativaPress));
                         
           super.add(alternativa3);
           alternativa3.setBounds(500,450, 90, 48);
           alternativa3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                alternativa3ActionPerformed(evt);
            }
        });
           alternativa3.setPressedIcon(new ImageIcon(alternativaPress));
             
           super.add(alternativa4);
           alternativa4.setBounds(620,450, 90, 48);
           alternativa4.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                alternativa4ActionPerformed(evt);
            }
        });
           alternativa4.setPressedIcon(new ImageIcon(alternativaPress));
            
           
        tempoDaImagemColorida = new Timer(50,new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                segundos++;
                            
                 if(segundos==30){
                     img.setVisible(true);
                     chamarImagemEBotoes();
                     tempoDaImagemColorida.stop();
                     
                        if(segundos != 0){
                            segundos=0;
                     }    
      }
                 if(segundos==1){
                     
                     if(pegandoElemento==0){
                        img.setVisible(false);
                        Image bull;
                         try {
                            bull = ImageIO.read(getClass().getResource("/Imagens/originais/Food/apple.jpg"));
                            img2.setIcon(new ImageIcon (bull.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==1){
                       img.setVisible(false);
                        Image bunny;
                         try {
                            bunny = ImageIO.read(getClass().getResource("/Imagens/originais/Food/banana.jpg"));
                            img2.setIcon(new ImageIcon (bunny.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==2){
                       img.setVisible(false);
                        Image camel;
                         try {
                            camel = ImageIO.read(getClass().getResource("/Imagens/originais/Food/chips.jpg"));
                            img2.setIcon(new ImageIcon (camel.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==3){
                       img.setVisible(false);
                        Image cat;
                         try {
                            cat = ImageIO.read(getClass().getResource("/Imagens/originais/Food/chocolate.jpg"));
                            img2.setIcon(new ImageIcon (cat.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==4){
                       img.setVisible(false);
                        Image cow;
                         try {
                            cow = ImageIO.read(getClass().getResource("/Imagens/originais/Food/coke.jpg"));
                            img2.setIcon(new ImageIcon (cow.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==5){
                       img.setVisible(false);
                        Image dog;
                         try {
                            dog = ImageIO.read(getClass().getResource("/Imagens/originais/Food/cupcake.jpg"));
                            img2.setIcon(new ImageIcon (dog.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==6){
                       img.setVisible(false);
                        Image donkey;
                         try {
                            donkey = ImageIO.read(getClass().getResource("/Imagens/originais/Food/eggs.jpg"));
                            img2.setIcon(new ImageIcon (donkey.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==7){
                       img.setVisible(false);
                        Image lion;
                         try {
                            lion = ImageIO.read(getClass().getResource("/Imagens/originais/Food/hamburger.jpg"));
                            img2.setIcon(new ImageIcon (lion.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==8){
                       img.setVisible(false);
                        Image monkey;
                         try {
                            monkey = ImageIO.read(getClass().getResource("/Imagens/originais/Food/hotdog.jpg"));
                            img2.setIcon(new ImageIcon (monkey.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==9){
                       img.setVisible(false);
                        Image pig;
                         try {
                            pig = ImageIO.read(getClass().getResource("/Imagens/originais/Food/icecream.jpg"));
                            img2.setIcon(new ImageIcon (pig.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                     if(pegandoElemento==10){
                       img.setVisible(false);
                        Image rat;
                         try {
                            rat = ImageIO.read(getClass().getResource("/Imagens/originais/Food/juice.jpg"));
                            img2.setIcon(new ImageIcon (rat.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                      }
                     if(pegandoElemento==11){
                       img.setVisible(false);
                        Image rhino;
                         try {
                            rhino = ImageIO.read(getClass().getResource("/Imagens/originais/Food/lemon.jpg"));
                            img2.setIcon(new ImageIcon (rhino.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                      }
                     if(pegandoElemento==12){
                       img.setVisible(false);
                        Image sheep;
                         try {
                            sheep = ImageIO.read(getClass().getResource("/Imagens/originais/Food/orange.jpg"));
                            img2.setIcon(new ImageIcon (sheep.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                      }
                     if(pegandoElemento==13){
                       img.setVisible(false);
                        Image turtle;
                         try {
                            turtle = ImageIO.read(getClass().getResource("/Imagens/originais/Food/pancake.jpg"));
                            img2.setIcon(new ImageIcon (turtle.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                     }
                    if(pegandoElemento==14){
                       img.setVisible(false);
                        Image walkman;
                         try {
                            walkman = ImageIO.read(getClass().getResource("/Imagens/originais/Food/radish.jpg"));
                            img2.setIcon(new ImageIcon (walkman.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                    }
                 if(pegandoElemento==15){
                       img.setVisible(false);
                        Image radish;
                         try {
                            radish = ImageIO.read(getClass().getResource("/Imagens/originais/Food/strawberry.jpg"));
                            img2.setIcon(new ImageIcon (radish.getScaledInstance(img2.getWidth(), img2.getHeight(), Image.SCALE_DEFAULT)));
                         } catch (IOException ex) {
                System.out.println(ex);
                         }
                 }
                 }
            }
        
        });  

        tempoDeRespostaTimer = new Timer(1000, new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                tempoDeResposta--;
                tempodeRespostaLabel.setText(Integer.toString(tempoDeResposta));
                
                if(tempoDeResposta==0){
                    tempoDeRespostaTimer.stop();
                new CaixaMensagem ().mostrarMensagem("You runned out of time :C");
                if(descFase == 1){
                	TelaFaseUm t = new TelaFaseUm(1);
                	t.setVisible(true);
                	
                } else if(descFase == 2){
                	TelaFaseDois t = new TelaFaseDois(2);
                	t.setVisible(true);
                } else {
                	TelaFaseTres t = new TelaFaseTres(3);
                	t.setVisible(true);
                }
                dispose();
            }
                
                }
        });
       
        chamarImagemEBotoes();
        }
        
        public JanelaComidasMedio(int descFase){
        	this.descFase = descFase;
            inicializaComponentes();
        }
  
        public void AudioBotao1(){
            if(alternativa1.getTexto().equals("Apple")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Apple.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Banana")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Banana.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Chips")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Chips.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Coke")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Coke.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Cupcake")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Cupcake.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Eggs")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Eggs.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Burguer")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Hamburguer.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Hotdog")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Hotdog.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Ice Cream")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Icecream.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Juice")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Juice.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Lemon")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Lemon.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();    
            }
            if(alternativa1.getTexto().equals("Onion")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Onion.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Orange")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Orange.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Pancakes")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Pancakes.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Radish")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Radish.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Strawberry")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Strawberry.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa1.getTexto().equals("Tomato")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Tomato.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
        }
        
        public void AudioBotao2(){
            if(alternativa2.getTexto().equals("Apple")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Apple.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Banana")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Banana.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Chips")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Chips.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Coke")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Coke.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Cupcake")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Cupcake.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Eggs")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Eggs.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Burguer")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Hamburguer.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Hotdog")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Hotdog.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Ice Cream")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Icecream.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Juice")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Juice.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Lemon")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Lemon.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();    
            }
            if(alternativa2.getTexto().equals("Onion")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Onion.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Orange")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Orange.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Pancakes")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Pancakes.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Radish")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Radish.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Strawberry")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Strawberry.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa2.getTexto().equals("Tomato")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Tomato.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
        }
        
        public void AudioBotao3(){
            if(alternativa3.getTexto().equals("Apple")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Apple.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Banana")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Banana.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Chips")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Chips.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Coke")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Coke.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Cupcake")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Cupcake.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Eggs")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Eggs.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Burguer")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Hamburguer.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Hotdog")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Hotdog.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Ice Cream")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Icecream.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Juice")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Juice.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Lemon")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Lemon.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();    
            }
            if(alternativa3.getTexto().equals("Onion")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Onion.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Orange")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Orange.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Pancakes")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Pancakes.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Radish")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Radish.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Strawberry")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Strawberry.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa3.getTexto().equals("Tomato")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Tomato.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
        }
 
        public void AudioBotao4(){
            if(alternativa4.getTexto().equals("Apple")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Apple.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Banana")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Banana.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Chips")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Chips.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Coke")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Coke.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Cupcake")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Cupcake.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Eggs")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Eggs.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Burguer")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Hamburguer.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Hot Dog")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Hotdog.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Ice Cream")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Icecream.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Juice")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Juice.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Lemon")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Lemon.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();    
            }
            if(alternativa4.getTexto().equals("Onion")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Onion.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Orange")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Orange.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Pancakes")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Pancakes.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Radish")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Radish.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Strawberry")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Strawberry.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
            if(alternativa4.getTexto().equals("Tomato")){
            URL url = JanelaComidasMedio.class.getResource("/Audios/Comida/Tomato.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
        }
        
        public void alternativa1actionPerformed(ActionEvent evt){
            if (alternativa1.getTexto() == Resposta)
      {
          tempoDeRespostaTimer.stop();
          acertos++;
          acertosLabel.setText("0"+acertos+"/20");
            if(acertos>9){
                acertosLabel.setText(acertos+"/20");
      }
          tempoDaImagemColorida.start();
        }else{
            erros++;
            new CaixaMensagem ().mostrarMensagem("Sorry. It's wrong :C");
            URL url = JanelaComidasMedio.class.getResource("/Audios/Error.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
        }
        
        public void alternativa2ActionPerformed(ActionEvent evt) {
            if (alternativa2.getTexto() == Resposta)
      {
            tempoDeRespostaTimer.stop();
            acertos++;
            acertosLabel.setText("0"+acertos+"/20");
            if(acertos>9){
                acertosLabel.setText(acertos+"/20");
      }
            tempoDaImagemColorida.start();
        }else{
            erros++;
            new CaixaMensagem ().mostrarMensagem("Sorry. It's wrong :C");
            URL url = JanelaComidasMedio.class.getResource("/Audios/Error.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
}
        
        public void alternativa3ActionPerformed(ActionEvent evt) {                                         
            if (alternativa3.getTexto() == Resposta)
            {
                tempoDeRespostaTimer.stop();
                acertos++;
                acertosLabel.setText("0"+acertos+"/20");
            if(acertos>9){
                acertosLabel.setText(acertos+"/20");
      }
                tempoDaImagemColorida.start();
        }else{
                erros++;
                new CaixaMensagem ().mostrarMensagem("Sorry. It's wrong :C");
            URL url = JanelaComidasMedio.class.getResource("/Audios/Error.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }
}
        
        public void alternativa4ActionPerformed(ActionEvent evt){
            if (alternativa4.getTexto() == Resposta)
      {
            tempoDeRespostaTimer.stop();
            acertos++;
            acertosLabel.setText("0"+acertos+"/20");
            if(acertos>9){
                acertosLabel.setText(acertos+"/20");
      }
            tempoDaImagemColorida.start();
      }else{
                erros++;
                new CaixaMensagem ().mostrarMensagem("Sorry. It's wrong :C");
            URL url = JanelaComidasMedio.class.getResource("/Audios/Error.wav");
            AudioClip audio  = Applet.newAudioClip(url);
            audio.play();
            }     
    }

        public void chamarImagemEBotoes(){
            if(acertos==20){
            	new CaixaMensagem().mostrarMensagem("Congratulations! You have won.");
                new GerenciamentoFases().terminoJogo(descFase, 2);
                
                if (descFase == 1){
                	TelaFaseUm t = new TelaFaseUm(1);
                	t.setVisible(true);
                	
                } else if (descFase == 2){
                	TelaFaseDois t = new TelaFaseDois(2);
                	t.setVisible(true);
                	
                } else if (descFase == 3){
                	TelaFaseTres t = new TelaFaseTres(3);
                	t.setVisible(true);
                	
                }
                
                dispose();
            }
            
            
            tempoDeRespostaTimer.start();
            
            if(tempoDeResposta != 10){
                tempoDeResposta=10;
            }

            
            PerguntasComida pi = new PerguntasComida();
            PerguntasComida.PerguntaImagem pim = pi.Random();
            
img.setIcon(new ImageIcon(pim.imagens.getImage().getScaledInstance(img.getWidth(), img.getHeight(), Image.SCALE_DEFAULT)));    
           

           PerguntasComida.Resposta r1 = pim.Respostas.get(0);
           alternativa1.setTexto(r1.Texto);
           if (r1.Correta) this.Resposta = r1.Texto;
           
           PerguntasComida.Resposta r2 = pim.Respostas.get(1);
           alternativa2.setTexto(r2.Texto);
           if (r2.Correta) this.Resposta = r2.Texto;
            
           PerguntasComida.Resposta r3 = pim.Respostas.get(2);
           alternativa3.setTexto(r3.Texto);
           if (r3.Correta) this.Resposta = r3.Texto;
            
           PerguntasComida.Resposta r4 = pim.Respostas.get(3);
           alternativa4.setTexto(r4.Texto);
           if (r4.Correta) this.Resposta = r4.Texto;
           
           alternativa1.repaint();
           alternativa2.repaint();
           alternativa3.repaint();
           alternativa4.repaint();
           
           pegandoElemento = pi.PI.indexOf(pim);
        }
 
        public void actionPerformed(ActionEvent e) {
            if(e.getSource()==minimizar){
            super.setState(JFrame.ICONIFIED);
        }
    }
}