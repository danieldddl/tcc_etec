package View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;

public class PerguntasAnimais {
    
    private Random randomGenerator;
    List<PerguntaImagem> PI = new ArrayList<PerguntaImagem>();
        
    public PerguntasAnimais()
    {       
        this.randomGenerator = new Random();
      
        PerguntaImagem p0 = new PerguntaImagem();
        p0.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/bull.jpg"));
        p0.Respostas.add(new Resposta(true, "Bull"));
        p0.Respostas.add(new Resposta(false, "Cow"));
        p0.Respostas.add(new Resposta(false, "Dog"));
        p0.Respostas.add(new Resposta(false, "Rhino"));
        
        PerguntaImagem p1 = new PerguntaImagem();
        p1.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/bunny.jpg"));
        p1.Respostas.add(new Resposta(false, "Rat"));
        p1.Respostas.add(new Resposta(false, "Cow"));
        p1.Respostas.add(new Resposta(false, "Bull"));
        p1.Respostas.add(new Resposta(true, "Bunny"));
        
        PerguntaImagem p2 = new PerguntaImagem();
        p2.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/camel.jpg"));
        p2.Respostas.add(new Resposta(false, "Donkey"));
        p2.Respostas.add(new Resposta(false, "Hippo"));
        p2.Respostas.add(new Resposta(true, "Camel"));
        p2.Respostas.add(new Resposta(false, "Bunny"));
        
        PerguntaImagem p3 = new PerguntaImagem();
        p3.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/cat.jpg"));
        p3.Respostas.add(new Resposta(false, "Dog"));
        p3.Respostas.add(new Resposta(true, "Cat"));
        p3.Respostas.add(new Resposta(false, "Rat"));
        p3.Respostas.add(new Resposta(false, "Camel"));
        
        PerguntaImagem p4 = new PerguntaImagem();
        p4.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/cow.jpg"));
        p4.Respostas.add(new Resposta(false, "Hippo"));
        p4.Respostas.add(new Resposta(false, "Rhino"));
        p4.Respostas.add(new Resposta(false, "Bull"));
        p4.Respostas.add(new Resposta(true, "Cow"));
        
        PerguntaImagem p5 = new PerguntaImagem();
        p5.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/dog.jpg"));
        p5.Respostas.add(new Resposta(true, "Dog"));
        p5.Respostas.add(new Resposta(false, "Donkey"));
        p5.Respostas.add(new Resposta(false, "Sheep"));
        p5.Respostas.add(new Resposta(false, "Monkey"));
        
        PerguntaImagem p6 = new PerguntaImagem();
        p6.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/donkey.jpg"));
        p6.Respostas.add(new Resposta(false, "Pig"));
        p6.Respostas.add(new Resposta(false, "Turtle"));
        p6.Respostas.add(new Resposta(true, "Donkey"));
        p6.Respostas.add(new Resposta(false, "Rat"));
       
        PerguntaImagem p7 = new PerguntaImagem();
        p7.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/lion.jpg"));
        p7.Respostas.add(new Resposta(false, "Monkey"));
        p7.Respostas.add(new Resposta(true, "Lion"));
        p7.Respostas.add(new Resposta(false, "Dog"));
        p7.Respostas.add(new Resposta(false, "Donkey"));
        
        PerguntaImagem p8 = new PerguntaImagem();
        p8.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/monkey.jpg"));
        p8.Respostas.add(new Resposta(false, "Bull"));
        p8.Respostas.add(new Resposta(false, "Bunny"));
        p8.Respostas.add(new Resposta(false, "Cow"));
        p8.Respostas.add(new Resposta(true, "Monkey"));
        
        PerguntaImagem p9 = new PerguntaImagem();
        p9.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/pig.jpg"));
        p9.Respostas.add(new Resposta(true, "Pig"));
        p9.Respostas.add(new Resposta(false, "Turtle"));
        p9.Respostas.add(new Resposta(false, "Donkey"));
        p9.Respostas.add(new Resposta(false, "Bunny"));
        
        PerguntaImagem p10 = new PerguntaImagem();
        p10.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/rat.jpg"));
        p10.Respostas.add(new Resposta(false, "Turtle"));
        p10.Respostas.add(new Resposta(false, "Sheep"));
        p10.Respostas.add(new Resposta(true, "Rat"));
        p10.Respostas.add(new Resposta(false, "Monkey"));
        
        PerguntaImagem p11 = new PerguntaImagem();
        p11.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/rhino.jpg"));
        p11.Respostas.add(new Resposta(false, "Hippo"));
        p11.Respostas.add(new Resposta(true, "Rhino"));
        p11.Respostas.add(new Resposta(false, "Cow"));
        p11.Respostas.add(new Resposta(false, "Bull"));
        
        PerguntaImagem p12 = new PerguntaImagem();
        p12.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/sheep.jpg"));
        p12.Respostas.add(new Resposta(false, "Lion"));
        p12.Respostas.add(new Resposta(false, "Turtle"));
        p12.Respostas.add(new Resposta(false, "Cow"));
        p12.Respostas.add(new Resposta(true, "Sheep"));
        
        PerguntaImagem p13 = new PerguntaImagem();
        p13.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/animals/Turtle.jpg"));
        p13.Respostas.add(new Resposta(true, "Turtle"));
        p13.Respostas.add(new Resposta(false, "Bunny"));
        p13.Respostas.add(new Resposta(false, "Bull"));
        p13.Respostas.add(new Resposta(false, "Rhino"));
              
        this.PI.add(p0);
        this.PI.add(p1);
        this.PI.add(p2);
        this.PI.add(p3);
        this.PI.add(p4);
        this.PI.add(p5);
        this.PI.add(p6);
        this.PI.add(p7);
        this.PI.add(p8);
        this.PI.add(p9);
        this.PI.add(p10);
        this.PI.add(p11);
        this.PI.add(p12);
        this.PI.add(p13);    
    }
 
    public PerguntaImagem Random()
   {
        int index = randomGenerator.nextInt(this.PI.size());
        PerguntaImagem item = this.PI.get(index);
        return item;
    }
    
    public class PerguntaImagem
    {
        ImageIcon imagens;
        List<Resposta> Respostas = new ArrayList<Resposta>();
    }
    
    public class Resposta
    {
        boolean Correta;
        String Texto;
        
        public Resposta(boolean Correta, String Texto)
        {
            this.Correta = Correta;
            this.Texto = Texto;
        }
        
        public boolean isCorreta() {
            return Correta;
        }
        public void setCorreta(boolean Correta) {
            this.Correta = Correta;
        }
        
        public String getTexto() {
            return Texto;
        }
        public void setTexto(String Texto) {
            this.Texto = Texto;
        }
    }
}

