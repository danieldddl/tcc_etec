package View;


import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;


public class PerguntasComida {
    
    private Random randomGenerator;
    public List<PerguntaImagem> PI = new ArrayList<PerguntaImagem>();
    
    public PerguntasComida()
    {
        
        this.randomGenerator = new Random();
      
        PerguntaImagem p0 = new PerguntaImagem();
        p0.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/apple.jpg"));
        p0.Respostas.add(new Resposta(false, "Banama"));
        p0.Respostas.add(new Resposta(false, "Lemon"));
        p0.Respostas.add(new Resposta(true, "Apple"));
        p0.Respostas.add(new Resposta(false, "Chocolate"));
        
        PerguntaImagem p1 = new PerguntaImagem();
        p1.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food//banana.jpg"));
        p1.Respostas.add(new Resposta(false, "Cupcake"));
        p1.Respostas.add(new Resposta(false, "Apple"));
        p1.Respostas.add(new Resposta(false, "Chips"));
        p1.Respostas.add(new Resposta(true, "Banana"));
        
        PerguntaImagem p2 = new PerguntaImagem();
        p2.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/chips.jpg"));
        p2.Respostas.add(new Resposta(false, "Burguer"));
        p2.Respostas.add(new Resposta(false, "Coke"));
        p2.Respostas.add(new Resposta(true, "Chips"));
        p2.Respostas.add(new Resposta(false, "Eggs"));
        
        PerguntaImagem p3 = new PerguntaImagem();
        p3.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/chocolate.jpg"));
        p3.Respostas.add(new Resposta(false, "Ice Cream"));
        p3.Respostas.add(new Resposta(true, "Chocolate"));
        p3.Respostas.add(new Resposta(false, "Hot Dog"));
        p3.Respostas.add(new Resposta(false, "Orange"));
        
        PerguntaImagem p4 = new PerguntaImagem();
        p4.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/coke.jpg"));
        p4.Respostas.add(new Resposta(false, "Pancakes"));
        p4.Respostas.add(new Resposta(false, "Juice"));
        p4.Respostas.add(new Resposta(false, "Tomato"));
        p4.Respostas.add(new Resposta(true, "Refrigerant"));
        
        PerguntaImagem p5 = new PerguntaImagem();
        p5.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/cupcake.jpg"));
        p5.Respostas.add(new Resposta(true, "Cupcake"));
        p5.Respostas.add(new Resposta(false, "Strawberry"));
        p5.Respostas.add(new Resposta(false, "Ice cream"));
        p5.Respostas.add(new Resposta(false, "Radish"));
        
        PerguntaImagem p6 = new PerguntaImagem();
        p6.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/eggs.jpg"));
        p6.Respostas.add(new Resposta(false, "Onion"));
        p6.Respostas.add(new Resposta(false, "Tomato"));
        p6.Respostas.add(new Resposta(true, "Eggs"));
        p6.Respostas.add(new Resposta(false, "Apple"));
        
        PerguntaImagem p7 = new PerguntaImagem();
        p7.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/hamburger.jpg"));
        p7.Respostas.add(new Resposta(false, "Pintão"));
        p7.Respostas.add(new Resposta(true, "Burger"));
        p7.Respostas.add(new Resposta(false, "Hot Dog"));
        p7.Respostas.add(new Resposta(false, "Radish"));
        
        PerguntaImagem p8 = new PerguntaImagem();
        p8.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/hotdog.jpg"));
        p8.Respostas.add(new Resposta(false, "Lemon"));
        p8.Respostas.add(new Resposta(false, "Banana"));
        p8.Respostas.add(new Resposta(false, "Cupcake"));
        p8.Respostas.add(new Resposta(true, "Hot Dog"));
        
        PerguntaImagem p9 = new PerguntaImagem();
        p9.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/icecream.jpg"));
        p9.Respostas.add(new Resposta(true, "Ice Cream"));
        p9.Respostas.add(new Resposta(false, "Eggs"));
        p9.Respostas.add(new Resposta(false, "Chips"));
        p9.Respostas.add(new Resposta(false, "Strawberry"));
        
        PerguntaImagem p10 = new PerguntaImagem();
        p10.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/juice.jpg"));
        p10.Respostas.add(new Resposta(false, "Coke"));
        p10.Respostas.add(new Resposta(false, "Chocolate"));
        p10.Respostas.add(new Resposta(true, "Juice"));
        p10.Respostas.add(new Resposta(false, "Ice Cream"));
        
        PerguntaImagem p11 = new PerguntaImagem();
        p11.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/lemon.jpg"));
        p11.Respostas.add(new Resposta(false, "Orange"));
        p11.Respostas.add(new Resposta(true, "Lemon"));
        p11.Respostas.add(new Resposta(false, "Strawberry"));
        p11.Respostas.add(new Resposta(false, "Radish"));
               
        PerguntaImagem p12 = new PerguntaImagem();
        p12.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/orange.jpg"));
        p12.Respostas.add(new Resposta(false, "Eggs"));
        p12.Respostas.add(new Resposta(false, "Banana"));
        p12.Respostas.add(new Resposta(false, "Coke"));
        p12.Respostas.add(new Resposta(true, "Orange"));
        
        PerguntaImagem p13 = new PerguntaImagem();
        p13.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/pancake.jpg"));
        p13.Respostas.add(new Resposta(true, "Pancakes"));
        p13.Respostas.add(new Resposta(false, "Cupcake"));
        p13.Respostas.add(new Resposta(false, "Onion"));
        p13.Respostas.add(new Resposta(false, "Tomato"));
        
        PerguntaImagem p14 = new PerguntaImagem();
        p14.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/radish.jpg"));
        p14.Respostas.add(new Resposta(false, "Juice"));
        p14.Respostas.add(new Resposta(false, "Burguer"));
        p14.Respostas.add(new Resposta(true, "Radish"));
        p14.Respostas.add(new Resposta(false, "Chips"));
        
        PerguntaImagem p15 = new PerguntaImagem();
        p15.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/food/strawberry.jpg"));
        p15.Respostas.add(new Resposta(false, "Burguer"));
        p15.Respostas.add(new Resposta(false, "Pancakes"));
        p15.Respostas.add(new Resposta(false, "Apple"));
        p15.Respostas.add(new Resposta(true, "Strawberry"));
        
        this.PI.add(p0);
        this.PI.add(p1);
        this.PI.add(p2);
        this.PI.add(p3);
        this.PI.add(p4);
        this.PI.add(p5);
        this.PI.add(p6);
        this.PI.add(p7);
        this.PI.add(p8);
        this.PI.add(p9);
        this.PI.add(p10);
        this.PI.add(p11);
        this.PI.add(p12);
        this.PI.add(p13);    
        this.PI.add(p14);
        this.PI.add(p15);
    }
 
    public PerguntaImagem Random()
   {
        int index = randomGenerator.nextInt(this.PI.size());
        PerguntaImagem item = this.PI.get(index);
        return item;
    }
    
    public class PerguntaImagem
    {
        ImageIcon imagens;
        List<Resposta> Respostas = new ArrayList<Resposta>();

    }
    
    public class Resposta
    {
        boolean Correta;
        String Texto;
        
        public Resposta(boolean Correta, String Texto)
        {
            this.Correta = Correta;
            this.Texto = Texto;
        }
        
        public boolean isCorreta() {
            return Correta;
        }
        public void setCorreta(boolean Correta) {
            this.Correta = Correta;
        }
        
        public String getTexto() {
            return Texto;
        }
        public void setTexto(String Texto) {
            this.Texto = Texto;
        }
    }
}

