package View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.swing.ImageIcon;


public class PerguntasObjetos {
    
    private Random randomGenerator;
    public List<PerguntaImagem> PI = new ArrayList<PerguntaImagem>();
    
    public PerguntasObjetos()
    {
        
        this.randomGenerator = new Random();
      
        PerguntaImagem p0 = new PerguntaImagem();
        p0.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/book.jpg"));
        p0.Respostas.add(new Resposta(true, "Book"));
        p0.Respostas.add(new Resposta(false, "Bucket"));
        p0.Respostas.add(new Resposta(false, "Cellphone"));
        p0.Respostas.add(new Resposta(false, "Clock"));
        
        PerguntaImagem p1 = new PerguntaImagem();
        p1.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/bucket.jpg"));
        p1.Respostas.add(new Resposta(false, "Locker"));
        p1.Respostas.add(new Resposta(false, "Hoe"));
        p1.Respostas.add(new Resposta(false, "Medal"));
        p1.Respostas.add(new Resposta(true, "Bucket"));
        
        PerguntaImagem p2 = new PerguntaImagem();
        p2.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/cellphone.jpg"));
        p2.Respostas.add(new Resposta(false, "Paperroll"));
        p2.Respostas.add(new Resposta(false, "Pill"));
        p2.Respostas.add(new Resposta(true, "Cellphone"));
        p2.Respostas.add(new Resposta(false, "Mug"));
        
        PerguntaImagem p3 = new PerguntaImagem();
        p3.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/clock.jpg"));
        p3.Respostas.add(new Resposta(false, "Plunger"));
        p3.Respostas.add(new Resposta(true, "Clock"));
        p3.Respostas.add(new Resposta(false, "Shoe"));
        p3.Respostas.add(new Resposta(false, "Shorts"));
                       
        PerguntaImagem p4 = new PerguntaImagem();
        p4.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/locker.jpg"));
        p4.Respostas.add(new Resposta(false, "Shoe"));
        p4.Respostas.add(new Resposta(false, "hoe"));
        p4.Respostas.add(new Resposta(false, "Clock"));
        p4.Respostas.add(new Resposta(true, "Locker"));
        
        PerguntaImagem p5 = new PerguntaImagem();
        p5.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/medal.jpg"));
        p5.Respostas.add(new Resposta(false, "Walkman"));
        p5.Respostas.add(new Resposta(false, "Shorts"));
        p5.Respostas.add(new Resposta(true, "Medal"));
        p5.Respostas.add(new Resposta(false, "Locker"));
        
        PerguntaImagem p6 = new PerguntaImagem();
        p6.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/mug.jpg"));
        p6.Respostas.add(new Resposta(false, "Book"));
        p6.Respostas.add(new Resposta(true, "Mug"));
        p6.Respostas.add(new Resposta(false, "Bucket"));
        p6.Respostas.add(new Resposta(false, "Paperroll"));
       
        PerguntaImagem p7 = new PerguntaImagem();
        p7.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/paperroll.jpg"));
        p7.Respostas.add(new Resposta(false, "Plunger"));
        p7.Respostas.add(new Resposta(false, "Hoe"));
        p7.Respostas.add(new Resposta(false, "Clock"));
        p7.Respostas.add(new Resposta(true, "Paperroll"));
        
        PerguntaImagem p8 = new PerguntaImagem();
        p8.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/pill.jpg"));
        p8.Respostas.add(new Resposta(true, "Pill"));
        p8.Respostas.add(new Resposta(false, "Soundbox"));
        p8.Respostas.add(new Resposta(false, "Cellphone"));
        p8.Respostas.add(new Resposta(false, "Book"));
        
        PerguntaImagem p9 = new PerguntaImagem();
        p9.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/shoes.jpg"));
        p9.Respostas.add(new Resposta(false, "Shorts"));
        p9.Respostas.add(new Resposta(false, "Mug"));
        p9.Respostas.add(new Resposta(false, "Book"));
        p9.Respostas.add(new Resposta(true, "Shoe"));
        
        PerguntaImagem p10 = new PerguntaImagem();
        p10.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/shorts.jpg"));
        p10.Respostas.add(new Resposta(true, "Shorts"));
        p10.Respostas.add(new Resposta(false, "Hoe"));
        p10.Respostas.add(new Resposta(false, "Clock"));
        p10.Respostas.add(new Resposta(false, "Bucket"));
        
        PerguntaImagem p11 = new PerguntaImagem();
        p11.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/soundbox.jpg"));
        p11.Respostas.add(new Resposta(false, "Plunger"));
        p11.Respostas.add(new Resposta(true, "Soundbox"));
        p11.Respostas.add(new Resposta(false, "Medal"));
        p11.Respostas.add(new Resposta(false, "Bucket"));
        
        PerguntaImagem p12 = new PerguntaImagem();
        p12.imagens = new ImageIcon(getClass().getResource("/Imagens/silhuetas/objects/walkman.jpg"));
        p12.Respostas.add(new Resposta(true, "Walkman"));
        p12.Respostas.add(new Resposta(false, "Medal"));
        p12.Respostas.add(new Resposta(false, "Mug"));
        p12.Respostas.add(new Resposta(false, "Locker"));
               
        this.PI.add(p0);
        this.PI.add(p1);
        this.PI.add(p2);
        this.PI.add(p3);
        this.PI.add(p4);
        this.PI.add(p5);
        this.PI.add(p6);
        this.PI.add(p7);
        this.PI.add(p8);
        this.PI.add(p9);
        this.PI.add(p10);
        this.PI.add(p11);  
        this.PI.add(p12);
    }
 
    public PerguntaImagem Random()
   {
        int index = randomGenerator.nextInt(this.PI.size());
        PerguntaImagem item = this.PI.get(index);
        return item;
    }
    
    public class PerguntaImagem
    {
        ImageIcon imagens;
        List<Resposta> Respostas = new ArrayList<Resposta>();

    }
    
    public class Resposta
    {
        boolean Correta;
        String Texto;
        
        public Resposta(boolean Correta, String Texto)
        {
            this.Correta = Correta;
            this.Texto = Texto;
        }
        
        public boolean isCorreta() {
            return Correta;
        }
        public void setCorreta(boolean Correta) {
            this.Correta = Correta;
        }
        
        public String getTexto() {
            return Texto;
        }
        public void setTexto(String Texto) {
            this.Texto = Texto;
        }
    }
}

