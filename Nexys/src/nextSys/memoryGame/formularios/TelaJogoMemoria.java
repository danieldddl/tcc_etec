package nextSys.memoryGame.formularios;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import nextSys.memoryGame.funcionalidades.Configuracoes;
import nextSys.memoryGame.funcionalidades.MyButtonCarta;
import nextSys.memoryGame.funcionalidades.Pontuacao;
import nextSys.sistemaGeral.formularios.CaixaMensagem;
import nextSys.sistemaGeral.formularios.TelaFaseDois;
import nextSys.sistemaGeral.formularios.TelaFaseTres;
import nextSys.sistemaGeral.formularios.TelaFaseUm;
import nextSys.sistemaGeral.funcionalidades.Reprodutor;

/**
 *
 * @author Daniel
 */
public class TelaJogoMemoria extends javax.swing.JFrame {

    Pontuacao p = new Pontuacao();
    JLabel pontuacao, pontuacaoTexto, acertos, acertosTexto, erros, errosTexto, 
           acertosEmStreak, acertosEmStreakTexto, errosEmStreak, errosEmStreakTexto;
    
    JLabel lblImagemUltimaCarta, lblNomeUltimaCarta;
    JButton btnReproduzirUltimaCarta;
    
    Icon iconVoltarMenuOpcoes, iconUltimaCarta, iconReproduzirSom;
    
    URL audio;
    Reprodutor r = new Reprodutor();
    private int descFase;
    
    public TelaJogoMemoria(final int descFase) throws IOException {
        
    	this.descFase = descFase;
    	
    	initComponents();
        
        setSize(1030,660);
        setResizable(false);
        setLocationRelativeTo(null);
        setLayout(null);
        
        try {
        	//setando o background
        	if (descFase == 1){
                setContentPane(new JLabel (new ImageIcon (ImageIO.read(
                        new File("res/imagens/menus/backgrounds/background_facil.png")))));
        	} else if (descFase == 2){
        		setContentPane(new JLabel (new ImageIcon (ImageIO.read(
                        new File("res/imagens/menus/backgrounds/background_medio.png")))));
        	} else if (descFase == 3){
        		setContentPane(new JLabel (new ImageIcon (ImageIO.read(
                        new File("res/imagens/menus/backgrounds/background_dificil.png")))));
        	}
            

            Image imagemVoltarMenuOpcoes = ImageIO.read(
                    new File("res/imagens/memoryGame/botoes/back_to_menu.png"));
            iconVoltarMenuOpcoes = new ImageIcon (imagemVoltarMenuOpcoes.getScaledInstance(
                    130, 35, Image.SCALE_DEFAULT));
            
            Image imagemUltimaCarta = ImageIO.read(
                    new File("res/imagens/memoryGame/botoes/sem_ultimaImagem.png"));
            iconUltimaCarta = new ImageIcon (imagemUltimaCarta.getScaledInstance(
                    100, 100, Image.SCALE_DEFAULT));
            
            Image imagemBotaoUltima = ImageIO.read(
                    new File("res/imagens/memoryGame/botoes/som.png"));
            iconReproduzirSom = new ImageIcon (imagemBotaoUltima.getScaledInstance(
                    100, 100, Image.SCALE_DEFAULT));
        
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        JButton botaoFechar = new JButton("");
        botaoFechar.setBorderPainted(true);
        botaoFechar.setBorder(null);
        botaoFechar.setContentAreaFilled(false);
        botaoFechar.setIcon(new ImageIcon (ImageIO.read(
                new File("res/imagens/memoryGame/botoes/close.png"))));
        botaoFechar.setPressedIcon(new ImageIcon (
                ImageIO.read(new File("res/imagens/memoryGame/botoes/close-pressed.png"))));
        botaoFechar.setBounds(980, 10, 40, 40);
        botaoFechar.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                CaixaMensagem cm = new CaixaMensagem();
                cm.mostrarPergunta("Do you REALLY want to leave Nexys? D:");
                int resposta = cm.getEscolhido();

                if (resposta == 1){
                    System.exit(0);
                } else {
                    
                }
                
            }
            
        });
        add(botaoFechar);
        
        JButton botaoMinimizar = new JButton("");
        botaoMinimizar.setBorderPainted(true);
        botaoMinimizar.setBorder(null);
        botaoMinimizar.setContentAreaFilled(false);
        botaoMinimizar.setIcon(new ImageIcon (ImageIO.read(
                new File("res/imagens/memoryGame/botoes/minimize.png"))));
        botaoMinimizar.setPressedIcon(new ImageIcon (ImageIO.read(
                new File("res/imagens/memoryGame/botoes/minimize-pressed.png"))));
        botaoMinimizar.setBounds(930, 10, 40, 40);
        botaoMinimizar.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                setState(TelaJogoMemoria.ICONIFIED);
            }
            
        });
        add(botaoMinimizar);
        
        int posicionaTextox = 110;
        int posicionaTextoy = 120;
        
        /*pontuacao = new JLabel (String.valueOf(p.getPontuacao()));
        pontuacao.setSize(pontuacao.getPreferredSize());
        pontuacao.setLocation(860+posicionaTextox, 270+posicionaTextoy);
        add(pontuacao);
        
        pontuacaoTexto = new JLabel ("Score: ");
        pontuacaoTexto.setSize(pontuacaoTexto.getPreferredSize());
        pontuacaoTexto.setLocation(pontuacao.getX() - pontuacaoTexto.getWidth() - 10 ,pontuacao.getY());
        add(pontuacaoTexto);
        
        acertos = new JLabel (String.valueOf(p.getNumAcertos()));
        acertos.setSize(acertos.getPreferredSize());
        acertos.setLocation(pontuacao.getX(), pontuacao.getY() + 20);
        add(acertos);
        
        acertosTexto = new JLabel ("Matched Pairs: ");
        acertosTexto.setSize(acertosTexto.getPreferredSize());
        acertosTexto.setLocation(acertos.getX() - acertosTexto.getWidth() - 10, acertos.getY());
        add(acertosTexto);
        
        acertosEmStreak = new JLabel ("" + p.getNumAcertosStreak());
        acertosEmStreak.setSize(acertosEmStreak.getPreferredSize());
        acertosEmStreak.setLocation(acertos.getX(), acertos.getY() + 20);
        add(acertosEmStreak);
        
        acertosEmStreakTexto = new JLabel ("Matched pairs in a row: ");
        acertosEmStreakTexto.setSize(acertosEmStreakTexto.getPreferredSize());
        acertosEmStreakTexto.setLocation(acertosEmStreak.getX() - acertosEmStreakTexto.getWidth() - 10, 
                                         acertosEmStreak.getY());
        add(acertosEmStreakTexto);
        
        erros = new JLabel ("" + p.getNumErros());
        erros.setSize(erros.getPreferredSize());
        erros.setLocation(acertosEmStreak.getX(), acertosEmStreak.getY() + 20);
        add(erros);
        
        errosTexto = new JLabel ("Missed shots ");
        errosTexto.setSize(errosTexto.getPreferredSize());
        errosTexto.setLocation(erros.getX() - errosTexto.getWidth() - 10, erros.getY());
        add(errosTexto);
        
        errosEmStreak = new JLabel ("" + p.getNumErrosStreak());
        errosEmStreak.setSize(errosEmStreak.getPreferredSize());
        errosEmStreak.setLocation(erros.getX(), erros.getY() + 20);
        add(errosEmStreak);
        
        errosEmStreakTexto = new JLabel ("Missed shots in a row");
        errosEmStreakTexto.setSize(errosEmStreakTexto.getPreferredSize());
        errosEmStreakTexto.setLocation(errosEmStreak.getX() - errosEmStreakTexto.getWidth() - 10,
                                       errosEmStreak.getY());
        add(errosEmStreakTexto);*/
        
        lblImagemUltimaCarta = new JLabel ();
        lblImagemUltimaCarta.setSize(100,100);
        lblImagemUltimaCarta.setLocation(855, 100);
        lblImagemUltimaCarta.setIcon(iconUltimaCarta);
        add(lblImagemUltimaCarta);
        
        btnReproduzirUltimaCarta = new JButton();
        btnReproduzirUltimaCarta.setBorderPainted(true);
        btnReproduzirUltimaCarta.setBorder(null);
        btnReproduzirUltimaCarta.setContentAreaFilled(false);
        btnReproduzirUltimaCarta.setSize(100,100);
        btnReproduzirUltimaCarta.setLocation(lblImagemUltimaCarta.getX(),
                                             lblImagemUltimaCarta.getY() + 100 + 10);
        btnReproduzirUltimaCarta.setIcon(iconReproduzirSom);
        btnReproduzirUltimaCarta.addActionListener(new ActionListener() {

        
            @Override
            public void actionPerformed(ActionEvent e) {
                //para não para o programa durante a reprodução do som 
                Thread threadSom = new Thread (){

                    @Override
                    public void run() {
                        super.run(); 
                        try{
                            File file = new File("res/sons/memoryGame/cartas/" + Configuracoes.getInstance().getTemaFiguras() + "/" 
                                + Configuracoes.getInstance().getLayoutJogoPlay().getPosicaoCartas(
                                        Configuracoes.getInstance().getLayoutJogoPlay().getCartasViradas(0, 0)   
                                 ,Configuracoes.getInstance().getLayoutJogoPlay().getCartasViradas(0, 1)) + ".wav");
                            URL url = file.toURL();
                            r.reproduzirSom(url);
                        } catch(Exception e){
                            System.out.println(e.getMessage());
                        }
                    }
                    
                };
                
                threadSom.start();
            }
        });
        add(btnReproduzirUltimaCarta);
        
        
        switch (Configuracoes.getInstance().getDificuldade()){
            case 1:
                posicionaBotoesUm();
                break;
            case 2:
                posicionaBotoesDois();
                break;
            case 3:
                posicionaBotoesTres();
                break;
            case 4:
                posicionaBotoesQuatro();
                break;
        }
        
        JButton voltarMenuOpcoes = new JButton();
        voltarMenuOpcoes.setBorder(null);
        voltarMenuOpcoes.setBorderPainted(true);
        voltarMenuOpcoes.setContentAreaFilled(false);
        voltarMenuOpcoes.setIcon(iconVoltarMenuOpcoes);
        voltarMenuOpcoes.setLocation(840, 520);
        voltarMenuOpcoes.setSize(voltarMenuOpcoes.getPreferredSize());
        voltarMenuOpcoes.addActionListener(new ActionListener() {
 
            @Override
            public void actionPerformed(ActionEvent e) {
                switch (descFase){
                	case 1:
                		TelaFaseUm t = new TelaFaseUm(1);
                		t.setVisible(true);
                		break;
                	case 2:
                		TelaFaseDois t2 = new TelaFaseDois(2);
                		t2.setVisible(true);
                		break;
                	case 3:
                		TelaFaseTres t3 = new TelaFaseTres(3);
                		t3.setVisible(true);
                		break;
                }
                
                dispose();
            }
        });
        add(voltarMenuOpcoes);
               
        setVisible(true);
    }
    
    /*public void atualizarLabels (){
        
        pontuacao.setText (p.getPontuacao() + "");
        pontuacao.setSize(pontuacao.getPreferredSize());
        
        acertos.setText (p.getNumAcertos() + "");
        acertos.setSize(acertos.getPreferredSize());
        
        erros.setText(p.getNumErros() + "");
        erros.setSize(erros.getPreferredSize());
        
        acertosEmStreak.setText(p.getNumAcertosStreak() + "");
        acertosEmStreak.setSize(acertosEmStreak.getPreferredSize());
        
        errosEmStreak.setText(p.getNumErrosStreak() + "");
        errosEmStreak.setSize(erros.getPreferredSize());
    }*/
    
    public void atualizarUltimaCarta(Image imagemUltimaCarta){
        try {
            iconUltimaCarta = new ImageIcon (imagemUltimaCarta.getScaledInstance(
                100, 100, Image.SCALE_DEFAULT));
        
            lblImagemUltimaCarta.setIcon(iconUltimaCarta);
        } catch (Exception e){
            //System.out.println (e.getMessage());
        }
    }
    
    public void atualizarSom (URL audio){
        this.audio = audio;
    }
    
    public void posicionaBotoesUm () throws IOException{
        //3x2
        MyButtonCarta carta1 = new MyButtonCarta(35, 88, 225, 225, 0, 0, p, this);
        add(carta1);        
        
        MyButtonCarta carta2 = new MyButtonCarta(295, 88, 225, 225, 0, 1, p, this);
        add(carta2);
        
        MyButtonCarta carta3 = new MyButtonCarta(555, 88, 225, 225, 1, 0, p, this);
        add(carta3);
        
        MyButtonCarta carta4 = new MyButtonCarta(35, 348, 225, 225, 1, 1, p, this);
        add(carta4);
        
        MyButtonCarta carta5 = new MyButtonCarta(295, 348, 225, 225, 2, 0, p, this);
        add(carta5);
        
        MyButtonCarta carta6 = new MyButtonCarta(555, 348, 225, 225, 2, 1, p, this);
        add(carta6);
    }
    
    public void posicionaBotoesDois() throws IOException{
        //4x3
        MyButtonCarta carta1 = new MyButtonCarta(20, 48, 175, 175, 0, 0, p, this);
        add(carta1);        
        
        MyButtonCarta carta2 = new MyButtonCarta(215, 48, 175, 175, 0, 1, p, this);
        add(carta2);
        
        MyButtonCarta carta3 = new MyButtonCarta(410, 48, 175, 175, 0, 2, p, this);
        add(carta3);
        
        MyButtonCarta carta4 = new MyButtonCarta(605, 48, 175, 175, 1, 0, p, this);
        add(carta4);
        
        MyButtonCarta carta5 = new MyButtonCarta(20, 243, 175, 175, 1, 1, p, this);
        add(carta5);
        
        MyButtonCarta carta6 = new MyButtonCarta(215, 243, 175, 175, 1, 2, p, this);
        add(carta6);
        
        MyButtonCarta carta7 = new MyButtonCarta(410, 243, 175, 175, 2, 0, p, this);
        add(carta7);        
        
        MyButtonCarta carta8 = new MyButtonCarta(605, 243, 175, 175, 2, 1, p, this);
        add(carta8);
        
        MyButtonCarta carta9 = new MyButtonCarta(20, 438, 175, 175, 2, 2, p, this);
        add(carta9);
        
        MyButtonCarta carta10 = new MyButtonCarta(215, 438, 175, 175, 3, 0, p, this);
        add(carta10);
        
        MyButtonCarta carta11 = new MyButtonCarta(410, 438, 175, 175, 3, 1, p, this);
        add(carta11);
        
        MyButtonCarta carta12 = new MyButtonCarta(605, 438, 175, 175, 3, 2, p, this);
        add(carta12);
    }
    
    public void posicionaBotoesTres() throws IOException{
        //5x4
        MyButtonCarta carta1 = new MyButtonCarta(15, 28, 140, 140, 0, 0, p, this);
        add(carta1);        
        
        MyButtonCarta carta2 = new MyButtonCarta(170, 28, 140, 140, 0, 1, p, this);
        add(carta2);
        
        MyButtonCarta carta3 = new MyButtonCarta(325, 28, 140, 140, 0, 2, p, this);
        add(carta3);
        
        MyButtonCarta carta4 = new MyButtonCarta(480, 28, 140, 140, 0, 3, p, this);
        add(carta4);
        
        MyButtonCarta carta5 = new MyButtonCarta(635, 28, 140, 140, 1, 0, p, this);
        add(carta5);
        
        MyButtonCarta carta6 = new MyButtonCarta(15, 183, 140, 140, 1, 1, p, this);
        add(carta6);
        
        MyButtonCarta carta7 = new MyButtonCarta(170, 183, 140, 140, 1, 2, p, this);
        add(carta7);        
        
        MyButtonCarta carta8 = new MyButtonCarta(325, 183, 140, 140, 1, 3, p, this);
        add(carta8);
        
        MyButtonCarta carta9 = new MyButtonCarta(480, 183, 140, 140, 2, 0, p, this);
        add(carta9);
        
        MyButtonCarta carta10 = new MyButtonCarta(635, 183, 140, 140, 2, 1, p, this);
        add(carta10);
        
        MyButtonCarta carta11 = new MyButtonCarta(15, 338, 140, 140, 2, 2, p, this);
        add(carta11);
        
        MyButtonCarta carta12 = new MyButtonCarta(170, 338, 140, 140, 2, 3, p, this);
        add(carta12);
        
        MyButtonCarta carta13 = new MyButtonCarta(325, 338, 140, 140, 3, 0, p, this);
        add(carta13);        
        
        MyButtonCarta carta14 = new MyButtonCarta(480, 338, 140, 140, 3, 1, p, this);
        add(carta14);
        
        MyButtonCarta carta15 = new MyButtonCarta(635, 338, 140, 140, 3, 2, p, this);
        add(carta15);
        
        MyButtonCarta carta16 = new MyButtonCarta(15, 493, 140, 140, 3, 3, p, this);
        add(carta16);
        
        MyButtonCarta carta17 = new MyButtonCarta(170, 493, 140, 140, 4, 0, p, this);
        add(carta17);
        
        MyButtonCarta carta18 = new MyButtonCarta(325, 493, 140, 140, 4, 1, p, this);
        add(carta18);
        
        MyButtonCarta carta19 = new MyButtonCarta(480, 493, 140, 140, 4, 2, p, this);
        add(carta19);
        
        MyButtonCarta carta20 = new MyButtonCarta(635, 493, 140, 140, 4, 3, p, this);
        add(carta20);
    }
    
    public void posicionaBotoesQuatro() throws IOException{
        //6x5
        MyButtonCarta carta1 = new MyButtonCarta(10, 10, 120, 120, 0, 0, p, this);
        add(carta1);        
        
        MyButtonCarta carta2 = new MyButtonCarta(140, 10, 120, 120, 0, 1, p, this);
        add(carta2);
        
        MyButtonCarta carta3 = new MyButtonCarta(270, 10, 120, 120, 0, 2, p, this);
        add(carta3);
        
        MyButtonCarta carta4 = new MyButtonCarta(400, 10, 120, 120, 0, 3, p, this);
        add(carta4);
        
        MyButtonCarta carta5 = new MyButtonCarta(530, 10, 120, 120, 0, 4, p, this);
        add(carta5);
        
        MyButtonCarta carta6 = new MyButtonCarta(660, 10, 120, 120, 1, 0, p, this);
        add(carta6);
        
        MyButtonCarta carta7 = new MyButtonCarta(10, 140, 120, 120, 1, 1, p, this);
        add(carta7);        
        
        MyButtonCarta carta8 = new MyButtonCarta(140, 140, 120, 120, 1, 2, p, this);
        add(carta8);
        
        MyButtonCarta carta9 = new MyButtonCarta(270, 140, 120, 120, 1, 3, p, this);
        add(carta9);
        
        MyButtonCarta carta10 = new MyButtonCarta(400, 140, 120, 120, 1, 4, p, this);
        add(carta10);
        
        MyButtonCarta carta11 = new MyButtonCarta(530, 140, 120, 120, 2, 0, p, this);
        add(carta11);
        
        MyButtonCarta carta12 = new MyButtonCarta(660, 140, 120, 120, 2, 1, p, this);
        add(carta12);
        
        MyButtonCarta carta13 = new MyButtonCarta(10, 270, 120, 120, 2, 2, p, this);
        add(carta13);        
        
        MyButtonCarta carta14 = new MyButtonCarta(140, 270, 120, 120, 2, 3, p, this);
        add(carta14);
        
        MyButtonCarta carta15 = new MyButtonCarta(270, 270, 120, 120, 2, 4, p, this);
        add(carta15);
        
        MyButtonCarta carta16 = new MyButtonCarta(400, 270, 120, 120, 3, 0, p, this);
        add(carta16);
        
        MyButtonCarta carta17 = new MyButtonCarta(530, 270, 120, 120, 3, 1, p, this);
        add(carta17);
        
        MyButtonCarta carta18 = new MyButtonCarta(660, 270, 120, 120, 3, 2, p, this);
        add(carta18);
        
        MyButtonCarta carta19 = new MyButtonCarta(10, 400, 120, 120, 3, 3, p, this);
        add(carta19);
        
        MyButtonCarta carta20 = new MyButtonCarta(140, 400, 120, 120, 3, 4, p, this);
        add(carta20);
        
        MyButtonCarta carta21 = new MyButtonCarta(270, 400, 120, 120, 4, 0, p, this);
        add(carta21);
        
        MyButtonCarta carta22 = new MyButtonCarta(400, 400, 120, 120, 4, 1, p, this);
        add(carta22);
        
        MyButtonCarta carta23 = new MyButtonCarta(530, 400, 120, 120, 4, 2, p, this);
        add(carta23);        
        
        MyButtonCarta carta24 = new MyButtonCarta(660, 400, 120, 120, 4, 3, p, this);
        add(carta24);
        
        MyButtonCarta carta25 = new MyButtonCarta(10, 530, 120, 120, 4, 4, p, this);
        add(carta25);
        
        MyButtonCarta carta26 = new MyButtonCarta(140, 530, 120, 120, 5, 0, p, this);
        add(carta26);
        
        MyButtonCarta carta27 = new MyButtonCarta(270, 530, 120, 120, 5, 1, p, this);
        add(carta27);
        
        MyButtonCarta carta28 = new MyButtonCarta(400, 530, 120, 120, 5, 2, p, this);
        add(carta28);
        
        MyButtonCarta carta29 = new MyButtonCarta(530, 530, 120, 120, 5, 3, p, this);
        add(carta29);
        
        MyButtonCarta carta30 = new MyButtonCarta(660, 530, 120, 120, 5, 4, p, this);
        add(carta30);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaJogoMemoria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaJogoMemoria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaJogoMemoria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaJogoMemoria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TelaJogoMemoria(1).setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(TelaJogoMemoria.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

	public int getDescFase() {
		return descFase;
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
