package nextSys.memoryGame.formularios;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import nextSys.memoryGame.funcionalidades.Configuracoes;
import nextSys.sistemaGeral.formularios.TelaMenuPrincipal;
import nextSys.sistemaGeral.funcionalidades.InformacoesUsuarioLogado;

/**
 *
 * @author Daniel
 */
public class TelaPreJogoMemoria extends javax.swing.JFrame {

    /**
     * Creates new form TelaPreJogoMemoria
     */
    
    private Boolean isDificuldadeEscolhida = false;
    private int dificuldadeEscolhida;
    
    private Boolean isTemaEscolhido = false;
    private String temaEscolhido;
    
    //IMAGENS - NÃƒO SELECIONADAS: NÃ�VEL
    private Image imagemOpcao3x2NaoSelecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/3x2unselected.png"));
    private Icon iconOpcao3x2NaoSelecionada = new ImageIcon (
            imagemOpcao3x2NaoSelecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemOpcao4x3NaoSelecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/4x3unselected.png"));
    private Icon iconOpcao4x3NaoSelecionada = new ImageIcon (
            imagemOpcao4x3NaoSelecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemOpcao5x4NaoSelecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/5x4unselected.png"));
    private Icon iconOpcao5x4NaoSelecionada = new ImageIcon (
            imagemOpcao5x4NaoSelecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemOpcao6x5NaoSelecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/6x5unselected.png"));
    private Icon iconOpcao6x5NaoSelecionada = new ImageIcon (
            imagemOpcao6x5NaoSelecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    //IMAGENS - SELECIONADAS: NÃ�VEL
    private Image imagemOpcao3x2Selecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/3x2selected.png"));
    private Icon iconOpcao3x2Selecionada = new ImageIcon (
            imagemOpcao3x2Selecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemOpcao4x3Selecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/4x3selected.png"));
    private Icon iconOpcao4x3Selecionada = new ImageIcon (
            imagemOpcao4x3Selecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemOpcao5x4Selecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/5x4selected.png"));
    private Icon iconOpcao5x4Selecionada = new ImageIcon (
            imagemOpcao5x4Selecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemOpcao6x5Selecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/6x5selected.png"));
    private Icon iconOpcao6x5Selecionada = new ImageIcon (
            imagemOpcao6x5Selecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    //IMAGENS - NÃƒO SELECIONADAS: TEMA
    private Image imagemObjetosNaoSelecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/objetosunselected.png"));
    private Icon iconObjetosNaoSelecionada = new ImageIcon (
            imagemObjetosNaoSelecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemAnimaisNaoSelecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/animaisunselected.png"));
    private Icon iconAnimaisNaoSelecionada = new ImageIcon (
            imagemAnimaisNaoSelecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemNada= ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/nothing.png"));
    private Icon iconNada = new ImageIcon (
            imagemNada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    //IMAGENS - SELECIONADAS: TEMA
    private Image imagemObjetosSelecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/objetosselected.png"));
    private Icon iconObjetosSelecionada = new ImageIcon (
            imagemObjetosSelecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemAnimaisSelecionada = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/opcoes/animaisselected.png"));
    private Icon iconAnimaisSelecionada = new ImageIcon (
            imagemAnimaisSelecionada.getScaledInstance(175, 175, Image.SCALE_DEFAULT));
    
    private Image imagemJogar = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/botoes/play.png"));
    private Icon iconJogar = new ImageIcon (
            imagemJogar.getScaledInstance(130, 35, Image.SCALE_DEFAULT));
    
    private Image imagemVoltarMenu = ImageIO.read (getClass().getResource(
                    "/MemoryGame/imagens/interface/botoes/main_menu.png"));
    private Icon iconVoltarMenu = new ImageIcon (
            imagemVoltarMenu.getScaledInstance(130, 35, Image.SCALE_DEFAULT));
    
    //BOTÃ•ES
    private JButton opcao3x2;
    private JButton opcao4x3;
    private JButton opcao5x4;
    private JButton opcao6x5;
    
    JButton opcaoTemaAnimais;
    JButton opcaoTemaObjetos;
    JButton opcaoTemaNada1;
    JButton opcaoTemaNada2;
    
    public TelaPreJogoMemoria() throws IOException {
        initComponents();
        
        Configuracoes.getInstance().zerandoTudo();
        
        setSize(1030,660);
        setResizable(false);
        setLocationRelativeTo(null);
        setLayout(null); 
                        
        //setando o background
        setContentPane(new JLabel (new ImageIcon (ImageIO.read(getClass().getResource(
                "/MemoryGame/imagens/interface/background/background-jogoMemoria.png")))));
        
        JLabel tituloForm = new JLabel ("");
        tituloForm.setIcon (new ImageIcon (getClass().getResource(
                "/MemoryGame/imagens/interface/titulos/tituloOpcoesJM.png")));
        tituloForm.setSize(600,70);
        //tituloForm.setLocation(170,15);
        tituloForm.setLocation(220,15);
        add(tituloForm);
        
        //BOTÃ•ES DE FECHAR E MININIZAR
        JButton botaoFechar = new JButton("");
        botaoFechar.setBorderPainted(true);
        botaoFechar.setBorder(null);
        botaoFechar.setContentAreaFilled(false);
        botaoFechar.setIcon(new ImageIcon (getClass().getResource(
                "/MemoryGame/imagens/interface/botoes/close.png")));
        botaoFechar.setPressedIcon(new ImageIcon (getClass().getResource(
                "/MemoryGame/imagens/interface/botoes/close-pressed.png")));
        botaoFechar.setBounds(980, 10, 40, 40);
        botaoFechar.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                UIManager.put("OptionPane.cancelButtonText", "I didn't mean to click in there... :D");  
                UIManager.put("OptionPane.noButtonText", "Oh! please, no!");  
                UIManager.put("OptionPane.yesButtonText", "Yeah... D:"); 

                int resposta = JOptionPane.showConfirmDialog(null, "Do you REALLY want to leave Nexys? D:?");

                if (resposta == 0){
                    System.exit(0);
                } else if (resposta == 1){
                    //nÃ£o
                } else {
                    //cancelar
                }
                
            }
            
        });
        add(botaoFechar);
        
        JButton botaoMinimizar = new JButton("");
        botaoMinimizar.setBorderPainted(true);
        botaoMinimizar.setBorder(null);
        botaoMinimizar.setContentAreaFilled(false);
        botaoMinimizar.setIcon(new ImageIcon (getClass().getResource(
                "/MemoryGame/imagens/interface/botoes/minimize.png")));
        botaoMinimizar.setPressedIcon(new ImageIcon (getClass().getResource(
                "/MemoryGame/imagens/interface/botoes/minimize-pressed.png")));
        botaoMinimizar.setBounds(930, 10, 40, 40);
        botaoMinimizar.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                setState(TelaPreJogoMemoria.ICONIFIED);
            }
            
        });
        add(botaoMinimizar);
        
        JLabel tituloDificuldade = new JLabel ("");
        tituloDificuldade.setIcon (new ImageIcon (getClass().getResource(
            "/MemoryGame/imagens/interface/titulos/tituloDificuldadeOpcoesJM.png")));
        tituloDificuldade.setSize(190,40);
        tituloDificuldade.setLocation(435,100);
        //tituloForm.setLocation(220,15);
        add(tituloDificuldade);
        
        opcao3x2 = new JButton("");
        opcao3x2.setBorderPainted(true);
        opcao3x2.setBorder(null);
        opcao3x2.setContentAreaFilled(false);
        opcao3x2.setIcon(iconOpcao3x2NaoSelecionada);
        opcao3x2.setBounds(175, 150, 175, 175);
        opcao3x2.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                isDificuldadeEscolhida = true;
                dificuldadeEscolhida = 1;
                
                //AlteraÃ§Ãµes visuais do clique
                opcao3x2.setIcon(iconOpcao3x2Selecionada);
                
                opcao4x3.setIcon(iconOpcao4x3NaoSelecionada);
                opcao5x4.setIcon(iconOpcao5x4NaoSelecionada);
                opcao6x5.setIcon(iconOpcao6x5NaoSelecionada);
            }
            
        });
        add(opcao3x2);
        
        opcao4x3 = new JButton("");
        opcao4x3.setBorderPainted(true);
        opcao4x3.setBorder(null);
        opcao4x3.setContentAreaFilled(false);
        opcao4x3.setIcon(iconOpcao4x3NaoSelecionada);
        opcao4x3.setBounds(360, 150, 175, 175);
        opcao4x3.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                isDificuldadeEscolhida = true;
                dificuldadeEscolhida = 2;
                
                opcao4x3.setIcon(iconOpcao4x3Selecionada);
                
                opcao3x2.setIcon(iconOpcao3x2NaoSelecionada);
                opcao5x4.setIcon(iconOpcao5x4NaoSelecionada);
                opcao6x5.setIcon(iconOpcao6x5NaoSelecionada);
            }
            
        });
        add(opcao4x3);
        
        opcao5x4 = new JButton("");
        opcao5x4.setBorderPainted(true);
        opcao5x4.setBorder(null);
        opcao5x4.setContentAreaFilled(false);
        opcao5x4.setIcon(iconOpcao5x4NaoSelecionada);
        opcao5x4.setBounds(545, 150, 175, 175);
        opcao5x4.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                isDificuldadeEscolhida = true;
                dificuldadeEscolhida = 3;
                
                opcao5x4.setIcon(iconOpcao5x4Selecionada);
                
                opcao3x2.setIcon(iconOpcao3x2NaoSelecionada);
                opcao4x3.setIcon(iconOpcao4x3NaoSelecionada);
                opcao6x5.setIcon(iconOpcao6x5NaoSelecionada);
            }
            
        });
        add(opcao5x4);
        
        opcao6x5 = new JButton("");
        opcao6x5.setBorderPainted(true);
        opcao6x5.setBorder(null);
        opcao6x5.setContentAreaFilled(false);
        opcao6x5.setIcon(iconOpcao6x5NaoSelecionada);
        opcao6x5.setBounds(730, 150, 175, 175);
        opcao6x5.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                isDificuldadeEscolhida = true;
                dificuldadeEscolhida = 4;
                
                opcao6x5.setIcon(iconOpcao6x5Selecionada);
                
                opcao3x2.setIcon(iconOpcao3x2NaoSelecionada);
                opcao4x3.setIcon(iconOpcao4x3NaoSelecionada);
                opcao5x4.setIcon(iconOpcao5x4NaoSelecionada);
            }
            
        });
        add(opcao6x5);
        
        JLabel tituloTema = new JLabel ("");
        tituloTema.setIcon (new ImageIcon (getClass().getResource(
            "/MemoryGame/imagens/interface/titulos/tituloTemaOpcoesJM.png")));
        tituloTema.setSize(190,40);
        tituloTema.setLocation(485,345);
        //tituloForm.setLocation(220,15);
        add(tituloTema);
        
        opcaoTemaAnimais = new JButton("");
        opcaoTemaAnimais.setBorderPainted(true);
        opcaoTemaAnimais.setBorder(null);
        opcaoTemaAnimais.setContentAreaFilled(false);
        opcaoTemaAnimais.setIcon(iconAnimaisNaoSelecionada);
        opcaoTemaAnimais.setBounds(175, 395, 175, 175);
        opcaoTemaAnimais.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                isTemaEscolhido = true;
                temaEscolhido = "animais";
                
                opcaoTemaAnimais.setIcon(iconAnimaisSelecionada);
                
                opcaoTemaObjetos.setIcon(iconObjetosNaoSelecionada);
            }
            
        });
        add(opcaoTemaAnimais);
        
        opcaoTemaObjetos = new JButton("");
        opcaoTemaObjetos.setBorderPainted(true);
        opcaoTemaObjetos.setBorder(null);
        opcaoTemaObjetos.setContentAreaFilled(false);
        opcaoTemaObjetos.setIcon(iconObjetosNaoSelecionada);
        opcaoTemaObjetos.setBounds(360, 395, 175, 175);
        opcaoTemaObjetos.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                isTemaEscolhido = true;
                temaEscolhido = "objetos";
                
                opcaoTemaObjetos.setIcon(iconObjetosSelecionada);
                
                opcaoTemaAnimais.setIcon(iconAnimaisNaoSelecionada);
                
            }
            
        });
        add(opcaoTemaObjetos);
        
        opcaoTemaNada1 = new JButton("");
        opcaoTemaNada1.setBorderPainted(true);
        opcaoTemaNada1.setBorder(null);
        opcaoTemaNada1.setContentAreaFilled(false);
        opcaoTemaNada1.setIcon(iconNada);
        opcaoTemaNada1.setBounds(545, 395, 175, 175);
        opcaoTemaNada1.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                isTemaEscolhido = false;
                temaEscolhido = "";
                
                opcaoTemaAnimais.setIcon(iconAnimaisNaoSelecionada);
                opcaoTemaObjetos.setIcon(iconObjetosNaoSelecionada);

            }
            
        });
        add(opcaoTemaNada1);
        
        opcaoTemaNada2 = new JButton("");
        opcaoTemaNada2.setBorderPainted(true);
        opcaoTemaNada2.setBorder(null);
        opcaoTemaNada2.setContentAreaFilled(false);
        opcaoTemaNada2.setIcon(iconNada);
        opcaoTemaNada2.setBounds(730, 395, 175, 175);
        opcaoTemaNada2.addActionListener(new ActionListener (){
            
            @Override
            public void actionPerformed(ActionEvent e) {
                //ainda em desenvolvimento, nÃ©? :b
                isTemaEscolhido = false;
                temaEscolhido = "";
                
                opcaoTemaAnimais.setIcon(iconAnimaisNaoSelecionada);
                opcaoTemaObjetos.setIcon(iconObjetosNaoSelecionada);

            }
            
        });
        add(opcaoTemaNada2);
        
        /*
        opcaoTemaNada1 = new JButton("");
        opcaoTemaNada1.setBorderPainted(true);
        opcaoTemaNada1.setBorder(null);
        opcaoTemaNada1.setContentAreaFilled(false);
        opcaoTemaNada1.setIcon(iconNada);
        opcaoTemaNada1.setBounds(545, 395, 175, 175);
        */
        
        JButton entrarJogar = new JButton();
        entrarJogar.setBorderPainted(true);
        entrarJogar.setBorder(null);
        entrarJogar.setContentAreaFilled(false);
        entrarJogar.setIcon(iconJogar);
        entrarJogar.setLocation(405, 600);
        entrarJogar .setSize(entrarJogar.getPreferredSize());
        entrarJogar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                
                if (isDificuldadeEscolhida){
                    if (isTemaEscolhido){

                    Configuracoes.getInstance().setDificuldade(dificuldadeEscolhida);
					Configuracoes.getInstance().setTemaFiguras(temaEscolhido);

//                        TelaJogoMemoria tjm = new TelaJogoMemoria();
					dispose();

                    } else {
                        JOptionPane.showMessageDialog (null, "Tema nÃ£o escolhido");
                    }
                } else {
                    JOptionPane.showMessageDialog (null, "Dificuldade nÃ£o escolhida");
                }
            }
        });
        add(entrarJogar);
        
        
        
        JButton sairMenu = new JButton("");
        sairMenu.setBorder (null);
        sairMenu.setBorderPainted(true);
        sairMenu.setContentAreaFilled(false);
        sairMenu.setIcon(iconVoltarMenu);
        sairMenu.setLocation(entrarJogar.getX() + entrarJogar.getWidth() + 10, 600);
        sairMenu .setSize(sairMenu.getPreferredSize());
        sairMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
                TelaMenuPrincipal tmp = new TelaMenuPrincipal();
            }
        });
        
        add(sairMenu);
        
        setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMaximumSize(new java.awt.Dimension(800, 500));
        setMinimumSize(new java.awt.Dimension(800, 500));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(800, 500));
        getContentPane().setLayout(null);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPreJogoMemoria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPreJogoMemoria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPreJogoMemoria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPreJogoMemoria.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new TelaPreJogoMemoria().setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(TelaPreJogoMemoria.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
