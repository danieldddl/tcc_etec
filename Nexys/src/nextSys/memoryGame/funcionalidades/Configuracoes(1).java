package nextSys.memoryGame.funcionalidades;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author Daniel
 */
public class Configuracoes {
    
    private static Configuracoes configuracoes;
    
    private static String temaFiguras;
    private static int dificuldade;

    private LayoutJogo lj;
    
    private static MyButtonCarta carta1;
    private static MyButtonCarta carta2;
    
    public void zerandoTudo(){
        configuracoes = null;
        temaFiguras = null;
        dificuldade = 0;
        lj = null;
        carta1 = null;
        carta2 = null;
    }
    
    /**
     * @return the temaFiguras
     */
    public String getTemaFiguras() {
        return temaFiguras;
    }

    /**
     * @param temaFiguras the temaFiguras to set
     */
    public void setTemaFiguras(String temaFiguras) {
        this.temaFiguras = temaFiguras;
    }

    /**
     * @return the dificuldade
     */
    public int getDificuldade() {
        return dificuldade;
    }

    /**
     * @param dificuldade the dificuldade to set
     */
    public void setDificuldade(int dificuldade) {
        this.dificuldade = dificuldade;
    }

    /**
     * @return the lj
     */
    
    /**
     * @return the carta1
     */
    public static MyButtonCarta getCarta1() {
        return carta1;
    }

    /**
     * @param aCarta1 the carta1 to set
     */
    public static void setCarta1(MyButtonCarta aCarta1) {
        carta1 = aCarta1;
    }

    /**
     * @return the carta2
     */
    public static MyButtonCarta getCarta2() {
        return carta2;
    }

    /**
     * @param aCarta2 the carta2 to set
     */
    public static void setCarta2(MyButtonCarta aCarta2) {
        carta2 = aCarta2;
    }
    
    public LayoutJogo getLayoutJogo (){

        switch (configuracoes.getDificuldade()){
            case 1:
                lj = new LayoutJogoUm();
                break;
            case 2:
                lj = new LayoutJogoDois();
                break;
        
            case 3:
                lj = new LayoutJogoTres();
                break;
        
            case 4:
                lj = new LayoutJogoQuatro();
                break;
            default :
                //JOptionPane.showMessageDialog(null, "A DIFICULDADE ESTÁ SETADA PARA 0");
                break;
        }
        return lj;
    }
    
     public LayoutJogo getLayoutJogoPlay(){
         if(lj == null){
             lj = getLayoutJogo();
         }
         return lj;
     }
    
    public void setLayoutJogo (LayoutJogo lj){
        this.lj = lj;
    }
    
    //o private impede que esta classe de ser instanciada
    //da maneira convencional, ao menos
    private Configuracoes(){
        
    }
    
    public static Configuracoes getInstance (){
        if(configuracoes == null){
            configuracoes = new Configuracoes();
        }
        
        return configuracoes;
    }
    
    //timer que faz as cartas virarem de volta
    //precisa estar aqui para funcionar de uma maneira global
    Timer desvirarCarta = new Timer(1000, new ActionListener() {  
            public void actionPerformed(ActionEvent ev) {  
                
                try {
                    
                    Image imagemCartaImage = ImageIO.read (new File("res//imagens/memoryGame/cartas/back.jpg"));
                    ImageIcon imagemCartaIcon = new ImageIcon (imagemCartaImage.getScaledInstance(
                            getCarta1().getLargura(), getCarta1().getAltura(), Image.SCALE_DEFAULT));
                                        
                    getCarta1().setIcon(imagemCartaIcon);
                    getCarta2().setIcon(imagemCartaIcon);
                    
                } catch (IOException ex) {
                    //System.out.println(ex.getMessage());
                }
                                
                // e novamente as variáveis são limpas
                setCarta1(null);
                setCarta2(null);
                
                desvirarCarta.restart();
                desvirarCarta.stop();        
            }  
        });
    
    public Timer getDesvirarCarta (){
        return desvirarCarta;
    }    
    
}
