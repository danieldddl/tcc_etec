package nextSys.memoryGame.funcionalidades;

/**
 *
 * @author Daniel
 */
public class LayoutJogo {
    private LayoutJogo lj;
    private int [][] posicaoCartas;
    private int [][] statusCartas; 
    private int [][] cartasViradas = new int [2][2]; //sempre o mesmo tamanho
    private int numVitoria = 0;
    private int numViradas = 0;
    
    public LayoutJogo (){
        
    }
    
    /**
     * @return the numVitoria
     */
    public int getNumVitoria() {
        return numVitoria;
    }

    /**
     * @param numVitoria the numVitoria to set
     */
    public void setNumVitoria(int numVitoria) {
        this.numVitoria = numVitoria;
    }

    /**
     * @return the numViradas
     */
    public int getNumViradas() {
        return numViradas;
    }

    /**
     * @param numViradas the numViradas to set
     */
    public void setNumViradas(int numViradas) {
        this.numViradas = numViradas;
    }
    
    //GETTERS IMPROVISADOS DOS ARRAYS UTILIZADOS NA CLASSE
    public int getPosicaoCartas(int linha, int coluna){
        return posicaoCartas [linha][coluna];
    }
    
    public int getStatusCartas(int linha, int coluna){
        return statusCartas [linha][coluna];
    }  
    
    public int getCartasViradas(int linha, int coluna){
        return cartasViradas [linha][coluna];
    }
    
    /**
     * @return the posicaoCartas
     */
    public int[][] getPosicaoCartasArray() {
        return posicaoCartas;
    }

    /**
     * @param posicaoCartas the posicaoCartas to set
     */
    public void setPosicaoCartasArray(int[][] posicaoCartas) {
        this.posicaoCartas = posicaoCartas;
    }

    /**
     * @return the statusCartas
     */
    public int[][] getStatusCartasArray() {
        return statusCartas;
    }

    /**
     * @param statusCartas the statusCartas to set
     */
    public void setStatusCartasArray(int[][] statusCartas) {
        this.statusCartas = statusCartas;
    }    
    
    public void mudandoStatusClick (int linha, int coluna){
        
        //quando é a primeira carta a estar sendo virada
        if (getNumViradas() == 0){
            
            //verificstatusCartasa se a carta está de cabeça para baixo
            if (statusCartas[linha][coluna] == 0){
            
                //muda os status no array para uso de verificação
                statusCartas[linha][coluna] = 1;
                
                //guarda as coordenadas das cartas que foram viradas
                cartasViradas[0][0] = linha;
                cartasViradas[0][1] = coluna;
                
                //o número de cartas é acrescido
                setNumViradas(getNumViradas() + 1);                
            }
        }
        
        //quando é a segunda carta a ser virada
        else if (getNumViradas() == 1){
            if (statusCartas[linha][coluna] == 0){
                
                statusCartas[linha][coluna] = 1;
                
                cartasViradas [1][0] = linha;
                cartasViradas [1][1] = coluna;
                
                setNumViradas(getNumViradas() + 1);
            }
        }
    }
    
    public void verificaCartasIguais (){
        //apenas checando se existem duas cartas viradas mesmo
        if (getNumViradas() == 2){
            //se a primeira carta for igual a segunda...
            if (posicaoCartas [cartasViradas[0][0]][cartasViradas[0][1]] ==
                posicaoCartas [cartasViradas[1][0]][cartasViradas[1][1]]){
                
                //os status de ambas as cartas é alterado para 2
                //os status 2 disativa a carta
                statusCartas[cartasViradas[0][0]][cartasViradas[0][1]] = 2;
                statusCartas[cartasViradas[1][0]][cartasViradas[1][1]] = 2;
                
                //como duas cartas foram acertadas,
                //o número para determinar a vitória cresce em dois
                setNumVitoria(getNumVitoria() + 2);                
            } 
            //se as cartas viradas não forem iguais...
            else {
            //os status delas voltam a ser o que eram antes
            statusCartas[cartasViradas[0][0]][cartasViradas[0][1]] = 0;
            statusCartas[cartasViradas[1][0]][cartasViradas[1][1]] = 0;
            }           
        }
    }   
}
