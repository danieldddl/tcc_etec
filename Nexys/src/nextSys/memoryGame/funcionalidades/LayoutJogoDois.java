package nextSys.memoryGame.funcionalidades;

import java.util.Random;

/**
 *
 * @author Daniel
 */
public class LayoutJogoDois extends LayoutJogo {
    
    public LayoutJogoDois(){
        embaralharCartas();
        super.setPosicaoCartasArray(posicaoCartas);
        super.setStatusCartasArray(statusCartas);
    }
    
    private int [][] posicaoCartas = new int [4][3];
    private int [][] statusCartas = new int [4][3];
    
    public void embaralharCartas() {
        
        Random r = new Random ();
        
        int [][] posicaoCartas = new int [4][3];
        int i = 0, cont = 0, linha = 0, coluna = 0;
        
        while (i < 6){
            linha = r.nextInt(4);
            coluna = r.nextInt(3);
            
            if (posicaoCartas[linha][coluna] == 0){
                posicaoCartas[linha][coluna] = i;
                
                cont++;
            }
            
            if (cont == 2){
                i++;
                cont = 0;
            }
        }
        
        this.posicaoCartas = posicaoCartas;
    }
    
}
