package nextSys.memoryGame.funcionalidades;

import java.util.Random;

/**
 *
 * @author Daniel
 */
public class LayoutJogoQuatro extends LayoutJogo{
    
    public LayoutJogoQuatro (){
        embaralharCartas();
        super.setPosicaoCartasArray(posicaoCartas);
        super.setStatusCartasArray(statusCartas);
    }  
   
    private int [][] posicaoCartas = new int [6][5];
    private int [][] statusCartas = new int [6][5];
    
    public void embaralharCartas() {
        
        Random r = new Random ();
        
        int [][] posicaoCartas = new int [6][5];
        int i = 0, cont = 0, linha = 0, coluna = 0;
        
        while (i < 15){
            linha = r.nextInt(6);
            coluna = r.nextInt(5);
            
            if (posicaoCartas[linha][coluna] == 0){
                posicaoCartas[linha][coluna] = i;
                
                cont++;
            }
            
            if (cont == 2){
                i++;
                cont = 0;
            }
        }
        
        this.posicaoCartas = posicaoCartas;
    }
    
}
