package nextSys.memoryGame.funcionalidades;

import java.util.Random;

/**
 *
 * @author Daniel
 */
public class LayoutJogoTres extends LayoutJogo{
    
    public LayoutJogoTres (){
        embaralharCartas();
        super.setPosicaoCartasArray(posicaoCartas);
        super.setStatusCartasArray(statusCartas);
    }
    
    private int [][] posicaoCartas = new int [5][4];
    private int [][] statusCartas = new int [5][4];
    
    public void embaralharCartas() {
        
        Random r = new Random ();
        
        int [][] posicaoCartas = new int [5][4];
        
        int i = 0, cont = 0, linha = 0, coluna = 0; 
        
        while (i < 10){
            
            linha = r.nextInt(5);
            coluna = r.nextInt(4);
        
            if (posicaoCartas [linha][coluna] == 0){
                posicaoCartas [linha][coluna] = i;
                cont++; 
            }

            if (cont == 2){
                i++;
                cont = 0;
            }
        }
        
        this.posicaoCartas = posicaoCartas;
    }
    
}
