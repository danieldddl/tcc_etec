package nextSys.memoryGame.funcionalidades;

import java.util.Random;

/**
 *
 * @author Daniel
 */
public class LayoutJogoUm extends LayoutJogo{
     
    public LayoutJogoUm (){
        embaralharCartas();
        super.setPosicaoCartasArray(posicaoCartas);
        super.setStatusCartasArray(statusCartas);
    }
    
    private int [][] posicaoCartas = new int [3][2];
    private int [][] statusCartas = new int [3][2];
    
    //vai retornar um array bidimensional do tipo inteiro
    public void embaralharCartas() {
        
        Random r = new Random ();
        
        int [][] posicaoCartas = new int [3][2];
        int i = 0, cont = 0, linha = 0, coluna = 0;
        
        while (i < 3){
            linha = r.nextInt(3);
            coluna = r.nextInt(2);
            
            if (posicaoCartas[linha][coluna] == 0){
                posicaoCartas[linha][coluna] = i;
                
                cont++;
            }
            
            if (cont == 2){
                i++;
                cont = 0;
            }
        }
        
        this.posicaoCartas = posicaoCartas;
    }
}
