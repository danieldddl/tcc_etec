package nextSys.memoryGame.funcionalidades;

//arrumar o caminho das imagens
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.Timer;

import nextSys.memoryGame.formularios.TelaJogoMemoria;
import nextSys.sistemaGeral.formularios.CaixaMensagem;
import nextSys.sistemaGeral.formularios.TelaFaseDois;
import nextSys.sistemaGeral.formularios.TelaFaseTres;
import nextSys.sistemaGeral.formularios.TelaFaseUm;
import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;
import nextSys.sistemaGeral.funcionalidades.Reprodutor;

/**
 *
 * @author Daniel
 */
public class MyButtonCarta extends JButton implements ActionListener{

    private FileInputStream in;
    private Pontuacao p;
    private TelaJogoMemoria tjm;
    
    public Pontuacao getPontuacao (){
        return p;
    }
    
    private int linha, coluna, altura, largura;
    
    //getters e settters
    /**
     * @return the coluna
     */
    public int getColuna() {
        return coluna;
    }

    /**
     * @param coluna the coluna to set
     */
    public void setColuna(int coluna) {
        this.coluna = coluna;
    }
    
    /**
     * @return the linha
     */
    public int getLinha() {
        return linha;
    }

    /**
     * @param linha the linha to set
     */
    public void setLinha(int linha) {
        this.linha = linha;
    }

    /**
     * @return the altura
     */
    public int getAltura() {
        return altura;
    }

    /**
     * @param altura the altura to set
     */
    public void setAltura(int altura) {
        this.altura = altura;
    }

    /**
     * @return the largura
     */
    public int getLargura() {
        return largura;
    }

    /**
     * @param largura the largura to set
     */
    public void setLargura(int largura) {
        this.largura = largura;
    }
    
    Configuracoes conf = Configuracoes.getInstance();
    Reprodutor r = new Reprodutor();
    
    //trows necessÃ¡rios para usar o ImageIO.read
        //(em caso de nÃ£o existÃªncia da imagem)
    public MyButtonCarta (int x, int y, int largura, int altura, int linha, int coluna, Pontuacao p, TelaJogoMemoria tjm){
        super();
        this.setOpaque(false);
        this.setContentAreaFilled(false);
        this.setBorderPainted(false);
        super.setBounds(x, y, largura, altura);
        
        try {
        
            Image imagemCarta = ImageIO.read (new File("res/imagens/memoryGame/cartas/back.jpg"));
            setIcon(new ImageIcon(imagemCarta.getScaledInstance(altura, largura, Image.SCALE_DEFAULT)));
                    
        } catch (Exception e){
            //System.out.println(e.getMessage());
        }
        super.addActionListener(this);
        //variÃ¡ves usadas no algoritmo de verificaÃ§Ã£o
        this.linha = linha;
        this.coluna = coluna;
        this.largura = largura;
        this.altura = altura;
        this.p = p;
        this.tjm = tjm;
        
    }
    
    public void depoisDoisCliques(MyButtonCarta carta){
        
        /*o mÃ©todo que diz se as cartas 
        estavam certas ou nÃ£o*/
        conf.getLayoutJogoPlay().verificaCartasIguais();
        
        //verifica se Ã© a primeira carta a estar sendo virada...
        if (conf.getLayoutJogoPlay().getNumViradas() == 1){
            //porque dessa forma podemos guardar qual Ã© o botÃ£o que foi clicado
            //para depois voltÃ¡-lo 
            conf.setCarta1(carta);
        }
        
        //se for a segunda carta a estar sendo virada...
        else if (conf.getLayoutJogoPlay().getNumViradas() == 2) {
            
            //a informaÃ§Ã£o que diz qual Ã© o botÃ£o tambÃ©m Ã© guardada no segundo botÃ£o
            conf.setCarta2(carta);
            
            //se as cartas estiverem certas (descobrimos com o mÃ©todo bem no inÃ­cio)...
            if (conf.getLayoutJogoPlay().getStatusCartas(
                    conf.getLayoutJogoPlay().getCartasViradas(0, 0), 
                    conf.getLayoutJogoPlay().getCartasViradas(0, 1)) == 2 
                        &&
                conf.getLayoutJogoPlay().getStatusCartas(
                    conf.getLayoutJogoPlay().getCartasViradas(1, 0), 
                    conf.getLayoutJogoPlay().getCartasViradas(1, 1)) == 2) {
            
                try {
        
                    Image imagemCarta = ImageIO.read (new File("res/imagens/memoryGame/cartas/"+conf.getTemaFiguras()+"/"
                            + conf.getLayoutJogoPlay().getPosicaoCartas(conf.getLayoutJogoPlay().getCartasViradas(0, 0), 
                                                                        conf.getLayoutJogoPlay().getCartasViradas(0, 1))+".jpg"));
                            conf.getCarta1().setDisabledIcon(new ImageIcon(imagemCarta.getScaledInstance(altura, largura, Image.SCALE_DEFAULT)));
                            
                            imagemCarta = ImageIO.read (new File("res/imagens/memoryGame/cartas/"+conf.getTemaFiguras()+"/"
                            + conf.getLayoutJogoPlay().getPosicaoCartas(conf.getLayoutJogoPlay().getCartasViradas(1, 0), 
                                                                        conf.getLayoutJogoPlay().getCartasViradas(1, 1))+".jpg"));
                            conf.getCarta2().setDisabledIcon(new ImageIcon(imagemCarta.getScaledInstance(altura, largura, Image.SCALE_DEFAULT)));

                } catch (Exception e){
                    //System.out.println(e.getMessage());
                }
                
                //os botÃµes sÃ£o desativados...
                conf.getCarta1().setEnabled(false);
                conf.getCarta2().setEnabled(false);
                                
                
                
                //para nÃ£o para o programa durante a reproduÃ§Ã£o do som 
                Thread threadSom = new Thread (){

                    URL url; 
                    
                    @Override
                    public void run() {
                        super.run(); 
                        try {
                            File file = new File("res/sons/memoryGame/cartas/" + conf.getTemaFiguras() + "/" 
                            + conf.getLayoutJogoPlay().getPosicaoCartas(conf.getLayoutJogoPlay().getCartasViradas(0, 0)   
                             ,conf.getLayoutJogoPlay().getCartasViradas(0, 1)) + ".wav");
                            url = file.toURL();
                        } catch (Exception e){
                            System.out.println(e.getMessage());
                        }
                        
                        
                        //teste
                        /*System.out.println("/arquivosComplementares/sons/memoryGame/cartas/" + conf.getTemaFiguras() + "/" 
                            + conf.getLayoutJogoPlay().getPosicaoCartas(conf.getLayoutJogoPlay().getCartasViradas(0, 0)   
                             ,conf.getLayoutJogoPlay().getCartasViradas(0, 1)) + ".wav");*/
                        r.reproduzirSom(url);
                    }
                    
                };
                threadSom.start();
                
                URL url = null;
                
                try {
                    File file = new File("res/sons/memoryGame/cartas/" + conf.getTemaFiguras() + "/" 
                            + conf.getLayoutJogoPlay().getPosicaoCartas(conf.getLayoutJogoPlay().getCartasViradas(0, 0)   
                             ,conf.getLayoutJogoPlay().getCartasViradas(0, 1)) + ".wav");
                    url = file.toURL();
                } catch (Exception e){
                    System.out.println(e.getMessage());
                }
                Image imagemCarta = null;
                
                try {
                    imagemCarta = ImageIO.read (new File("res/imagens/memoryGame/cartas/"+conf.getTemaFiguras()+"/"
                                + conf.getLayoutJogoPlay().getPosicaoCartas(conf.getLayoutJogoPlay().getCartasViradas(0, 0), 
                                                                            conf.getLayoutJogoPlay().getCartasViradas(0, 1))+".jpg"));
                } catch (Exception e){
                    //System.out.println(e.getMessage());
                }
                
                tjm.atualizarSom(url);
                tjm.atualizarUltimaCarta(imagemCarta);
                                
                //e as variÃ¡veis com as informaÃ§Ãµes sobre os botÃµes sÃ£o limpas 
                conf.setCarta1(null);
                conf.setCarta2(null);
                p.acertou();
                //tjm.atualizarLabels();
            }
            
            //se as cartas nÃ£o baterem...
            else {
                
                conf.getDesvirarCarta().start();
                p.errou();
                //tjm.atualizarLabels();
                
            }
            
            //em ambos os processos o nÃºmero de 
            //cartas viradas passa a ser zero
            conf.getLayoutJogoPlay().setNumViradas(0);
        } 
    }
    
    
    
    public void determinarVitoria(){
        CaixaMensagem cm = new CaixaMensagem();
        GerenciamentoFases gf = new GerenciamentoFases();
        
        switch(conf.getDificuldade()){
        
            case 1:
                if (conf.getLayoutJogoPlay().getNumVitoria() == 6){
                        cm.mostrarMensagem("Congratulations! You have won.");
                        gf.terminoJogo(tjm.getDescFase(), 1);
                        
                        if (tjm.getDescFase() == 1){
                        	TelaFaseUm t = new TelaFaseUm(1);
                        	t.setVisible(true);
                        	
                        } else if (tjm.getDescFase() == 2){
                        	TelaFaseDois t = new TelaFaseDois(2);
                        	t.setVisible(true);
                        	
                        } else if (tjm.getDescFase() == 3){
                        	TelaFaseTres t = new TelaFaseTres(3);
                        	t.setVisible(true);
                        	
                        }
                        conf.zerandoTudo();
                        tjm.dispose();
                        
                }
                break;
            case 2:
                if (conf.getLayoutJogoPlay().getNumVitoria() == 12){
                	cm.mostrarMensagem("Congratulations! You have won.");
                    gf.terminoJogo(tjm.getDescFase(), 1);
                    
                    if (tjm.getDescFase() == 1){
                    	TelaFaseUm t = new TelaFaseUm(1);
                    	t.setVisible(true);
                    	
                    } else if (tjm.getDescFase() == 2){
                    	TelaFaseDois t = new TelaFaseDois(2);
                    	t.setVisible(true);
                    	
                    } else if (tjm.getDescFase() == 3){
                    	TelaFaseTres t = new TelaFaseTres(3);
                    	t.setVisible(true);
                    	
                    }
                    conf.zerandoTudo();
                    tjm.dispose();                
                }
                break;
            case 3:
                if (conf.getLayoutJogoPlay().getNumVitoria() == 20){
                	cm.mostrarMensagem("Congratulations! You have won.");
                    gf.terminoJogo(tjm.getDescFase(), 1);
                    
                    if (tjm.getDescFase() == 1){
                    	TelaFaseUm t = new TelaFaseUm(1);
                    	t.setVisible(true);
                    	
                    } else if (tjm.getDescFase() == 2){
                    	TelaFaseDois t = new TelaFaseDois(2);
                    	t.setVisible(true);
                    	
                    } else if (tjm.getDescFase() == 3){
                    	TelaFaseTres t = new TelaFaseTres(3);
                    	t.setVisible(true);
                    	
                    }
                    conf.zerandoTudo();
                    tjm.dispose();
                }
                break;
            case 4:
                if (conf.getLayoutJogoPlay().getNumVitoria() == 30){
                	cm.mostrarMensagem("Congratulations! You have won.");
                    gf.terminoJogo(tjm.getDescFase(), 1);
                    
                    if (tjm.getDescFase() == 1){
                    	TelaFaseUm t = new TelaFaseUm(1);
                    	t.setVisible(true);
                    	
                    } else if (tjm.getDescFase() == 2){
                    	TelaFaseDois t = new TelaFaseDois(2);
                    	t.setVisible(true);
                    	
                    } else if (tjm.getDescFase() == 3){
                    	TelaFaseTres t = new TelaFaseTres(3);
                    	t.setVisible(true);
                    	
                    }
                    conf.zerandoTudo();
                    tjm.dispose();
                }
                break;
            default:
                //JOptionPane.showMessageDialog(null, "NUNCA TERÃ� FIM, VACILÃƒO!");
                break;
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (conf.getDesvirarCarta().isRunning() || mostrarInicio.isRunning()){
            
        }
        
        else {
            
            //fazendo alteraÃ§Ãµes necessÃ¡rias em variÃ¡veis...
            conf.getLayoutJogoPlay().mudandoStatusClick(getLinha(), getColuna());

            //virando a carta
            try {
                Image imagemCartaNova = ImageIO.read(new File ("res/imagens/memoryGame/cartas/" +
                    conf.getTemaFiguras() + "/" + conf.getLayoutJogoPlay().getPosicaoCartas(getLinha(), getColuna()) + ".jpg"));
                
                super.setIcon(new ImageIcon(imagemCartaNova.getScaledInstance(getAltura(), getLargura(), Image.SCALE_DEFAULT)));
                
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
            depoisDoisCliques(this);
            determinarVitoria();
            
        }
    }

    Timer mostrarInicio = new Timer(1500, new ActionListener() {  
            public void actionPerformed(ActionEvent ev) {  
                
                try {
                    
                    Image imagemCarta = ImageIO.read (new File("res/imagens/memoryGame/cartas/back.jpg"));
                    setIcon(new ImageIcon(imagemCarta.getScaledInstance(altura, largura, Image.SCALE_DEFAULT)));
                    
                } catch (IOException ex) {
                    System.out.println(ex.getMessage());
                }
                
                mostrarInicio.stop();
            }  
        });

}//fim da classe MyButtonCarta
