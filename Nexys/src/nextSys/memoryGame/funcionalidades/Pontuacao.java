package nextSys.memoryGame.funcionalidades;

import nextSys.sistemaGeral.funcionalidades.Conexao;

/**
 *
 * @author Daniel
 */
public class Pontuacao {

    Conexao c = new Conexao();
    
    private int numAcertos = 0;
    private int numErros = 0;
    private int numAcertosStreak = 0;
    private int numErrosStreak = 0;
    
    private int sequenciaAcertos = 0;
    private int sequenciaErros = 0;
    
    /*private int minutos = 0;
    private int segundos = 0;
    private int milissegundos = 0;*/
    
    private int pontuacao = 0;
    
    /**
     * @return the numAcertos
     */
    public int getNumAcertos() {
        return numAcertos;
    }

    /**
     * @param numAcertos the numAcertos to set
     */
    public void setNumAcertos(int numAcertos) {
        this.numAcertos = numAcertos;
    }

    /**
     * @return the numErros
     */
    public int getNumErros() {
        return numErros;
    }

    /**
     * @param numErros the numErros to set
     */
    public void setNumErros(int numErros) {
        this.numErros = numErros;
    }

    /**
     * @return the numAcertosStreak
     */
    public int getNumAcertosStreak() {
        return numAcertosStreak;
    }

    /**
     * @param numAcertosStreak the numAcertosStreak to set
     */
    public void setNumAcertosStreak(int numAcertosStreak) {
        this.numAcertosStreak = numAcertosStreak;
    }

    /**
     * @return the numErrosStreak
     */
    public int getNumErrosStreak() {
        return numErrosStreak;
    }

    /**
     * @param numErrosStreak the numErrosStreak to set
     */
    public void setNumErrosStreak(int numErrosStreak) {
        this.numErrosStreak = numErrosStreak;
    }

    /**
     * @return the sequenciaAcertos
     */
    public int getSequenciaAcertos() {
        return sequenciaAcertos;
    }

    /**
     * @param sequenciaAcertos the sequenciaAcertos to set
     */
    public void setSequenciaAcertos(int sequenciaAcertos) {
        this.sequenciaAcertos = sequenciaAcertos;
    }

    /**
     * @return the sequenciaErros
     */
    public int getSequenciaErros() {
        return sequenciaErros;
    }

    /**
     * @param sequenciaErros the sequenciaErros to set
     */
    public void setSequenciaErros(int sequenciaErros) {
        this.sequenciaErros = sequenciaErros;
    }
    
    /**
     * @return the pontuacao
     */
    public int getPontuacao() {
        return pontuacao;
    }

    /**
     * @param pontuacao the pontuacao to set
     */
    public void setPontuacao(int pontuacao) {
        this.pontuacao = pontuacao;
    }
        
    public int acertou (){
        setNumAcertos(getNumAcertos() + 1);
        setSequenciaAcertos(getSequenciaAcertos() + 1);
        setSequenciaErros(0);
        
        if (getSequenciaAcertos() == 3){
            setNumAcertosStreak(getNumAcertosStreak() + 1);
            setSequenciaAcertos(0);
        }
        
        return calcularPontuacao (this);
    }
    
    public int errou(){
        setNumErros(getNumErros() + 1);
        setSequenciaErros(getSequenciaErros() + 1);
        setSequenciaAcertos(0);
        
        if (getSequenciaErros() == 3){
            setNumErrosStreak(getNumErrosStreak() + 1);
            setSequenciaErros(0);
        }
        
        return calcularPontuacao (this);
    }
    
    public int calcularPontuacao (Pontuacao p){
        p.setPontuacao((p.getNumAcertos() * 100) + (p.getNumAcertosStreak() * 400) +
                        (p.getNumErros() * - 10) + (p.getNumErrosStreak() * - 25));
        
               
        return p.getPontuacao();
    }

} // fim da classe de Pontuação
