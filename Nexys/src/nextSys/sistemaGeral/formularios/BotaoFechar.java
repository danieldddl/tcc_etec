package nextSys.sistemaGeral.formularios;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;

public class BotaoFechar extends JButton implements ActionListener{

	//VARIAVEIS
	private int distanciaLargura;
	private int distanciaAltura;
	private int largura;
	private int altura;
	private int cor;
	private String mensagem;
	private JFrame framePai;
	private Icon iconBotaoFechar;
	private Icon iconBotaoFecharPress;
	private CaixaMensagem cm;
	
	static final int AMARELO = 1;
	static final int AZUL = 2;
	static final int VERDE = 3;
	static final int VERMELHO = 4;
	
	//CONSTRUTOR
	public BotaoFechar(JFrame framePai, String mensagem, int cor, 
					   int largura, int altura, int distanciaLargura, int distanciaAltura){
		
		this.distanciaLargura = distanciaLargura;
		this.distanciaAltura = distanciaAltura;
		this.largura = largura;
		this.altura = altura;
		this.cor = cor;
		this.mensagem = mensagem;
		if (framePai != null){
			this.framePai = framePai;
		}
		cm = new CaixaMensagem();
		carregaImagens();
		adicionar();
	}
	
	private void carregaImagens(){
		
		try {
			if (cor == AMARELO){
				Image imagemBotaoFechar = ImageIO.read (new File(
						"res/imagens/menus/botoes/botao_fechar_amarelo.png"));
				iconBotaoFechar = new ImageIcon(
						imagemBotaoFechar.getScaledInstance(largura, altura, Image.SCALE_DEFAULT));
			
				Image imagemBotaoFecharPressionado = ImageIO.read (new File(
						"res/imagens/menus/botoes/botao_fechar_amarelo_press.png"));
				iconBotaoFecharPress = new ImageIcon(
						imagemBotaoFecharPressionado.getScaledInstance(largura, altura, Image.SCALE_DEFAULT));
				
			} else if (cor == VERDE){
				Image imagemBotaoFechar = ImageIO.read (new File(
						"res/imagens/menus/botoes/botao_fechar_verde.png"));
				iconBotaoFechar = new ImageIcon(
						imagemBotaoFechar.getScaledInstance(largura, altura, Image.SCALE_DEFAULT));
			
				Image imagemBotaoFecharPressionado = ImageIO.read (new File(
						"res/imagens/menus/botoes/botao_fechar_verde_press.png"));
				iconBotaoFecharPress = new ImageIcon(
						imagemBotaoFecharPressionado.getScaledInstance(largura, altura, Image.SCALE_DEFAULT));
				
			} else if (cor == AZUL){
				Image imagemBotaoFechar = ImageIO.read (new File(
						"res/imagens/menus/botoes/botao_fechar_azul.png"));
				iconBotaoFechar = new ImageIcon(
						imagemBotaoFechar.getScaledInstance(largura, altura, Image.SCALE_DEFAULT));
			
				Image imagemBotaoFecharPressionado = ImageIO.read (new File(
						"res/imagens/menus/botoes/botao_fechar_azul_press.png"));
				iconBotaoFecharPress = new ImageIcon(
						imagemBotaoFecharPressionado.getScaledInstance(largura, altura, Image.SCALE_DEFAULT));
				
			} else if (cor == VERMELHO){
				Image imagemBotaoFechar = ImageIO.read (new File(
						"res/imagens/menus/botoes/botao_fechar_vermelho.png"));
				iconBotaoFechar = new ImageIcon(
						imagemBotaoFechar.getScaledInstance(largura, altura, Image.SCALE_DEFAULT));
			
				Image imagemBotaoFecharPressionado = ImageIO.read (new File(
						"res/imagens/menus/botoes/botao_fechar_vermelho_press.png"));
				iconBotaoFecharPress = new ImageIcon(
						imagemBotaoFecharPressionado.getScaledInstance(largura, altura, Image.SCALE_DEFAULT));
			}
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
	}
	
	private void adicionar(){
		
		setIcon(iconBotaoFechar);
		setPressedIcon(iconBotaoFecharPress);
		setContentAreaFilled(false);
		setBorder(null);
		setSize(super.getPreferredSize());
		setLocation(framePai.getWidth() - super.getWidth() - distanciaLargura, 
					distanciaAltura);
		addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (this.mensagem == null){
			//FECHA DIRETO O FRAME SE N�O TIVER 
			//UMA MENSAGEM SER MOSTRADA
			framePai.dispose();
		} else {
			cm.mostrarPergunta(this.mensagem);
			int resposta = cm.getEscolhido();            
	       	if (resposta == 1){
	        	framePai.dispose();
	        } else {
	        	//CANCELAR
	        }
		}
		
		
	}
	
	
	
}
