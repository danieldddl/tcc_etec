package nextSys.sistemaGeral.formularios;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.border.LineBorder;

import nextSys.sistemaGeral.funcionalidades.DAOUsuario;
import nextSys.sistemaGeral.funcionalidades.InformacoesUsuarioLogado;

public class DialogoTrocarSenha extends JDialog implements ActionListener{

	//TEXTFIELDS
	private JPasswordField txtSenha;
	private JPasswordField txtNovaSenha;
	private JPasswordField txtNovaSenhaRep;
	
	//LABELS
	private JLabel lblTitulo;
	private JLabel lblSenha;
	private JLabel lblNovaSenha;
	private JLabel lblNovaSenhaRep;
	
	private JLabel lblErro;
	
	//BOTOES
	private JButton btnFechar;
	private JButton btnConfirmar;
	private JButton btnCancelar;
	
	//IMAGENS
	private ImageIcon iconBtnFechar, iconBtnFecharPress;
	private ImageIcon iconBtnConfirmar, iconBtnConfirmarPress;
	private ImageIcon iconBtnCancelar, iconBtnCancelarPress;
	private ImageIcon iconLblTitulo;
	
	private static final Color corBordaTextos = new Color(1,112,207);
	
	private static final int LARGURA_DIALOGO = 400;
	private static final int ALTURA_DIALOGO = 330;
	private static final int LARGURA_TEXTOS = 177;
	private static final int ALTURA_TEXTOS = 25;
	private static final int DISTANCIA = 15;
	
	public DialogoTrocarSenha (){
		carregarImagens();
		iniciaComponentes();
		
	}
		
	public void carregarImagens(){
		//BOTAO DE FECHAR
		Image imagemBtnFechar = new ImageIcon("res/imagens/menus/botoes/botao_fechar_azul.png").getImage();
		iconBtnFechar = new ImageIcon (imagemBtnFechar.getScaledInstance(35, 35, Image.SCALE_DEFAULT));
		
		Image imagemBtnFecharPress = new ImageIcon("res/imagens/menus/botoes/botao_fechar_azul_press.png").getImage();
		iconBtnFecharPress = new ImageIcon (imagemBtnFecharPress.getScaledInstance(35, 35, Image.SCALE_DEFAULT));
		
		//BOTOES DE CONFIRMAR E CANCELAR
		Image imageBtnConfirmar = new ImageIcon ("res/imagens/menus/botoes/botao_confirmar_editar.png").getImage();
		iconBtnConfirmar = new ImageIcon (imageBtnConfirmar.getScaledInstance(177, 35, Image.SCALE_DEFAULT));
		
		Image imageBtnConfirmarPress = new ImageIcon ("res/imagens/menus/botoes/botao_confirmar_press_editar.png").getImage();
		iconBtnConfirmarPress = new ImageIcon (imageBtnConfirmarPress.getScaledInstance(177, 35, Image.SCALE_DEFAULT));
		
		Image imageBtnCancelar = new ImageIcon ("res/imagens/menus/botoes/botao_cancelar_editar.png").getImage();
		iconBtnCancelar = new ImageIcon (imageBtnCancelar.getScaledInstance(177, 35, Image.SCALE_DEFAULT));
		
		Image imageBtnCancelarPress = new ImageIcon ("res/imagens/menus/botoes/botao_cancelar_press_editar.png").getImage();
		iconBtnCancelarPress = new ImageIcon (imageBtnCancelarPress.getScaledInstance(177, 35, Image.SCALE_DEFAULT));
	
		//TITULO
		iconLblTitulo = new ImageIcon ("res/imagens/menus/titulos/lblTitulo_mudarTitulo.png");
		
	}
	
	public void iniciaComponentes(){
		
		super.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		super.setUndecorated(true);
		super.setBackground(new Color(245,204,59,250));
		super.getRootPane().setBorder(new LineBorder(new Color(221,175,13,220), 5));
		super.setSize(LARGURA_DIALOGO, ALTURA_DIALOGO);
		super.setLocationRelativeTo(null);
		super.setModalityType(ModalityType.APPLICATION_MODAL);
		super.setLayout(null);
		
		btnFechar = new JButton();
		btnFechar.setSize(35,35);
		btnFechar.setLocation(LARGURA_DIALOGO - btnFechar.getWidth() - 12, 2);
		btnFechar.setIcon(iconBtnFechar);
		btnFechar.setPressedIcon(iconBtnFecharPress);
		btnFechar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnFechar.setContentAreaFilled(false);
		btnFechar.setFocusable(false);
		btnFechar.setBorder(null);
		btnFechar.addActionListener(this);
		super.add(btnFechar);
		
		int larguraTitulo = LARGURA_DIALOGO - btnFechar.getWidth() - DISTANCIA * 3;
		int xTitulo = (LARGURA_DIALOGO - larguraTitulo)/2;
		int yTitulo = btnFechar.getY() + btnFechar.getHeight() + DISTANCIA;
		
		lblTitulo = new JLabel();
		lblTitulo.setSize(larguraTitulo, 40);
		lblTitulo.setLocation(xTitulo, yTitulo);
		lblTitulo.setIcon(iconLblTitulo);
		super.add(lblTitulo);
		
		int tamanhoBotoes = (LARGURA_DIALOGO - DISTANCIA * 3) / 2;
		int alturaBotoes = 35;
		int yBotoes = ALTURA_DIALOGO - alturaBotoes - DISTANCIA;
		
		btnConfirmar = new JButton();
		btnConfirmar.setSize(tamanhoBotoes, alturaBotoes);
		btnConfirmar.setLocation(DISTANCIA, yBotoes);
		btnConfirmar.setIcon(iconBtnConfirmar);
		btnConfirmar.setPressedIcon(iconBtnConfirmarPress);
		btnConfirmar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnConfirmar.setContentAreaFilled(false);
		btnConfirmar.setBorder(null);
		btnConfirmar.setFocusable(false);
		btnConfirmar.addActionListener(this);
		super.add(btnConfirmar);
			
		btnCancelar = new JButton();
		btnCancelar.setSize(btnConfirmar.getSize());
		btnCancelar.setLocation(btnConfirmar.getX() + btnConfirmar.getWidth() + DISTANCIA, yBotoes);
		btnCancelar.setIcon(iconBtnCancelar);
		btnCancelar.setPressedIcon(iconBtnCancelarPress);
		btnCancelar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnCancelar.setContentAreaFilled(false);
		btnCancelar.setBorder(null);
		btnCancelar.setFocusable(false);
		btnCancelar.addActionListener(this);
		super.add(btnCancelar);
		
		//COMECANDO DE BAIXO, PARA TOMAR COMO BASE OS BOTOES
		txtNovaSenhaRep = new JPasswordField();
		txtNovaSenhaRep.setSize(LARGURA_TEXTOS, ALTURA_TEXTOS);
		txtNovaSenhaRep.setLocation(btnCancelar.getX() - 20, btnCancelar.getY() - ALTURA_TEXTOS - DISTANCIA * 3);
		txtNovaSenhaRep.setBorder(new LineBorder(corBordaTextos));
		super.add(txtNovaSenhaRep);
		
		lblNovaSenhaRep = new JLabel("Re-type new password:");
		lblNovaSenhaRep.setSize(lblNovaSenhaRep.getPreferredSize());
		int alturaLabel = lblNovaSenhaRep.getHeight();
		lblNovaSenhaRep.setLocation(txtNovaSenhaRep.getX() - lblNovaSenhaRep.getWidth() - DISTANCIA, 
									txtNovaSenhaRep.getY() + (ALTURA_TEXTOS - alturaLabel)/2);
		lblNovaSenhaRep.setForeground(new Color(0,112,207));
		super.add(lblNovaSenhaRep);
		
		txtNovaSenha = new JPasswordField();
		txtNovaSenha.setSize(txtNovaSenhaRep.getSize());
		txtNovaSenha.setLocation(txtNovaSenhaRep.getX(), txtNovaSenhaRep.getY() - ALTURA_TEXTOS - DISTANCIA);
		txtNovaSenha.setBorder(new LineBorder(corBordaTextos));
		super.add(txtNovaSenha);
		
		lblNovaSenha = new JLabel("New password:");
		lblNovaSenha.setSize(lblNovaSenha.getPreferredSize());
		lblNovaSenha.setLocation(txtNovaSenha.getX() - lblNovaSenha.getWidth() - DISTANCIA, 
									txtNovaSenha.getY() + (ALTURA_TEXTOS - alturaLabel)/2);
		lblNovaSenha.setForeground(new Color(0,112,207));
		super.add(lblNovaSenha);
		
		txtSenha = new JPasswordField();
		txtSenha.setSize(txtNovaSenhaRep.getSize());
		txtSenha.setLocation(txtNovaSenhaRep.getX(), txtNovaSenha.getY() - ALTURA_TEXTOS - DISTANCIA);
		txtSenha.setBorder(new LineBorder(corBordaTextos));
		super.add(txtSenha);
		
		lblSenha = new JLabel("Current password:");
		lblSenha.setSize(lblSenha.getPreferredSize());
		lblSenha.setLocation(txtSenha.getX() - lblSenha.getWidth() - DISTANCIA, 
							 txtSenha.getY() + (txtSenha.getHeight() - alturaLabel)/2);
		lblSenha.setForeground(new Color(0,112,207));
		super.add(lblSenha);
		
		lblErro = new JLabel();
		lblErro.setForeground(Color.RED);
		lblErro.setSize(LARGURA_DIALOGO - DISTANCIA * 2, lblSenha.getHeight());
		lblErro.setLocation(btnConfirmar.getX(), btnConfirmar.getY() - lblErro.getHeight() - DISTANCIA);
		super.add(lblErro);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnConfirmar){
			
			boolean isSenhaVazio = txtSenha.getPassword().length == 0;
			boolean isNovaSenhaVazio = txtNovaSenha.getPassword().length == 0;
			boolean isNovaSenhaRepVazio = txtNovaSenhaRep.getPassword().length == 0;
			
			//CAMPOS VAZIOS
			if (isSenhaVazio || isNovaSenhaVazio || isNovaSenhaRepVazio){
				
				int numCamposVazios = 0;
				
				if (isSenhaVazio){
					txtSenha.setBorder(new LineBorder (Color.RED));
					numCamposVazios++;
				} 
				if (isNovaSenhaVazio){
					txtNovaSenha.setBorder(new LineBorder(Color.RED));
					numCamposVazios++;
				} 
				if (isNovaSenhaRepVazio){
					txtNovaSenhaRep.setBorder(new LineBorder(Color.RED));
					numCamposVazios++;
				}
				
				if (numCamposVazios == 1){
					lblErro.setText("The red field is empty");
				} else {
					lblErro.setText("The " + numCamposVazios + " red fields are empty");
				}
				
			} else {
				//TODOS OS CAMPOS PREENCHIDOS
				//se as novas senhas nao forem iguais
				if (!txtNovaSenha.getText().equals(txtNovaSenhaRep.getText())){
					pintarTodosCampos();
					lblErro.setText("Passwords does not match");
					txtNovaSenhaRep.setBorder(new LineBorder(Color.RED));
					txtNovaSenhaRep.setText("");
					
				} else {
					//a senha atual nao esta correta
					if (!InformacoesUsuarioLogado.getInstance().getUsuario().getSenhaUsuario().equals(txtSenha.getText())){
						pintarTodosCampos();
						lblErro.setText("Your current password is incorrect");
						txtSenha.setBorder(new LineBorder(Color.RED));
					} else {
						//TUDO CORRETO
						InformacoesUsuarioLogado.getInstance().getUsuario().setSenhaUsuario(txtNovaSenha.getText());
						if (new DAOUsuario().editarUsuario(InformacoesUsuarioLogado.getInstance().getUsuario())){
							new CaixaMensagem().mostrarMensagem("Password successfully changed");
							pintarTodosCampos();
						} else {
							new CaixaMensagem().mostrarMensagem("�... deu erro :b"); 
						}
						
						super.dispose();
						
					}
				}
				
			}
			
		} 
		
		else if (e.getSource() == btnCancelar){
			super.dispose();
		}
		
		else if (e.getSource() == btnFechar){
			super.dispose();
		}
		
	}
	
	private void pintarTodosCampos(){
		txtSenha.setBorder(new LineBorder(corBordaTextos));
		txtNovaSenha.setBorder(new LineBorder(corBordaTextos));
		txtNovaSenhaRep.setBorder(new LineBorder(corBordaTextos));
	}
	
	
}
