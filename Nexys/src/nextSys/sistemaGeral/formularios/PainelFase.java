package nextSys.sistemaGeral.formularios;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;

public class PainelFase extends JPanel{

	//VARIAVEIS
	private boolean isDesbloqueada;
	private int descFase;
	private int largura, altura;
	private int x, y;
	
	private Image imagemFundo = null;
	
	private GerenciamentoFases gs;
	
	//BOTAO DISPARADOR DO EVENTO 
	private JButton btnDisparador;
	
	//CONTRUTOR
	public PainelFase(int x, int y,
					  int largura, int altura, 
					  int descFase){
		super();
		gs = new GerenciamentoFases();
		
		this.x = x;
		this.y = y;
		
		this.largura = largura;
		this.altura = altura;
		
		this.descFase = descFase;
		this.isDesbloqueada = gs.isFaseDesbloqueada(descFase);
		
		carregaImagens();
		iniciaComponentes();
		
	}
	
	private void carregaImagens(){
		
		//animais
		if (descFase == 1){
			if (isDesbloqueada){
				imagemFundo = new ImageIcon ("res/imagens/menus/backgrounds/fundo_fase_1.png").getImage();
			} else {
				imagemFundo = new ImageIcon ("res/imagens/menus/backgrounds/fundo_fase_1_locked.png").getImage();
			}
		
		//comidas
		} else if (descFase == 2){
			if (isDesbloqueada){
				imagemFundo = new ImageIcon ("res/imagens/menus/backgrounds/fundo_fase_2.png").getImage();
			} else {
				imagemFundo = new ImageIcon ("res/imagens/menus/backgrounds/fundo_fase_2_locked.png").getImage();
			}
		
		//objetos
		} else if (descFase == 3){
			if (isDesbloqueada){
				imagemFundo = new ImageIcon ("res/imagens/menus/backgrounds/fundo_fase_3.png").getImage();
			} else {
				imagemFundo = new ImageIcon ("res/imagens/menus/backgrounds/fundo_fase_3_locked.png").getImage();
			}
		}
		
	}
	
	private void iniciaComponentes(){
		
		super.setSize(largura, altura);
		super.setLocation(x, y);
		super.setLayout(null);
		super.setOpaque(false);//OLHA PARA ISSO AQUI, SEU BABACA!
		
		btnDisparador = new JButton();
		btnDisparador.setSize(largura, altura);
		btnDisparador.setLocation(0,0);
		btnDisparador.setOpaque(true);
		btnDisparador.setFocusable(false);
		btnDisparador.setContentAreaFilled(false);
		btnDisparador.setBorder(null);
		super.add(btnDisparador);		
		
	}
	
	@Override
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		if (imagemFundo != null){
			g.drawImage(imagemFundo, 0, 0, largura, altura, null);
		}
	}

	
	//GETTERS
	public JButton getBtnDisparador() {
		return btnDisparador;
	}
	
	public boolean isDesbloqueada() {
		return isDesbloqueada;
	}

	

	
}
