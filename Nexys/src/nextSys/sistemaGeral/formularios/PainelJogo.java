package nextSys.sistemaGeral.formularios;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;

/**
 * 
 * @author Daniel
 * */
public class PainelJogo extends JPanel{

	/*ARRUMAR AS IMAGENS*/
	
	//BOTAO
	private JButton btnEstrela;
	
	//LABEL
	private JLabel lblJogo;
	
	//IMAGENS
	private ImageIcon iconJogo; //296X276
	private ImageIcon iconEstrela, iconEstrelaPress; //296X64
	
	//VARIAVEIS
	private static final int LARGURA_PANEL = 316;
	private static final int ALTURA_PANEL = 380;
	private static final int DISTANCIA = 10;
	private static final int LARGURA_IMAGEM_JOGO = LARGURA_PANEL - DISTANCIA * 2;
	private static final int ALTURA_IMAGEM_JOGO = LARGURA_IMAGEM_JOGO - 30; //30 so para adaptar o tamanho
	private static final int LARGURA_IMAGEM_ESTRELA = LARGURA_IMAGEM_JOGO;
	private static final int ALTURA_IMAGEM_ESTRELA = ALTURA_PANEL - ALTURA_IMAGEM_JOGO - DISTANCIA * 3;
	
	private static final Color AMARELO = new Color(255,198,0,180);
	private static final Color VERDE = new Color(78,207,0,180);
	private static final Color AZUL = new Color(0,152,216,180);
	private static final Color VERMELHO = new Color(255,39,39,180);
	
	private Color corFase;
	
	private int x, y;
	private int descFase, numIdentificador;
	
	private boolean isConquistado;
	
	public PainelJogo(int x, int y, int descFase, int numIdentificador){
		
		this.x = x;
		this.y = y;
		
		this.descFase = descFase;
		this.numIdentificador = numIdentificador;
		
		this.isConquistado = new GerenciamentoFases().isJogoConcluido(descFase, numIdentificador);
		
		carregaImagens();
		iniciaComponentes();
	}
	
	private void carregaImagens(){
		
		//IMAGENS DIFERENTES PARA N�VEIS DIFERENTES
		//1 - JOGO DA MEMORIA
		//2 - QUIZ
		//3 - JOGO DA FORCA
		//fazer depois, n�o �? :b
		if (descFase == 1){
			
			corFase = AMARELO;
			
			if (numIdentificador == 1){
				iconJogo = new ImageIcon("res/imagens/menus/labels/memory_game1.png");
				
			} else if (numIdentificador == 2){
				iconJogo = new ImageIcon("res/imagens/menus/labels/quiz1.png");
				
			} else if (numIdentificador == 3){
				iconJogo = new ImageIcon("res/imagens/menus/labels/forca1.png");
				
			}
			
		} else if (descFase == 2){
			
			corFase = AZUL;
			
			if (numIdentificador == 1){
				iconJogo = new ImageIcon("res/imagens/menus/labels/memory_game2.png");
				
			} else if (numIdentificador == 2){
				iconJogo = new ImageIcon("res/imagens/menus/labels/quiz2.png");
				
			} else if (numIdentificador == 3){
				iconJogo = new ImageIcon("res/imagens/menus/labels/forca2.png");
				
			}
			
			
		} else if (descFase == 3){
			
			corFase = VERDE;
			
			if (numIdentificador == 1){
				iconJogo = new ImageIcon("res/imagens/menus/labels/memory_game3.png");
				
			} else if (numIdentificador == 2){
				iconJogo = new ImageIcon("res/imagens/menus/labels/quiz3.png");
				
			} else if (numIdentificador == 3){
				iconJogo = new ImageIcon("res/imagens/menus/labels/forca3.png");
				
			}
			
		}
		
		Image imageEstrela = new ImageIcon ("res/imagens/menus/botoes/jogar_jogar.png").getImage();
		iconEstrela = new ImageIcon (imageEstrela.getScaledInstance(296, 84, Image.SCALE_DEFAULT));
		
		Image imageEstrelaPress = new ImageIcon ("res/imagens/menus/botoes/jogar_jogar_pressed.png").getImage();
		iconEstrelaPress = new ImageIcon (imageEstrelaPress.getScaledInstance(296, 84, Image.SCALE_DEFAULT));
		
	}
	
	private void iniciaComponentes(){
		
		super.setSize(LARGURA_PANEL, ALTURA_PANEL);
		super.setLocation(x, y);
		super.setLayout(null);
		super.setOpaque(false);
		//super.setBackground(corFase);
		
		lblJogo = new JLabel();
		lblJogo.setSize(LARGURA_IMAGEM_JOGO, ALTURA_IMAGEM_JOGO);
		lblJogo.setLocation(DISTANCIA, DISTANCIA);
		lblJogo.setOpaque(true);
		lblJogo.setBackground(new Color(0,0,0,120));
		lblJogo.setIcon(iconJogo);
		super.add(lblJogo);
		
		btnEstrela = new JButton();
		btnEstrela.setSize(LARGURA_IMAGEM_ESTRELA, ALTURA_IMAGEM_ESTRELA);
		btnEstrela.setLocation(DISTANCIA, ALTURA_IMAGEM_JOGO + DISTANCIA * 2);
		btnEstrela.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnEstrela.setIcon(iconEstrela);
		btnEstrela.setPressedIcon(iconEstrelaPress);
		btnEstrela.setBorder(null);
		btnEstrela.setContentAreaFilled(false);
		btnEstrela.setFocusable(false);
		btnEstrela.setOpaque(false);
		//btnEstrela.setIcon(iconEstrela);
		super.add(btnEstrela);
	}
	
	public void paintComponent (Graphics g){
		super.paintComponent(g);
		g.setColor(corFase);
	    g.fillRect(0, 0, getWidth(), getHeight());
	}

	public boolean isConquistado() {
		return isConquistado;
	}

	public JButton getBtnEstrela() {
		return btnEstrela;
	}
}
