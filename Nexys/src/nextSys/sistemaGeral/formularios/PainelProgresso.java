package nextSys.sistemaGeral.formularios;

import java.awt.Color;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;

public class PainelProgresso extends JPanel{

	//LABELS
	private JLabel lblJogoMemoria;
	private JLabel lblJogoQuiz;
	private JLabel lblJogoForca;
	
	private JLabel lblEstrelaJogoMemoria;
	private JLabel lblEstrelaJogoQuiz;
	private JLabel lblEstrelaJogoForca;
	
	//ICONES	
	private ImageIcon iconJogoMemoria;
	private ImageIcon iconJogoQuiz;
	private ImageIcon iconJogoForca;
	
	private ImageIcon iconEstrelaAtivada;
	private ImageIcon iconEstrelaDesativada;
	
	//VARIAVEIS
	private int x, y;
	private int descFase;
	private static final int LARGURA_PAINEL = 647;	
	private static final int ALTURA_PAINEL = 150;
	
	private GerenciamentoFases gf;
			
	//width=647,height=560		
	//CONTRUTOR
	public PainelProgresso(int x, int y, int descFase) {
		
		this.x = x;
		this.y = y;
		this.descFase = descFase;
		
		gf = new GerenciamentoFases();
		
		carregarImagens();
		iniciaComponentes();
	}
	
	public void carregarImagens(){
		
		//IMAGENS ESTRELAS
		iconEstrelaAtivada = new ImageIcon("res/imagens/menus/labels/estrela_ativada_menu_inicial.png");
		iconEstrelaDesativada = new ImageIcon("res/imagens/menus/labels/estrela_desativada_menu_inicial.png");
		
		iconJogoMemoria = new ImageIcon ("res/imagens/menus/labels/label_memoria_"+descFase+"_menu_inicial.png");
		iconJogoQuiz = new ImageIcon ("res/imagens/menus/labels/label_quiz_"+descFase+"_menu_inicial.png");
		iconJogoForca = new ImageIcon ("res/imagens/menus/labels/label_forca_"+descFase+"_menu_inicial.png");
	}
	
	public void iniciaComponentes(){
		
		super.setSize(LARGURA_PAINEL, ALTURA_PAINEL);
		super.setLocation(x, y);
		super.setLayout(null);
		
		//JOGO DA MEMORIA
		lblJogoMemoria = new JLabel();
		lblJogoMemoria.setIcon(iconJogoMemoria);
		lblJogoMemoria.setSize(LARGURA_PAINEL, 50);
		lblJogoMemoria.setLocation(0, 0);
		lblJogoMemoria.setOpaque(true);
		
		boolean isJogoMemoriaConcluido = gf.isJogoConcluido(descFase, 1);
		lblEstrelaJogoMemoria = new JLabel();
		lblEstrelaJogoMemoria.setSize(50, 50);
		lblEstrelaJogoMemoria.setLocation(LARGURA_PAINEL - 50, 0);
		if (isJogoMemoriaConcluido){
			lblEstrelaJogoMemoria.setIcon(iconEstrelaAtivada);
		} else {
			lblEstrelaJogoMemoria.setIcon(iconEstrelaDesativada);
		}
		
		super.add(lblEstrelaJogoMemoria);
		super.add(lblJogoMemoria);
		
		//QUIZ
		lblJogoQuiz = new JLabel();
		lblJogoQuiz.setSize(LARGURA_PAINEL, 50);
		lblJogoQuiz.setLocation(0, 50);
		lblJogoQuiz.setIcon(iconJogoQuiz);
		
		boolean isJogoQuizConcluido = gf.isJogoConcluido(descFase, 2);
		lblEstrelaJogoQuiz = new JLabel();
		lblEstrelaJogoQuiz.setSize(50, 50);
		lblEstrelaJogoQuiz.setLocation(LARGURA_PAINEL - 50, 50);
		if (isJogoQuizConcluido){
			lblEstrelaJogoQuiz.setIcon(iconEstrelaAtivada);
		} else {
			lblEstrelaJogoQuiz.setIcon(iconEstrelaDesativada);
		}
		
		super.add(lblEstrelaJogoQuiz);
		super.add(lblJogoQuiz);
		
		//FORCA
		lblJogoForca = new JLabel();
		lblJogoForca.setSize(LARGURA_PAINEL, 50);
		lblJogoForca.setLocation(0, 100);
		lblJogoForca.setIcon(iconJogoForca);
		
		boolean isJogoForcaConcluido = gf.isJogoConcluido(descFase, 3);
		lblEstrelaJogoForca = new JLabel();
		lblEstrelaJogoForca.setSize(50, 50);
		lblEstrelaJogoForca.setLocation(LARGURA_PAINEL - 50, 100);
		if (isJogoForcaConcluido){
			lblEstrelaJogoForca.setIcon(iconEstrelaAtivada);
		} else {
			lblEstrelaJogoForca.setIcon(iconEstrelaDesativada);
		}
		
		super.add(lblEstrelaJogoForca);
		super.add(lblJogoForca);
	}
	
	
	
	
}
