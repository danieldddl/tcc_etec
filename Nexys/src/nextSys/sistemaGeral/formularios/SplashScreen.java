package nextSys.sistemaGeral.formularios;

import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class SplashScreen extends JFrame {

    private int duracao;
    private JLabel lblImagem = new JLabel();

    //setando pelo construtor o tempo
    public SplashScreen(int duracao) {
        this.duracao = duracao;
    }

    public void mostrarSplash() {        
        
        setLayout(null);
        setUndecorated(true);
        setSize(960,720);
        setLocationRelativeTo(null);
        setBackground(new Color(1.0f,1.0f,1.0f,0f)); //deixando o fundo invisivel
        setResizable(false);
       
        lblImagem.setIcon(new ImageIcon("res/imagens/menus/backgrounds/splash.gif"));
        lblImagem.setSize(lblImagem.getPreferredSize());
        lblImagem.setLocation(0,0);
        add(lblImagem);
        
        setVisible(true);

        // tempo de espera
        try { 
            Thread.sleep(duracao); 
        } catch (Exception e) {
        
        }        
        
        setVisible(false);        
    }
}