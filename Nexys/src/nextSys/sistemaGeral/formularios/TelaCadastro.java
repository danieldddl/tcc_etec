package nextSys.sistemaGeral.formularios;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.plaf.ColorUIResource;

import nextSys.sistemaGeral.funcionalidades.DAOUsuario;
import nextSys.sistemaGeral.funcionalidades.Usuario;
import nextSys.sistemaGeral.funcionalidades.ValidacaoCampos;

/**
 *
 * @author Daniel
 */
public class TelaCadastro extends JFrame implements ActionListener, FocusListener, KeyListener{

    //vari�veis dos widgets
	private JLabel lblNome, lblSobrenome, lblApelido, lblLogin, lblSenha, lblRepetirSenha;
	private JTextField txtNome, txtSobrenome, txtApelido, txtLogin;
	private JPasswordField txtSenha, txtRepetirSenha;
	private JButton btnCadastrar, btnCancelar, btnFechar, btnMinimizar;
    
    //widgets de valida��o
	private JLabel lblValidacaoNome, lblValidacaoSobrenome, lblValidacaoLogin, lblValidacaoSenha, lblValidacaoRepetirSenha;
    
    //visual
	private JLabel lblTituloTelaCadastro;
    
	private boolean nomeJaFocado, sobrenomeJaFocado, loginJaFocado, senhaJaFocado, repetirSenhaJaFocado;
    
	private Icon iconAviso, iconBotaoCadastrar, iconBotaoCancelar, iconTituloTela;
	private Icon iconBotaoFechar, iconBotaoMinimizar, iconBotaoFecharPressionado, iconBotaoMinimizarPressionado;
    
	private Color padrao = Color.LIGHT_GRAY, 
		    	  errado = Color.RED, 
		    	  valido = Color.BLUE, 
		    	  naoNecessario = new Color(33, 200, 255);
          
    ValidacaoCampos vc = new ValidacaoCampos();
    CaixaMensagem cm = new CaixaMensagem();
    
    public TelaCadastro (){
        super();
        iniciaComponentes();

    }
    
    public void iniciaComponentes(){
        
        //frame
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);        
        setSize(850, 500);
        setResizable(false);
        setUndecorated(true);
        setLocationRelativeTo(null);
        
        //calculo de posicionamento
        int distanciaAltura = 15, distanciaLargura = 10, larguraTotal;
        int larguraTxts = 150, alturaTxts = 25;
        
        try {
            //imagem das labels de aviso
            Image imagemAviso = ImageIO.read (
                    new File("res/imagens/menus/labels/warning.png"));
            iconAviso = new ImageIcon(
                    imagemAviso.getScaledInstance(alturaTxts, alturaTxts, Image.SCALE_DEFAULT));
            
            Image imagemCadastrar = ImageIO.read (
                    new File("res/imagens/menus/botoes/botao_cadastrar.png"));
            iconBotaoCadastrar = new ImageIcon(
                    imagemCadastrar.getScaledInstance(130, 35, Image.SCALE_DEFAULT));
            
            Image imagemCancelar = ImageIO.read (
                    new File("res/imagens/menus/botoes/botao_cancelar.png"));
            iconBotaoCancelar = new ImageIcon(
                    imagemCancelar.getScaledInstance(130, 35, Image.SCALE_DEFAULT));
            
            Image imagemTituloTela = ImageIO.read (
                    new File("res/imagens/menus/titulos/titulo_form_cadastrar.png"));
            iconTituloTela = new ImageIcon(
                    imagemTituloTela.getScaledInstance(400, 100, Image.SCALE_DEFAULT));
            
            //botoes de fechar e minimizar
            Image imagemBotaoFechar = ImageIO.read (
                    new File("res/imagens/menus/botoes/close.png"));
            iconBotaoFechar = new ImageIcon(
                    imagemBotaoFechar.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
            
            Image imagemBotaoMinimizar = ImageIO.read (
                    new File("res/imagens/menus/botoes/minimize.png"));
            iconBotaoMinimizar = new ImageIcon(
                    imagemBotaoMinimizar.getScaledInstance(40, 40, Image.SCALE_DEFAULT));

            Image imagemBotaoFecharPressionado = ImageIO.read (
                    new File("res/imagens/menus/botoes/close-pressed.png"));
            iconBotaoFecharPressionado = new ImageIcon(
                    imagemBotaoFecharPressionado.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
            
            Image imagemBotaoMinimizarPressionado = ImageIO.read (
                    new File("res/imagens/menus/botoes/minimize-pressed.png"));
            iconBotaoMinimizarPressionado = new ImageIcon(
                    imagemBotaoMinimizarPressionado.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
            
            //personalizando o hint
            ToolTipManager.sharedInstance().setInitialDelay(1);
            UIManager.put("ToolTip.foreground", new ColorUIResource(Color.WHITE));
            UIManager.put("ToolTip.background", new ColorUIResource(Color.RED));
            Border border = BorderFactory.createLineBorder(Color.WHITE);
            UIManager.put("ToolTip.border", border);
            
            //background
            setContentPane (new JLabel (new ImageIcon (ImageIO.read(
                    new File("res/imagens/menus/backgrounds/background_cadastro.png")))));
        
        } catch (IOException e){
            
        }
                
        //instanciando os widgets 
        //labels
        lblNome = new JLabel("Nome: ");
        lblSobrenome = new JLabel ("Sobrenome: ");
        lblApelido = new JLabel ("Apelido: ");
        lblLogin = new JLabel ("Login: ");
        lblSenha = new JLabel ("Senha: ");
        lblRepetirSenha = new JLabel ("Repetir Senha: ");
        //labels de validação
        lblValidacaoNome = new JLabel();
        lblValidacaoSobrenome = new JLabel();
        lblValidacaoLogin = new JLabel();
        lblValidacaoSenha = new JLabel();
        lblValidacaoRepetirSenha = new JLabel();
        //labels visuais
        lblTituloTelaCadastro = new JLabel();
        
        //texts fields
        txtNome = new JTextField();
        txtSobrenome = new JTextField();
        txtApelido = new JTextField();
        txtLogin = new JTextField();
        txtSenha = new JPasswordField();
        txtRepetirSenha = new JPasswordField();
        
        //botões
        btnCadastrar = new JButton();
        btnCancelar = new JButton();
        btnFechar = new JButton();
        btnMinimizar = new JButton();
        
        //configurando widgets
        //nome
        txtNome.setSize(larguraTxts, alturaTxts); 
        txtNome.setLocation(385, 150); //elemento principal
        txtNome.addFocusListener(this);
        txtNome.setBorder(new LineBorder(padrao));
        txtNome.addKeyListener(this);
        add(txtNome);
        
        lblNome.setSize(lblNome.getPreferredSize());
        lblNome.setLocation(txtNome.getX() - lblNome.getWidth() - distanciaLargura , 
                            txtNome.getY() + (txtNome.getHeight() - lblNome.getHeight())/2);
        add(lblNome);
        
        lblValidacaoNome.setIcon(iconAviso);
        lblValidacaoNome.setSize(lblValidacaoNome.getPreferredSize());
        lblValidacaoNome.setLocation(txtNome.getX() + txtNome.getWidth() + distanciaLargura, txtNome.getY());
        lblValidacaoNome.setToolTipText("O campo \"Nome\" deve ser preenchido");
        lblValidacaoNome.setVisible(false);
        add(lblValidacaoNome);
        
        //sobrenome
        txtSobrenome.setSize(larguraTxts, alturaTxts); 
        txtSobrenome.setLocation(txtNome.getX(), txtNome.getY() + txtNome.getHeight() + distanciaAltura); 
        txtSobrenome.addFocusListener(this);
        txtSobrenome.setBorder(new LineBorder(padrao));
        txtSobrenome.addKeyListener(this);
        add(txtSobrenome);
        
        lblSobrenome.setSize(lblSobrenome.getPreferredSize());
        lblSobrenome.setLocation(txtSobrenome.getX() - lblSobrenome.getWidth() - distanciaLargura ,
                                 txtSobrenome.getY() + (txtSobrenome.getHeight() - lblSobrenome.getHeight())/2);
        add(lblSobrenome);       
        
        lblValidacaoSobrenome.setIcon(iconAviso);
        lblValidacaoSobrenome.setSize(lblValidacaoSobrenome.getPreferredSize());
        lblValidacaoSobrenome.setLocation(txtSobrenome.getX() + txtSobrenome.getWidth() + distanciaLargura, txtSobrenome.getY());
        lblValidacaoSobrenome.setToolTipText("O campo \"Sobrenome\" deve ser preenchido ");
        lblValidacaoSobrenome.setVisible(false);
        add(lblValidacaoSobrenome);
        
        //apelido
        txtApelido.setSize(larguraTxts, alturaTxts); 
        txtApelido.setLocation(txtSobrenome.getX(), txtSobrenome.getY() + txtSobrenome.getHeight() + distanciaAltura); 
        txtApelido.addFocusListener(this);
        txtApelido.setBorder(new LineBorder(naoNecessario));
        txtApelido.addKeyListener(this);
        add(txtApelido);
        
        lblApelido.setSize(lblApelido.getPreferredSize());
        lblApelido.setLocation(txtApelido.getX() - lblApelido.getWidth() - distanciaLargura , 
                               txtApelido.getY() + (txtApelido.getHeight() - lblApelido.getHeight())/2);
        add(lblApelido);
        
        //login
        txtLogin.setSize(larguraTxts, alturaTxts); 
        txtLogin.setLocation(txtApelido.getX(), txtApelido.getY() + txtApelido.getHeight() + distanciaAltura); 
        txtLogin.addFocusListener(this);
        txtLogin.setBorder(new LineBorder(padrao));
        txtLogin.addKeyListener(this);
        add(txtLogin);
        
        lblLogin.setSize(lblLogin.getPreferredSize());
        lblLogin.setLocation(txtLogin.getX() - lblLogin.getWidth() - distanciaLargura , 
                             txtLogin.getY() + (txtLogin.getHeight() - lblLogin.getHeight())/2);
        add(lblLogin);
        
        lblValidacaoLogin.setIcon(iconAviso);
        lblValidacaoLogin.setSize(lblValidacaoLogin.getPreferredSize());
        lblValidacaoLogin.setLocation(txtLogin.getX() + txtLogin.getWidth() + distanciaLargura, txtLogin.getY());
        lblValidacaoLogin.setToolTipText("O campo \"Login\" deve ter no m�nio 6 caracteres e "
                                       + "\n n�o pode j� estar cadastrado");
        lblValidacaoLogin.setVisible(false);
        add(lblValidacaoLogin);
        
        //senha
        txtSenha.setSize(larguraTxts, alturaTxts); 
        txtSenha.setLocation(txtLogin.getX(), txtLogin.getY() + txtLogin.getHeight() + distanciaAltura);
        txtSenha.addFocusListener(this);
        txtSenha.setBorder(new LineBorder(padrao));
        txtSenha.addKeyListener(this);
        add(txtSenha);
        
        lblSenha.setSize(lblSenha.getPreferredSize());
        lblSenha.setLocation(txtSenha.getX() - lblSenha.getWidth() - distanciaLargura , 
                             txtSenha.getY() + (txtSenha.getHeight() - lblSenha.getHeight())/2);
        add(lblSenha);
        
        lblValidacaoSenha.setIcon(iconAviso);
        lblValidacaoSenha.setSize(lblValidacaoSenha.getPreferredSize());
        lblValidacaoSenha.setLocation(txtSenha.getX() + txtSenha.getWidth() + distanciaLargura, txtSenha.getY());
        lblValidacaoSenha.setToolTipText("O campo \"Senha\" deve ter no m�nimo 6 caracteres");
        lblValidacaoSenha.setVisible(false);
        add(lblValidacaoSenha);
                
        //repetir senha
        txtRepetirSenha.setSize(larguraTxts, alturaTxts); 
        txtRepetirSenha.setLocation(txtSenha.getX(), txtSenha.getY() + txtSenha.getHeight() + distanciaAltura);
        txtRepetirSenha.addFocusListener(this);
        txtRepetirSenha.setBorder(new LineBorder(padrao));
        txtRepetirSenha.addKeyListener(this);
        add(txtRepetirSenha);
        
        lblRepetirSenha.setSize(lblRepetirSenha.getPreferredSize());
        lblRepetirSenha.setLocation(txtRepetirSenha.getX() - lblRepetirSenha.getWidth() - distanciaLargura , 
                                    txtRepetirSenha.getY() + (txtRepetirSenha.getHeight() - lblRepetirSenha.getHeight())/2);
        add(lblRepetirSenha);
        
        lblValidacaoRepetirSenha.setIcon(iconAviso);
        lblValidacaoRepetirSenha.setSize(lblValidacaoRepetirSenha.getPreferredSize());
        lblValidacaoRepetirSenha.setLocation(txtRepetirSenha.getX() + txtRepetirSenha.getWidth() + distanciaLargura, txtRepetirSenha.getY());
        lblValidacaoRepetirSenha.setToolTipText("O campo \"Senha\" e o campo \"Repetir Senha\" devem ser iguais");
        lblValidacaoRepetirSenha.setVisible(false);
        add(lblValidacaoRepetirSenha);
        
        larguraTotal = lblRepetirSenha.getWidth() + distanciaLargura + txtRepetirSenha.getWidth();
        
        lblTituloTelaCadastro.setIcon(iconTituloTela);
        lblTituloTelaCadastro.setBorder(null);
        lblTituloTelaCadastro.setSize(lblTituloTelaCadastro.getPreferredSize());
        lblTituloTelaCadastro.setLocation(lblRepetirSenha.getX()+(larguraTotal - lblTituloTelaCadastro.getWidth())/2 + 15, 
                              txtNome.getY() - distanciaAltura - lblTituloTelaCadastro.getHeight()- 30);
        add(lblTituloTelaCadastro);
        

        //botões
        btnCadastrar.setBorderPainted(true);
        btnCadastrar.setBorder(null);
        btnCadastrar.setContentAreaFilled(false);
        btnCadastrar.setIcon(iconBotaoCadastrar);
        btnCadastrar.setSize(130,35);        
        btnCadastrar.setLocation(lblRepetirSenha.getX() - 13,
                                 lblRepetirSenha.getY() + lblRepetirSenha.getHeight() + distanciaAltura + 20);
        btnCadastrar.addActionListener(this);
        add(btnCadastrar);
        
        btnCancelar.setBorderPainted(true);
        btnCancelar.setBorder(null);
        btnCancelar.setContentAreaFilled(false);
        btnCancelar.setIcon(iconBotaoCancelar);
        btnCancelar.setSize(130,35);
        btnCancelar.setLocation(btnCadastrar.getX() + btnCadastrar.getWidth() + distanciaLargura, btnCadastrar.getY());
        btnCancelar.addActionListener(this);
        add(btnCancelar);
        
        btnFechar.setBorderPainted(true);
        btnFechar.setBorder(null);
        btnFechar.setContentAreaFilled(false);
        btnFechar.setIcon(iconBotaoFechar);
        btnFechar.setPressedIcon(iconBotaoFecharPressionado);
        btnFechar.setSize(40,40);
        btnFechar.setLocation(getWidth() - btnFechar.getWidth() - 10, 10);
        btnFechar.addActionListener(this);
        add(btnFechar);
        
        btnMinimizar.setBorderPainted(true);
        btnMinimizar.setBorder(null);
        btnMinimizar.setContentAreaFilled(false);
        btnMinimizar.setIcon(iconBotaoMinimizar);
        btnMinimizar.setPressedIcon(iconBotaoMinimizarPressionado);
        btnMinimizar.setSize(40,40);
        btnMinimizar.setLocation(
                getWidth() - btnFechar.getWidth() - btnMinimizar.getWidth() - 10 - distanciaLargura, 10);
        btnMinimizar.addActionListener(this);
        add(btnMinimizar);
                
        setVisible(true);
    }
            
    //eventos
    //clique
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnCadastrar){
            //ação do botão de cadastrar
            if (vc.validarNome(txtNome.getText()) && vc.validarSobrenome(txtSobrenome.getText())
             && vc.validarLogin(txtLogin.getText()) && vc.validarSenha(txtSenha.getText(), txtNome.getText()) 
             && vc.validarRepeticaoSenha(txtRepetirSenha.getText(), txtSenha.getText())){
                
                Usuario u = new Usuario();
                u.setNomeUsuario(txtNome.getText());
                u.setSobrenomeUsuario(txtSobrenome.getText());
                u.setApelidoUsuario(txtApelido.getText());
                u.setLoginUsuario(txtLogin.getText());
                u.setSenhaUsuario(txtSenha.getText());
                
                DAOUsuario daoU = new DAOUsuario();
                
                if (daoU.cadastrarUsuario(u)){
                    cm.mostrarMensagem("Cadastro efetuado com sucesso");
                    TelaLogin tl = new TelaLogin();
                    dispose();
                } else {
                    cm.mostrarMensagem("O cadastro nao pôde ser realizado");
                }
                
            } else {
                cm.mostrarMensagem("Todos os campos devem estar validados :D");
                /*código de teste (não tirar, por enquanto)
                System.out.println(vc.validarNome(txtNome.getText()));
                System.out.println(vc.validarSobrenome(txtSobrenome.getText()));
                System.out.println(vc.validarLogin(txtLogin.getText()));
                System.out.println(vc.validarSenha(txtSenha.getText(), txtNome.getText()));
                System.out.println(vc.validarRepeticaoSenha(txtRepetirSenha.getText(), txtSenha.getText()));*/
            }
            
        } else if(e.getSource() == btnCancelar){
            //ação do botão de cancelar
        	
            TelaLogin tl = new TelaLogin();
            setVisible(false);
            
        } 
        
        else if (e.getSource() == btnFechar){
            //ação do botão de fechar
            CaixaMensagem cm = new CaixaMensagem ();
            
            cm.mostrarPergunta("Você deseja realmente sair do sistema?");
            int resposta = cm.getEscolhido();
            
            if (resposta == 1){
                System.exit(0);
            } else{ 
                //cancelar
            }
        } 
        
        else if (e.getSource() == btnMinimizar){
            //ação do botão de minimizar
            setState(TelaCadastro.ICONIFIED);
        }
    }

    //foco de texto
    @Override
    public void focusGained(FocusEvent e) {
        //ações ocorridas no ganho de foco dos jtextfields
        if (e.getSource() == txtNome){
            nomeJaFocado = true;
        } 
        
        else if (e.getSource() == txtSobrenome){
            sobrenomeJaFocado = true;
        }
        
        else if (e.getSource() == txtApelido){
            
        }
        
        else if (e.getSource() == txtLogin){
            loginJaFocado = true;
        }
        
        else if (e.getSource() == txtSenha){
            senhaJaFocado = true;
        }
        
        else if (e.getSource() == txtRepetirSenha){
            repetirSenhaJaFocado = true;
        }
    }

    @Override
    public void focusLost(FocusEvent e) {
        
        //ações ocorridas na perca de foco dos jtextfields
        if (e.getSource() == txtNome){
            pintarCampos();
        } 
        
        else if (e.getSource() == txtSobrenome){
            pintarCampos();
        }
        
        else if (e.getSource() == txtApelido){
            pintarCampos();
        }
        
        else if (e.getSource() == txtLogin){
            pintarCampos();
        }
        
        else if (e.getSource() == txtSenha){
            pintarCampos();
        }
        
        else if (e.getSource() == txtRepetirSenha){
            pintarCampos();
        }
        
    } 
    
    //eventos de mudança de texto 
    @Override
    public void keyTyped(KeyEvent e) {
        //limitando o número de caracteres 
        if (e.getSource() == txtNome){
            //variável que guarda a última tecla digitada
            int k = e.getKeyChar();
            
            if (txtNome.getText().length() < 100){
            } else {
                e.setKeyChar((char)KeyEvent.VK_CLEAR);
            }
        }
        
        else if (e.getSource() == txtSobrenome){
            
            int k = e.getKeyChar();
            if (txtSobrenome.getText().length() < 100){
                
            } else {
                e.setKeyChar((char)KeyEvent.VK_CLEAR);
            }
        }
        
        else if (e.getSource() == txtApelido){
            int k = e.getKeyChar();
            if (txtApelido.getText().length() < 100){
                
            } else {
                e.setKeyChar((char)KeyEvent.VK_CLEAR);
            }
        }
        
        else if (e.getSource() == txtLogin){
            int k = e.getKeyChar();
            if (txtLogin.getText().length() < 16){
                
            } else {
                e.setKeyChar((char)KeyEvent.VK_CLEAR);
            }
        }
        
        else if (e.getSource() == txtSenha){
            int k = e.getKeyChar();
            if (txtSenha.getText().length() < 16){
                
            } else {
                e.setKeyChar((char)KeyEvent.VK_CLEAR);
            }
        }
        
        else if (e.getSource() == txtRepetirSenha){
            int k = e.getKeyChar();
            if (txtRepetirSenha.getText().length() < 16){
                
            } else {
                e.setKeyChar((char)KeyEvent.VK_CLEAR);
            }
        }
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }
    
    //método executado sempre algum campo é desfocado
    //faz com que todos sejam verificados ao mesmo tempo
    //porque existem alterações em determinados campos que podem fazer alterações em outros
    public void pintarCampos (){
        //nome
        if (!vc.validarNome(txtNome.getText()) && nomeJaFocado){
            lblValidacaoNome.setVisible(true);
            txtNome.setBorder(new LineBorder(errado));  
        } else if (vc.validarNome(txtNome.getText()) && nomeJaFocado){
            txtNome.setBorder(new LineBorder(valido));
            lblValidacaoNome.setVisible(false);
        } 
        else {
            lblValidacaoNome.setVisible(false);
        }
        
        //sobrenome
        if (!vc.validarSobrenome(txtSobrenome.getText()) && sobrenomeJaFocado){
            lblValidacaoSobrenome.setVisible(true);
            txtSobrenome.setBorder(new LineBorder(errado));
        } else if (vc.validarSobrenome(txtSobrenome.getText()) && sobrenomeJaFocado){
            txtSobrenome.setBorder(new LineBorder(valido));
            lblValidacaoSobrenome.setVisible(false);
        }else {
            lblValidacaoSobrenome.setVisible(false);
        }
        
        //login
        if (!vc.validarLogin(txtLogin.getText()) && loginJaFocado){
            lblValidacaoLogin.setVisible(true);
            txtLogin.setBorder(new LineBorder(errado));
        } else if (vc.validarLogin(txtLogin.getText()) && loginJaFocado){
            txtLogin.setBorder(new LineBorder(valido));
            lblValidacaoLogin.setVisible(false);            
        } else {
            lblValidacaoLogin.setVisible(false);
        }
        
        //senha
        if (!vc.validarSenha(txtSenha.getText(), txtNome.getText()) && senhaJaFocado){
            lblValidacaoSenha.setVisible(true);
            txtSenha.setBorder(new LineBorder(errado));
        } else if(vc.validarSenha(txtSenha.getText(), txtNome.getText()) && senhaJaFocado) {
            txtSenha.setBorder(new LineBorder (valido));
            lblValidacaoSenha.setVisible(false);
        } else {
            lblValidacaoSenha.setVisible(false);
        }
        
        //repetir senha
        if (!vc.validarRepeticaoSenha(txtRepetirSenha.getText(), txtSenha.getText()) && repetirSenhaJaFocado){
            lblValidacaoRepetirSenha.setVisible(true);
            txtRepetirSenha.setBorder(new LineBorder(errado));
        }  else if (vc.validarRepeticaoSenha(txtRepetirSenha.getText(), txtSenha.getText()) && repetirSenhaJaFocado){
            txtRepetirSenha.setBorder(new LineBorder (valido));
            lblValidacaoRepetirSenha.setVisible(false);
        } else {
            lblValidacaoRepetirSenha.setVisible(false);
        }
    }
    
}// fim do form de cadastro