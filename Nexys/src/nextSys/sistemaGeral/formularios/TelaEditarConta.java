package nextSys.sistemaGeral.formularios;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import nextSys.sistemaGeral.funcionalidades.DAOUsuario;
import nextSys.sistemaGeral.funcionalidades.InformacoesUsuarioLogado;
import nextSys.sistemaGeral.funcionalidades.Usuario;
import nextSys.sistemaGeral.funcionalidades.ValidacaoCampos;

public class TelaEditarConta extends JFrame implements ActionListener, KeyListener, FocusListener{

	//BOTOES
	private BotaoFechar btnFechar;
	private JButton btnConfirmar;
	
	//LABELS
	private JLabel lblNome;
	private JLabel lblSobrenome;
	private JLabel lblLogin;
	private JLabel lblSenhaAtual;
	private JLabel lblNovaSenha;
	private JLabel lblNovaSenhaRep;
	private JLabel lblApelido;
	
	//TEXT FIELDS
	private JTextField txtNome;
	private JTextField txtSobrenome;
	private JTextField txtLogin;
	private JPasswordField txtSenhaAtual;
	private JPasswordField txtNovaSenha;
	private JPasswordField txtNovaSenhaRep;
	private JTextField txtApelido;
	
	//LABELS DE VALIDACAO
	private JLabel lblValidSenhaAtual;
	private JLabel lblValidSenha;
	private JLabel lblValidSenhaRep;
	
	//CONTAINERS DE COMPONENTES
	private JPanel pnlFundo;
	private JPanel pnlLabels;
	private JPanel pnlTexts;
	private JPanel pnlLabelsValid;
	
	//ICONES
	private Icon iconValid;	

	//COLORS
	private Color colorValido = Color.GREEN;
	private Color colorInvalido = Color.RED;
		
	//BOLEANOS DE FOCO
	private boolean isSenhaAtualJaFocado;
	private boolean isNovaSenhaJaFocado;
	private boolean isNovaSenhaRepJaFocado;
	
	private boolean isSenhaAtualJaDesfocado;
	private boolean isNovaSenhaJaDesfocado;
	private boolean isNovaSenhaRepJaDesfocado;
	
	//INSTANCIAS DE OUTRAS CLASSES DO PROJETO
	private ValidacaoCampos vc;
	private DAOUsuario daoU;
	private Usuario u;
	private CaixaMensagem cm;
	
	//VARIAVEIS USADAS NO POSICIONAMENTO DOS WIDGETS
	private static final int LARGURA_TXTS = 150;
	private static final int ALTURA_TXTS = 25;
	private static final int DISTANCIA_LARGURA = 15;
	private static final int DISTANCIA_ALTURA = 10;
	private static final int LABEL_CENTRO = (ALTURA_TXTS - 16)/2;
	private static final int LARGURA_FRAME = 450;
	private static final int ALTURA_FRAME = 420;
	
	//CONSTRUTOR
	public TelaEditarConta(){
		vc = new ValidacaoCampos();
		daoU = new DAOUsuario();
		u = InformacoesUsuarioLogado.getInstance().getUsuario();
		cm = new CaixaMensagem();
		
		carregaImagens();
		iniciaComponentes();
	}
	
	public void carregaImagens(){
		try {
			Image imagemValImage= ImageIO.read (
                    new File("res/imagens/menus/labels/warning.png"));
            iconValid = new ImageIcon(
            		imagemValImage.getScaledInstance(ALTURA_TXTS, ALTURA_TXTS, Image.SCALE_DEFAULT));
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
	}

	public void iniciaComponentes(){
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setUndecorated(true);
		setSize(LARGURA_FRAME, ALTURA_FRAME); 
		setLocationRelativeTo(null);
		setLayout(null);
		
		//PANELS
		pnlFundo = new JPanel();
		pnlFundo.setSize(super.getSize());
		pnlFundo.setLocation(0, 0);
		pnlFundo.setLayout(null);
		
		pnlTexts = new JPanel();
		pnlTexts.setSize(LARGURA_TXTS, (ALTURA_TXTS * 7) + (DISTANCIA_ALTURA * 6));
		pnlTexts.setLocation(LARGURA_FRAME / 2 - ALTURA_TXTS / 2, 100);
		pnlTexts.setLayout(null);
		//pnlTexts.setBackground(Color.BLACK); //---Teste
		pnlFundo.add(pnlTexts);
		
		pnlLabels = new JPanel();
		pnlLabels.setSize(pnlTexts.getSize());
		pnlLabels.setLocation(pnlTexts.getX() - pnlLabels.getWidth(), 100);
		pnlLabels.setLayout(null);
		//pnlLabels.setBackground(Color.ORANGE); //---Teste
		pnlFundo.add(pnlLabels);
		
		pnlLabelsValid = new JPanel();
		pnlLabelsValid.setSize(ALTURA_TXTS, pnlTexts.getHeight());
		pnlLabelsValid.setLocation(pnlTexts.getX() + pnlTexts.getWidth() + (DISTANCIA_LARGURA/2), 
								   pnlTexts.getY());
		pnlLabelsValid.setLayout(null);
		//pnlLabelsValid.setBackground(Color.BLUE); //---Teste
		pnlFundo.add(pnlLabelsValid);
		
		//NOME
		txtNome = new JTextField();
		txtNome.setSize(LARGURA_TXTS, ALTURA_TXTS);
		txtNome.setLocation(0, 0);
		txtNome.setText(u.getNomeUsuario());
		txtNome.setEnabled(false);
		pnlTexts.add(txtNome);
		
		lblNome = new JLabel("First name:");
		lblNome.setSize(lblNome.getPreferredSize());
		lblNome.setLocation(pnlLabels.getWidth() - lblNome.getWidth() - DISTANCIA_LARGURA, 
						    txtNome.getY() + LABEL_CENTRO);
		pnlLabels.add(lblNome);
		
		//SOBRENOME
		txtSobrenome = new JTextField();
		txtSobrenome.setSize(LARGURA_TXTS, ALTURA_TXTS);
		txtSobrenome.setLocation(0, txtNome.getY() + ALTURA_TXTS + DISTANCIA_ALTURA);
		txtSobrenome.setText(u.getSobrenomeUsuario());
		txtSobrenome.setEnabled(false);
		pnlTexts.add(txtSobrenome);
		
		lblSobrenome = new JLabel("Surname:");
		lblSobrenome.setSize(lblSobrenome.getPreferredSize());
		lblSobrenome.setLocation(pnlLabels.getWidth() - lblSobrenome.getWidth() - DISTANCIA_LARGURA, 
								 txtSobrenome.getY() + LABEL_CENTRO);
		pnlLabels.add(lblSobrenome);
		
		//LOGIN
		txtLogin = new JTextField();
		txtLogin.setSize(LARGURA_TXTS, ALTURA_TXTS);
		txtLogin.setLocation(0, txtSobrenome.getY() + ALTURA_TXTS + DISTANCIA_ALTURA);
		txtLogin.setText(u.getLoginUsuario());
		txtLogin.setEnabled(false);
		pnlTexts.add(txtLogin);
		
		lblLogin = new JLabel("Login:");
		lblLogin.setSize(lblLogin.getPreferredSize());
		lblLogin.setLocation(pnlLabels.getWidth() - lblLogin.getWidth() - DISTANCIA_LARGURA, 
							 txtLogin.getY() + LABEL_CENTRO);
		pnlLabels.add(lblLogin);
		
		//SENHA ATUAL
		txtSenhaAtual = new JPasswordField();
		txtSenhaAtual.setSize(LARGURA_TXTS, ALTURA_TXTS);
		txtSenhaAtual.setLocation(0, txtLogin.getY() + ALTURA_TXTS + DISTANCIA_ALTURA);
		txtSenhaAtual.addFocusListener(this);
		txtSenhaAtual.addKeyListener(this);
		pnlTexts.add(txtSenhaAtual);
		
		lblSenhaAtual = new JLabel("Current password:");
		lblSenhaAtual.setSize(lblSenhaAtual.getPreferredSize());
		lblSenhaAtual.setLocation(pnlLabels.getWidth() - lblSenhaAtual.getWidth() - DISTANCIA_LARGURA, 
								  txtSenhaAtual.getY() + LABEL_CENTRO);
		pnlLabels.add(lblSenhaAtual);
		
		lblValidSenhaAtual = new JLabel();
		lblValidSenhaAtual.setIcon(iconValid);
		lblValidSenhaAtual.setSize(lblValidSenhaAtual.getPreferredSize());
		lblValidSenhaAtual.setLocation(0, txtSenhaAtual.getY());
		lblValidSenhaAtual.setVisible(false);
		pnlLabelsValid.add(lblValidSenhaAtual);
		
		//NOVA SENHA
		txtNovaSenha = new JPasswordField();
		txtNovaSenha.setSize(LARGURA_TXTS, ALTURA_TXTS);
		txtNovaSenha.setLocation(0, txtSenhaAtual.getY() + ALTURA_TXTS + DISTANCIA_ALTURA);
		txtNovaSenha.addFocusListener(this);
		txtNovaSenha.addKeyListener(this);
		pnlTexts.add(txtNovaSenha);
		
		lblNovaSenha = new JLabel("New password:");
		lblNovaSenha.setSize(lblNovaSenha.getPreferredSize());
		lblNovaSenha.setLocation(pnlLabels.getWidth() - lblNovaSenha.getWidth() - DISTANCIA_LARGURA, 
								 txtNovaSenha.getY() + LABEL_CENTRO);
		pnlLabels.add(lblNovaSenha);
		
		lblValidSenha = new JLabel();
		lblValidSenha.setIcon(iconValid);
		lblValidSenha.setSize(lblValidSenha.getPreferredSize());
		lblValidSenha.setLocation(0, txtNovaSenha.getY());
		lblValidSenha.setVisible(false);
		pnlLabelsValid.add(lblValidSenha);
		
		//REPETIR NOVA SENHA
		txtNovaSenhaRep = new JPasswordField();
		txtNovaSenhaRep.setSize(LARGURA_TXTS, ALTURA_TXTS);
		txtNovaSenhaRep.setLocation(0, txtNovaSenha.getY() + ALTURA_TXTS + DISTANCIA_ALTURA);
		txtNovaSenhaRep.addFocusListener(this);
		txtNovaSenhaRep.addKeyListener(this);
		pnlTexts.add(txtNovaSenhaRep);
		
		lblNovaSenhaRep = new JLabel("Re-type new password:"); //---"Re-type new"
		lblNovaSenhaRep.setSize(lblNovaSenhaRep.getPreferredSize());
		lblNovaSenhaRep.setLocation(pnlLabels.getWidth() - lblNovaSenhaRep.getWidth() - DISTANCIA_LARGURA, 
									txtNovaSenhaRep.getY() + LABEL_CENTRO);
		pnlLabels.add(lblNovaSenhaRep);
		
		lblValidSenhaRep = new JLabel();
		lblValidSenhaRep.setIcon(iconValid);
		lblValidSenhaRep.setSize(lblValidSenhaRep.getPreferredSize());
		lblValidSenhaRep.setLocation(0, txtNovaSenhaRep.getY());
		lblValidSenhaRep.setVisible(false);
		pnlLabelsValid.add(lblValidSenhaRep);
		
		//APELIDO
		txtApelido = new JTextField();
		txtApelido.setSize(LARGURA_TXTS, ALTURA_TXTS);
		txtApelido.setLocation(0, txtNovaSenhaRep.getY() + ALTURA_TXTS + DISTANCIA_ALTURA);
		txtApelido.setText(u.getApelidoUsuario());
		pnlTexts.add(txtApelido);
		
		lblApelido = new JLabel("Nickname:");
		lblApelido.setSize(lblApelido.getPreferredSize());
		lblApelido.setLocation(pnlLabels.getWidth() - lblApelido.getWidth() - DISTANCIA_LARGURA, 
							   txtApelido.getY() + LABEL_CENTRO);
		pnlLabels.add(lblApelido);
		
		btnConfirmar = new JButton("Confirm");
		btnConfirmar.setSize(LARGURA_TXTS, 35);
		btnConfirmar.setLocation((LARGURA_FRAME - btnConfirmar.getWidth())/2, 
							      pnlTexts.getY() + pnlTexts.getHeight() + DISTANCIA_ALTURA * 2);
		btnConfirmar.addActionListener(this);
		pnlFundo.add(btnConfirmar);
		
		btnFechar = new BotaoFechar(this, null, 1, 35, 35, 10, 10);
		pnlFundo.add(btnFechar);
				
		add(pnlFundo);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == btnConfirmar){
			
			if (u.getSenhaUsuario().equals(txtSenhaAtual.getText()) && 
				vc.validarSenha(txtNovaSenha.getText(), u.getNomeUsuario()) &&
				vc.validarRepeticaoSenha(txtNovaSenhaRep.getText(), txtNovaSenha.getText())){
				
				u.setSenhaUsuario(txtNovaSenha.getText());
				u.setApelidoUsuario(txtApelido.getText());
				
				daoU.editarUsuario(u);
				
				cm.mostrarMensagem("Informations changed with sucess");
				super.dispose();
				
			} else {
				
				cm.mostrarMensagem("All the fields must be validated");
				
			}
			
		}
		
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		
		if (e.getSource() == txtSenhaAtual){
			
		} 
		
		else if (e.getSource() == txtNovaSenha){
			int k = e.getKeyChar();
            
            if (txtNovaSenha.getText().length() < 16){
            } else {
                e.setKeyChar((char)KeyEvent.VK_CLEAR);
            }
            
            pintarCampos();
		}
		
		else if (e.getSource() == txtNovaSenhaRep){
			int k = e.getKeyChar();
            
            if (txtNovaSenha.getText().length() < 16){
            } else {
                e.setKeyChar((char)KeyEvent.VK_CLEAR);
            }
            
            pintarCampos();
		}
		
	}

	@Override
	public void focusGained(FocusEvent e) {

		if (e.getSource() == txtSenhaAtual){
			isSenhaAtualJaFocado = true;
		} 
		
		else if (e.getSource() == txtNovaSenha){
			isNovaSenhaJaFocado = true;
		}
		
		else if (e.getSource() == txtNovaSenhaRep){
			isNovaSenhaRepJaFocado = true;
		}
		
	}

	@Override
	public void focusLost(FocusEvent e) {
		
		if (e.getSource() == txtSenhaAtual){
			isSenhaAtualJaDesfocado = true;
			pintarCampos();
		} 
		
		else if (e.getSource() == txtNovaSenha){
			isNovaSenhaJaDesfocado = true;
			pintarCampos();
		}
		
		else if (e.getSource() == txtNovaSenhaRep){
			isNovaSenhaRepJaDesfocado = true;
			pintarCampos();
		}
		
	}
	
	private void pintarCampos(){
		
		//NOVA SENHA
		if (isNovaSenhaJaDesfocado){
			if (vc.validarSenha(txtNovaSenha.getText(), u.getLoginUsuario())){
				txtNovaSenha.setBorder(new LineBorder (colorValido));
				lblValidSenha.setVisible(false);
			} else {
				txtNovaSenha.setBorder(new LineBorder (colorInvalido));
				lblValidSenha.setVisible(true);
			}
		}
		
		//REPETIR NOVA SENHA
		if (isNovaSenhaRepJaDesfocado){
			if (vc.validarRepeticaoSenha(txtNovaSenha.getText(), txtNovaSenhaRep.getText())){
				txtNovaSenhaRep.setBorder(new LineBorder (colorValido));
				lblValidSenhaRep.setVisible(false);
			} else {
				txtNovaSenhaRep.setBorder(new LineBorder (colorInvalido));
				lblValidSenhaRep.setVisible(true);
			}
		}
		
	}

}
