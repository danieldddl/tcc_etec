package nextSys.sistemaGeral.formularios;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;

public abstract class TelaFase extends JFrame implements ActionListener{

	/*COLOCAR A LABEL DE PROGRESSO, A IMAGEM DA ESTRELAS*/
	/*O PROGRESSO DE CADA FASE VAI FICAR NOS FORMUL�RIO FILHOS*/
	
	/*ARRUMAR A IMAGEM DO BOT�O DE VOLTAR*/
	
	//PANELS
	private JPanel pnlBarraInferior;
	
	//BOTOES
	private BotaoFechar btnFechar;
	private JButton btnMinimizar;
	private JButton btnVoltar;
	
	//LABELS
	private JLabel lblProgressoEstrelas;
	
	//IMAGENS
	ImageIcon iconBackground;
	ImageIcon iconProgressoEstrelas;
	ImageIcon iconBtnMinimizar, iconBtnMinimizarPress;
	ImageIcon iconBtnVoltar, iconBtnVoltarPress;
	
    private ImageIcon iconZeroEstrela;
	private ImageIcon iconUmaEstrela;
    private ImageIcon iconDuasEstrela;
    private ImageIcon iconTresEstrela;
    
	//VARIAVEIS
	private int descFase;
	private static final int LARGURA_FRAME = 1030;
	private static final int ALTURA_FRAME = 660;
	private static final int DISTANCIA = 20;
	
	private static final Color AMARELO = new Color(255,198,0,180);
	private static final Color VERDE = new Color(78,207,0,180);
	private static final Color AZUL = new Color(0,152,216,180);
	private static final Color VERMELHO = new Color(255,39,39,180);
	
	private Color corFase;
	
	//CONTRUTOR
	public TelaFase (int descFase){
		this.descFase = descFase;
		carregaImagensBase();
		iniciaComponentesBase();
	}
	
	public void carregaImagensBase(){
		//BOTAO DE MINIMIZAR
		if (descFase == BotaoFechar.VERMELHO){
			Image imageBtnMinimizar = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_vermelho.png").getImage();
			iconBtnMinimizar = new ImageIcon (imageBtnMinimizar.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
			
			Image imageBtnMinimizarPress = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_vermelho_press.png").getImage();
			iconBtnMinimizarPress = new ImageIcon (imageBtnMinimizarPress.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
			
			corFase = VERMELHO;
			
		} else if (descFase == BotaoFechar.AMARELO){
			Image imageBtnMinimizar = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_amarelo.png").getImage();
			iconBtnMinimizar = new ImageIcon (imageBtnMinimizar.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
			
			Image imageBtnMinimizarPress = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_amarelo_press.png").getImage();
			iconBtnMinimizarPress = new ImageIcon (imageBtnMinimizarPress.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
			
			corFase = AMARELO;
			
		} else if (descFase == BotaoFechar.VERDE){
			Image imageBtnMinimizar = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_verde.png").getImage();
			iconBtnMinimizar = new ImageIcon (imageBtnMinimizar.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
			
			Image imageBtnMinimizarPress = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_verde_press.png").getImage();
			iconBtnMinimizarPress = new ImageIcon (imageBtnMinimizarPress.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
			
			corFase = VERDE;
			
		} else if (descFase == BotaoFechar.AZUL){
			Image imageBtnMinimizar = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_azul.png").getImage();
			iconBtnMinimizar = new ImageIcon (imageBtnMinimizar.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
			
			Image imageBtnMinimizarPress = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_azul_press.png").getImage();
			iconBtnMinimizarPress = new ImageIcon (imageBtnMinimizarPress.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
			
			corFase = AZUL;
		}
		
		//BACKGROUND
		if (descFase == 1){
			iconBackground = new ImageIcon ("res/imagens/menus/backgrounds/background_facil.png");
			
		} else if (descFase == 2){
			iconBackground = new ImageIcon ("res/imagens/menus/backgrounds/background_medio.png");
			
		} else if (descFase == 3){
			iconBackground = new ImageIcon ("res/imagens/menus/backgrounds/background_dificil.png");
			
		}
		
		Image imagemZeroEstrelas = new ImageIcon("res/imagens/menus/botoes/botao_zero_estrelas.png").getImage();
		Image imagemUmaEstrela = new ImageIcon("res/imagens/menus/botoes/botao_uma_estrelas.png").getImage();
		Image imagemDuasEstrelas = new ImageIcon("res/imagens/menus/botoes/botao_duas_estrelas.png").getImage();
		Image imagemTresEstrelas = new ImageIcon("res/imagens/menus/botoes/botao_tres_estrelas.png").getImage();
		
		switch (new GerenciamentoFases().progressoFase(descFase)){
			case 0:
				iconProgressoEstrelas = new ImageIcon (imagemZeroEstrelas.getScaledInstance(256, 88, Image.SCALE_DEFAULT));
				break;
			case 1:
				iconProgressoEstrelas = new ImageIcon (imagemUmaEstrela.getScaledInstance(256, 88, Image.SCALE_DEFAULT));
				break;
			case 2:
				iconProgressoEstrelas = new ImageIcon (imagemDuasEstrelas.getScaledInstance(256, 88, Image.SCALE_DEFAULT));
				break;
			case 3:
				iconProgressoEstrelas = new ImageIcon (imagemTresEstrelas.getScaledInstance(256, 88, Image.SCALE_DEFAULT));
				break;
		}
		
		iconBtnVoltar = new ImageIcon("res/imagens/menus/botoes/botao_voltar.png");
		iconBtnVoltarPress = new ImageIcon("res/imagens/menus/botoes/botao_voltar_press.png");
		
	}
	
	public void iniciaComponentesBase(){
		
		//FRAME
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);
		super.setUndecorated(true);
		super.setSize(LARGURA_FRAME, ALTURA_FRAME);
		super.setLocationRelativeTo(null);
		super.setContentPane(new JLabel (iconBackground));
		super.setResizable(false);
		super.setLayout(null);
		
		//BOTAO DE FECHAR E MINIMIZAR
		btnFechar = new BotaoFechar(this, null, descFase, 40, 40, 20, 20);
		btnFechar.removeActionListener(btnFechar);
		btnFechar.addActionListener(this);
		super.add(btnFechar);
		
		btnMinimizar = new JButton();
		btnMinimizar.setSize(btnFechar.getSize());
		btnMinimizar.setLocation(btnFechar.getX() - btnMinimizar.getWidth() - 20, btnFechar.getY());
		btnMinimizar.setIcon(iconBtnMinimizar);
		btnMinimizar.setPressedIcon(iconBtnMinimizarPress);
		btnMinimizar.setContentAreaFilled(false);
		btnMinimizar.setFocusable(false);
		btnMinimizar.setBorder(null);
		btnMinimizar.addActionListener(this);
		super.add(btnMinimizar);
		
		//BARRA INFERIOR
		pnlBarraInferior = new JPanel();
		pnlBarraInferior.setSize(LARGURA_FRAME - DISTANCIA * 2, 160);
		pnlBarraInferior.setLocation(DISTANCIA, ALTURA_FRAME - DISTANCIA - pnlBarraInferior.getHeight());
//		pnlBarraInferior.setOpaque(true);
		pnlBarraInferior.setBackground(corFase);
		pnlBarraInferior.setLayout(null);
		
		int yVoltar = pnlBarraInferior.getY() + 52;
		int xVoltar = pnlBarraInferior.getX() + 788;
		
		btnVoltar = new JButton ();
		btnVoltar.setSize(150, 55);
		btnVoltar.setLocation(xVoltar, yVoltar);
		btnVoltar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnVoltar.setIcon(iconBtnVoltar);
		btnVoltar.setPressedIcon(iconBtnVoltarPress);
		btnVoltar.setContentAreaFilled(false);
		btnVoltar.setFocusable(false);
		btnVoltar.setBorder(null);
		btnVoltar.addActionListener(this);
		super.add(btnVoltar);
		
		lblProgressoEstrelas = new JLabel();
		lblProgressoEstrelas.setIcon(iconProgressoEstrelas);
		lblProgressoEstrelas.setSize(lblProgressoEstrelas.getPreferredSize());
		lblProgressoEstrelas.setLocation(20, 36);
		pnlBarraInferior.add(lblProgressoEstrelas);
		
		super.add(pnlBarraInferior);
		
	}
	
	//METODOS A SEREM SOBRESCRITOS
	public abstract void carregaImagens();
	public abstract void iniciaComponentes();

	public BotaoFechar getBtnFechar() {
		return btnFechar;
	}

	public JButton getBtnMinimizar() {
		return btnMinimizar;
	}

	public JButton getBtnVoltar() {
		return btnVoltar;
	}
	
	
	
}
