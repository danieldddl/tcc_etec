package nextSys.sistemaGeral.formularios;

import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import Classes.principal;
import View.JanelaAnimais;

import View.JanelaComidasMedio;
import nextSys.memoryGame.formularios.TelaJogoMemoria;
import nextSys.memoryGame.funcionalidades.Configuracoes;

public class TelaFaseDois extends TelaFase{

	private PainelJogo pnlJogo1;
	private PainelJogo pnlJogo2;
	private PainelJogo pnlJogo3;
	
	private JLabel lblEstrela1;
	private JLabel lblEstrela2;
	private JLabel lblEstrela3;
	
	private ImageIcon iconEstrelaConquistada;
	private ImageIcon iconEstrelaNaoConquistada;
	
	private int descFase;
	
	public TelaFaseDois(int descFase) {
		super(descFase);
		this.descFase = descFase;
		carregaImagens();
		iniciaComponentes();
	}

	@Override
	public void carregaImagens() {
			iconEstrelaConquistada = new ImageIcon ("res/imagens/menus/labels/estrela_conquistada.png");
			iconEstrelaNaoConquistada = new ImageIcon ("res/imagens/menus/labels/estrela_nao_conquistada.png");
	}

	@Override
	public void iniciaComponentes() {
		
		int xInicial = 20;
		int yFases = 80;
		
		//JOGO 1
		pnlJogo1 = new PainelJogo(xInicial, yFases, descFase, 1);
		pnlJogo1.getBtnEstrela().addActionListener(this);
		
		lblEstrela1 = new JLabel ();
		lblEstrela1.setSize(100, 100);
		lblEstrela1.setLocation(pnlJogo1.getX() + (pnlJogo1.getWidth() - lblEstrela1.getWidth())/2, 
								pnlJogo1.getY() - 50);
		if (pnlJogo1.isConquistado()){
			lblEstrela1.setIcon(iconEstrelaConquistada);
		} else {
			lblEstrela1.setIcon(iconEstrelaNaoConquistada);
		}
		
		super.add(lblEstrela1);
		super.add(pnlJogo1);
		
		xInicial += pnlJogo1.getWidth() + 20;
		
		//JOGO 2
		pnlJogo2 = new PainelJogo(xInicial, yFases, descFase, 2);
		pnlJogo2.getBtnEstrela().addActionListener(this);
		
		lblEstrela2 = new JLabel ();
		lblEstrela2.setSize(100, 100);
		lblEstrela2.setLocation(pnlJogo2.getX() + (pnlJogo2.getWidth() - lblEstrela2.getWidth())/2, 
								pnlJogo2.getY() - 50);
		if (pnlJogo2.isConquistado()){
			lblEstrela2.setIcon(iconEstrelaConquistada);
		} else {
			lblEstrela2.setIcon(iconEstrelaNaoConquistada);
		}
		
		super.add(lblEstrela2);
		super.add(pnlJogo2);
		
		xInicial += pnlJogo1.getWidth() + 20;
		
		//JOGO 3
		pnlJogo3 = new PainelJogo(xInicial, yFases, descFase, 3);
		pnlJogo3.getBtnEstrela().addActionListener(this);
		
		lblEstrela3 = new JLabel ();
		lblEstrela3.setSize(100, 100);
		lblEstrela3.setLocation(pnlJogo3.getX() + (pnlJogo3.getWidth() - lblEstrela3.getWidth())/2, 
								pnlJogo3.getY() - 50);
		if (pnlJogo3.isConquistado()){
			lblEstrela3.setIcon(iconEstrelaConquistada);
		} else {
			lblEstrela3.setIcon(iconEstrelaNaoConquistada);
		}
		
		super.add(lblEstrela3);
		super.add(pnlJogo3);
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == super.getBtnFechar()){
			TelaMenuSelecaoFase t = new TelaMenuSelecaoFase();
			t.setVisible(true);
			super.dispose();
		}	
		
		else if (e.getSource() == super.getBtnMinimizar()){
			super.setState(JFrame.ICONIFIED);
		}
		
		else if (e.getSource() == super.getBtnVoltar()){
			TelaMenuSelecaoFase t = new TelaMenuSelecaoFase();
			t.setVisible(true);
			super.dispose();
		}
		
		else if (e.getSource() == pnlJogo1.getBtnEstrela()){
			
			Configuracoes.getInstance().setDificuldade(3);
			Configuracoes.getInstance().setTemaFiguras("comidas");
			java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run() {
	                try {
	                    new TelaJogoMemoria(2).setVisible(true);
	                } catch (IOException ex) {
	                    Logger.getLogger(TelaJogoMemoria.class.getName()).log(Level.SEVERE, null, ex);
	                }
	            }
	        });
			
		} 
		
		else if (e.getSource() == pnlJogo2.getBtnEstrela()){
			
			JanelaComidasMedio j = new JanelaComidasMedio(2);
			j.setVisible(true);
			
			super.dispose();
			
		} 
		
		else if (e.getSource() == pnlJogo3.getBtnEstrela()){
			
			java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run() {
	                new principal("Lista de comidas.txt", 2).setVisible(true);
	            }
	        });
	        
	        super.dispose();
			
		}
		
	}

}
