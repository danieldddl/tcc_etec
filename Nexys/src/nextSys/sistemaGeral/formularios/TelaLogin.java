package nextSys.sistemaGeral.formularios;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import nextSys.sistemaGeral.funcionalidades.DAOUsuario;
import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;
import nextSys.sistemaGeral.funcionalidades.InformacoesUsuarioLogado;

/**
 *
 * @author Daniel
 */
public class TelaLogin extends JFrame implements ActionListener{

    DAOUsuario daoU = new DAOUsuario();
    
    private JLabel lblLogin;
    private JLabel lblSenha;
    
    private JTextField txtLogin;
    private JPasswordField txtSenha;
    
    //label que informa quando a senha e o login estao errados
    private JLabel lblErroLogin;
    
    private JButton btnEntrar;
    private JButton btnCadastrar;
    
    private JButton btnFechar;
    private JButton btnMinimizar;
    
    private Icon iconBotaoEntrar, iconBotaoCadastrar;
    private Icon iconBotaoFechar, iconBotaoMinimizar, iconBotaoFecharPressionado, iconBotaoMinimizarPressionado;
    
    public TelaLogin(){
        iniciaComponentes();
    }
    
    public void iniciaComponentes(){
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(500,250);
        setUndecorated(true);
        setResizable(false);
        setLocationRelativeTo(null);
        
        int distanciaAltura = 15, distanciaLargura = 10;
        int larguraTxts = 150, alturaTxts = 25;
        
        try {
              
            Image imagemEntrar = ImageIO.read(new File("res/imagens/menus/botoes/botao_logar.png"));
            iconBotaoEntrar = new ImageIcon(imagemEntrar.getScaledInstance(130, 35, Image.SCALE_DEFAULT));
            
            Image imagemCadastrar = ImageIO.read (new File("res/imagens/menus/botoes/botao_cadastrar-se.png"));
            iconBotaoCadastrar = new ImageIcon(imagemCadastrar.getScaledInstance(130, 35, Image.SCALE_DEFAULT));
            
            //botoes de fechar e minimizar
            Image imagemBotaoFechar = ImageIO.read (new File("res/imagens/menus/botoes/close.png"));
            iconBotaoFechar = new ImageIcon(imagemBotaoFechar.getScaledInstance(30, 30, Image.SCALE_DEFAULT));
            
            Image imagemBotaoMinimizar = ImageIO.read (new File("res/imagens/menus/botoes/minimize.png"));
            iconBotaoMinimizar = new ImageIcon(imagemBotaoMinimizar.getScaledInstance(30, 30, Image.SCALE_DEFAULT));

            Image imagemBotaoFecharPressionado = ImageIO.read (new File ("res/imagens/menus/botoes/close-pressed.png"));
            iconBotaoFecharPressionado = new ImageIcon(
                     imagemBotaoFecharPressionado.getScaledInstance(30, 30, Image.SCALE_DEFAULT));
            
            Image imagemBotaoMinimizarPressionado = ImageIO.read (new File("res/imagens/menus//botoes/minimize-pressed.png"));
            iconBotaoMinimizarPressionado = new ImageIcon(
                     imagemBotaoMinimizarPressionado.getScaledInstance(30, 30, Image.SCALE_DEFAULT));
            
            Image imagemBackground = ImageIO.read(new File ("res/imagens/menus//backgrounds/tela_login.png"));
            ImageIcon iconBackground = new ImageIcon (
            		imagemBackground.getScaledInstance(super.getWidth(), super.getHeight(), Image.SCALE_DEFAULT));
            
            setContentPane (new JLabel (iconBackground));
            
            
        } catch (Exception e){
            
        }
        
        //login
        txtLogin = new JTextField();
        txtLogin.setSize(larguraTxts,alturaTxts);
        txtLogin.setLocation(320,70);
        txtLogin.setBorder(new LineBorder(Color.ORANGE));
        add(txtLogin);
        
        lblLogin = new JLabel ("Login: ");
        lblLogin.setSize(lblLogin.getPreferredSize());
        lblLogin.setLocation(txtLogin.getX() - lblLogin.getWidth() - distanciaLargura,
                             txtLogin.getY() + (txtLogin.getHeight() - lblLogin.getHeight())/2);
        add(lblLogin);
        
        //senha
        txtSenha = new JPasswordField();
        txtSenha.setSize(larguraTxts,alturaTxts);
        txtSenha.setLocation (txtLogin.getX(), txtLogin.getY() + txtLogin.getHeight() + distanciaAltura);
        txtSenha.setBorder(new LineBorder(Color.ORANGE));
        add(txtSenha);
        
        lblSenha = new JLabel ("Senha: ");
        lblSenha.setSize(lblSenha.getPreferredSize());
        lblSenha.setLocation(txtSenha.getX() - lblSenha.getWidth() - distanciaLargura,
                             txtSenha.getY() + (txtSenha.getHeight() - lblSenha.getHeight())/2);
        add(lblSenha);
        
        //botoes de acesso
        btnEntrar = new JButton();
        btnEntrar.setBorderPainted(true);
        btnEntrar.setBorder(null);
        btnEntrar.setContentAreaFilled(false);
        btnEntrar.setIcon(iconBotaoEntrar);
        btnEntrar.setSize(btnEntrar.getPreferredSize());
        btnEntrar.setLocation(200, txtSenha.getY() + alturaTxts + distanciaAltura + 30);
        btnEntrar.addActionListener(this);
        add(btnEntrar);
               
        btnCadastrar = new JButton();
        btnCadastrar.setBorderPainted(true);
        btnCadastrar.setBorder(null);
        btnCadastrar.setContentAreaFilled(false);
        btnCadastrar.setIcon(iconBotaoCadastrar);
        btnCadastrar.setSize(btnCadastrar.getPreferredSize());
        btnCadastrar.setLocation(btnEntrar.getWidth() + btnEntrar.getX() + distanciaLargura, 
                                 btnEntrar.getY());
        btnCadastrar.addActionListener(this);
        add(btnCadastrar);
        
        lblErroLogin = new JLabel("<html><body><font color=\"ff0000\">Senha ou login incorretos</font><body><html>");
        lblErroLogin.setSize(lblErroLogin.getPreferredSize());
        lblErroLogin.setLocation(lblSenha.getX() + 25, lblSenha.getY() + lblSenha.getHeight() + distanciaAltura + 5);
        lblErroLogin.setVisible(false); //fica invisivel até errar a senha e/ou login
        add(lblErroLogin);
        
        btnFechar = new JButton();
        btnFechar.setBorderPainted(true);
        btnFechar.setBorder(null);
        btnFechar.setContentAreaFilled(false);
        btnFechar.setIcon(iconBotaoFechar);
        btnFechar.setPressedIcon(iconBotaoFecharPressionado);
        btnFechar.setSize(btnFechar.getPreferredSize());
        btnFechar.setLocation(getWidth() - btnFechar.getWidth() - 10, 10);
        btnFechar.addActionListener(this);
        add(btnFechar);
        
        btnMinimizar = new JButton();
        btnMinimizar.setBorderPainted(true);
        btnMinimizar.setBorder(null);
        btnMinimizar.setContentAreaFilled(false);
        btnMinimizar.setIcon(iconBotaoMinimizar);
        btnMinimizar.setPressedIcon(iconBotaoMinimizarPressionado);
        btnMinimizar.setSize(btnMinimizar.getPreferredSize());
        btnMinimizar.setLocation(
                getWidth() - btnFechar.getWidth() - btnMinimizar.getWidth() - 10 - distanciaLargura, 10);
        btnMinimizar.addActionListener(this);
        add(btnMinimizar);
        
        setVisible(true);
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnEntrar){
             if (daoU.selecionarUsuario(txtLogin.getText(), txtSenha.getText())){
                 InformacoesUsuarioLogado.getInstance().setUsuario(daoU.pegandoUsuarioCompleto(txtLogin.getText()));
                                  
                 InformacoesUsuarioLogado.getInstance().setUsuario(daoU.pegandoUsuarioCompleto(txtLogin.getText()));
                 daoU.alterandoPrimeiraVez(txtLogin.getText());
                 
                 TelaMenuInicial menuPrincipal = new TelaMenuInicial();
                 menuPrincipal.setVisible(true);
                 //TelaFaseTres tfu = new TelaFaseTres(3);
                 
                 dispose();
             } else {
                 lblErroLogin.setVisible(true);
             }
        }
        	
        else if (e.getSource() == btnCadastrar){
          
             TelaCadastro tc = new TelaCadastro();
             dispose();   
         }
         
        else if (e.getSource() == btnFechar){
            CaixaMensagem cm = new CaixaMensagem();
            cm.mostrarPergunta("Voc� deseja realmente sair do neXys? D:");
            int resposta = cm.getEscolhido();            
            
            if (resposta == 1){
                System.exit(0);
            } else {
                //cancelar
            }
        } 
        
        else if (e.getSource() == btnMinimizar){
            //a��o do bot�o de minimizar
            setState(TelaCadastro.ICONIFIED);
        }
    }
}
