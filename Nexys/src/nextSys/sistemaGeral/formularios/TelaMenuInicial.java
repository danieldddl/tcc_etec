package nextSys.sistemaGeral.formularios;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;
import nextSys.sistemaGeral.funcionalidades.InformacoesUsuarioLogado;

public class TelaMenuInicial extends JFrame implements ActionListener{

	//BOTOES
	private JButton btnJogar;
	private JButton btnEditarConta;
	private JButton btnSobre;
	private JButton btnLogout;
	
	private BotaoFechar btnFechar;
	private JButton btnMinimizar;
	
	//PANELS
	private JPanel pnlDesempenho;
	private JPanel pnlBotoes;
	private PainelProgresso pnlProgresso1;
	private PainelProgresso pnlProgresso2;
	private PainelProgresso pnlProgresso3;
	
	//IMAGENS
	//--botoes
	private ImageIcon iconBtnJogar, iconBtnJogarPress;
	private ImageIcon iconBtnEditarConta, iconBtnEditarContaPress;
	private ImageIcon iconBtnSobre, iconBtnSobrePress;
	private ImageIcon iconBtnLogout, iconBtnLogoutPress;
	private ImageIcon iconBtnMinimizar;
	private ImageIcon iconBtnMinimizarPress;
	//--panels
	private ImageIcon iconFundo;
	
	private static final int LARGURA_FRAME = 1030;
	private static final int ALTURA_FRAME = 660;
	private static final int DISTANCIA = 20;
	
	private CaixaMensagem cm;
	private GerenciamentoFases gf;
	
	//CONSTRUTOR
	public TelaMenuInicial(){
		cm = new CaixaMensagem();
		gf = new GerenciamentoFases();
		
		carregaImagens();
		iniciaComponentes();
	}
	
	public void carregaImagens(){
		//BACKGROUND
		iconFundo = new ImageIcon ("res/imagens/menus/backgrounds/background_menu_principal.png");
		
		//BOTOES 
		Image imageBtnJogar = new ImageIcon ("res/imagens/menus/botoes/jogar_menu_inicial.png").getImage();
		iconBtnJogar = new ImageIcon (imageBtnJogar.getScaledInstance(203, 60, Image.SCALE_DEFAULT));
		
		Image imageBtnJogarPress = new ImageIcon ("res/imagens/menus/botoes/jogar_menu_inicial_press.png").getImage();
		iconBtnJogarPress = new ImageIcon (imageBtnJogarPress.getScaledInstance(203, 60, Image.SCALE_DEFAULT));
		
		Image imageBtnEditarConta = new ImageIcon ("res/imagens/menus/botoes/mudar_senha_menu_inicial.png").getImage();
		iconBtnEditarConta = new ImageIcon (imageBtnEditarConta.getScaledInstance(203, 60, Image.SCALE_DEFAULT));
		
		Image imageBtnEditarContaPress = new ImageIcon ("res/imagens/menus/botoes/mudar_senha_menu_inicial_press.png").getImage();
		iconBtnEditarContaPress = new ImageIcon (imageBtnEditarContaPress.getScaledInstance(203, 60, Image.SCALE_DEFAULT));
		
		Image imageBtnSobre = new ImageIcon ("res/imagens/menus/botoes/sobre_menu_inicial.png").getImage();
		iconBtnSobre = new ImageIcon (imageBtnSobre.getScaledInstance(203, 60, Image.SCALE_DEFAULT));
		
		Image imageBtnSobrePress = new ImageIcon ("res/imagens/menus/botoes/sobre_menu_inicial_press.png").getImage();
		iconBtnSobrePress = new ImageIcon (imageBtnSobrePress.getScaledInstance(203, 60, Image.SCALE_DEFAULT));
		
		Image imageBtnLogout = new ImageIcon ("res/imagens/menus/botoes/logout_menu_inicial.png").getImage();
		iconBtnLogout = new ImageIcon (imageBtnLogout.getScaledInstance(203, 60, Image.SCALE_DEFAULT));
		
		Image imageBtnLogoutPress = new ImageIcon ("res/imagens/menus/botoes/logout_menu_inicial_press.png").getImage();
		iconBtnLogoutPress = new ImageIcon (imageBtnLogoutPress.getScaledInstance(203, 60, Image.SCALE_DEFAULT));
		
		//BOTAO DE MINIMIZAR
		Image imageBtnMinimizar = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_amarelo.png").getImage();
		iconBtnMinimizar = new ImageIcon (imageBtnMinimizar.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		
		Image imageBtnMinimizarPress = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_amarelo_press.png").getImage();
		iconBtnMinimizarPress = new ImageIcon (imageBtnMinimizarPress.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
	}
	
	public void iniciaComponentes(){
		
		//FRAME
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);
		super.setResizable(false);
		super.setUndecorated(true);
		super.setSize(LARGURA_FRAME, ALTURA_FRAME);
		super.setLocationRelativeTo(null);
		super.setContentPane(new JLabel(iconFundo));
				
		//BOTOES DE FECHAR E MINIMIZAR
		String mensagem = "Dou you REALLY want to leave Nexys? D:";
		btnFechar = new BotaoFechar(this, mensagem, BotaoFechar.AMARELO, 40, 40, DISTANCIA, DISTANCIA);
		super.add(btnFechar);
		
		btnMinimizar = new JButton ();
		btnMinimizar.setSize(btnFechar.getSize());
		btnMinimizar.setLocation(btnFechar.getX() - btnFechar.getWidth() - DISTANCIA, btnFechar.getY());
		btnMinimizar.setIcon(iconBtnMinimizar);
		btnMinimizar.setPressedIcon(iconBtnMinimizarPress);
		btnMinimizar.setContentAreaFilled(false);
		btnMinimizar.setFocusable(false);
		btnMinimizar.setBorder(null);
		btnMinimizar.addActionListener(this);
		super.add(btnMinimizar);
		
		//PANELS
		int espacoLargura = LARGURA_FRAME - DISTANCIA * 3;
		int espacoAltura  = ALTURA_FRAME - btnFechar.getHeight() - DISTANCIA * 3; 
		
		pnlBotoes = new JPanel();
		pnlBotoes.setSize(espacoLargura / 3, espacoAltura);
		pnlBotoes.setLocation(DISTANCIA, btnFechar.getHeight() + DISTANCIA * 2);
		//pnlBotoes.setBackground(new Color(0,0,0,50));
		pnlBotoes.setOpaque(false);
		pnlBotoes.setLayout(null);
		
		pnlDesempenho = new JPanel();
		pnlDesempenho.setSize(espacoLargura - espacoLargura / 3, espacoAltura);
		pnlDesempenho.setLocation(pnlBotoes.getX() + pnlBotoes.getWidth() + DISTANCIA, pnlBotoes.getY());
		//pnlDesempenho.setBackground(new Color(0,0,0,50));
		pnlDesempenho.setOpaque(false);
		pnlDesempenho.setLayout(null);
				
		pnlProgresso1 = new PainelProgresso(0, 0, 1);
		pnlDesempenho.add(pnlProgresso1);
		
		pnlProgresso2 = new PainelProgresso(0, 150, 2);
		pnlDesempenho.add(pnlProgresso2);
		
		pnlProgresso3 = new PainelProgresso(0, 300, 3);
		pnlDesempenho.add(pnlProgresso3);
		
		//BOTOES
		btnJogar = new JButton();
		btnJogar.setSize(pnlBotoes.getWidth() - DISTANCIA * 6, 60);
		btnJogar.setLocation(DISTANCIA * 3, DISTANCIA);
		btnJogar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnJogar.setIcon(iconBtnJogar);
		btnJogar.setPressedIcon(iconBtnJogarPress);
		btnJogar.setContentAreaFilled(false);
		btnJogar.setBorder(null);
		btnJogar.addActionListener(this);
		pnlBotoes.add(btnJogar);
				
		btnEditarConta = new JButton();
		btnEditarConta.setSize(btnJogar.getSize());
		btnEditarConta.setLocation(btnJogar.getX(), btnJogar.getY() + btnJogar.getHeight() + DISTANCIA);
		btnEditarConta.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnEditarConta.setIcon(iconBtnEditarConta);
		btnEditarConta.setPressedIcon(iconBtnEditarContaPress);
		btnEditarConta.setContentAreaFilled(false);
		btnEditarConta.setBorder(null);
		btnEditarConta.addActionListener(this);
		pnlBotoes.add(btnEditarConta);
		
		btnSobre = new JButton();
		btnSobre.setSize(btnJogar.getSize());
		btnSobre.setLocation(btnJogar.getX(), btnEditarConta.getY() + btnEditarConta.getHeight() + DISTANCIA);
		btnSobre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnSobre.setIcon(iconBtnSobre);
		btnSobre.setPressedIcon(iconBtnSobrePress);
		btnSobre.setContentAreaFilled(false);
		btnSobre.setBorder(null);
		btnSobre.addActionListener(this);
		pnlBotoes.add(btnSobre);
		
		btnLogout = new JButton ();
		btnLogout.setSize(btnJogar.getSize());
		btnLogout.setLocation(btnJogar.getX(), pnlBotoes.getHeight() - btnLogout.getHeight() - DISTANCIA );
		btnLogout.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnLogout.setIcon(iconBtnLogout);
		btnLogout.setPressedIcon(iconBtnLogoutPress);
		btnLogout.setContentAreaFilled(false);
		btnLogout.setBorder(null);
		btnLogout.addActionListener(this);
		pnlBotoes.add(btnLogout);
		
		super.add(pnlDesempenho);
		super.add(pnlBotoes);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnJogar){
			TelaMenuSelecaoFase tmsf = new TelaMenuSelecaoFase();
			tmsf.setVisible(true);
			super.dispose();
		}
		
		else if (e.getSource() == btnSobre){
			
		}
		
		else if (e.getSource() == btnEditarConta){
			DialogoTrocarSenha d = new DialogoTrocarSenha();
			d.setVisible(true);
			
		}
		
		else if (e.getSource() == btnLogout){
			cm.mostrarPergunta("Do you want to leave Nexys? D:");
			int resposta = cm.getEscolhido();
			
			if (resposta == 1){
				InformacoesUsuarioLogado.getInstance().setUsuario(null);
				TelaLogin tl = new TelaLogin();
				super.dispose();
			}
			
			
		}
		
		else if (e.getSource() == btnMinimizar){
			super.setState(JFrame.ICONIFIED);
		}
		
	}
	
	
	
}
