package nextSys.sistemaGeral.formularios;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import nextSys.sistemaGeral.funcionalidades.GerenciamentoFases;

/**
 *
 * @author Daniel
 */
public class TelaMenuPrincipal extends JFrame implements ActionListener{
	
    //JPANELS
    private JPanel pnlFase1;
    private JPanel pnlFase2;
    private JPanel pnlFase3;
    private JPanel pnlUsuario;
	
    //BOTOES DE FASE
    private JButton btnFase1;
    private JButton btnFase2;
    private JButton btnFase3;
    
    //BOTOES NORMAIS
    private JButton btnEditar;
    private JButton btnLogout;
    private JButton btnMinimizar;
    private BotaoFechar btnFechar;
    
    //LABELS
    private JLabel lblTitulo;
    private JLabel lblIdentificadorFase1;
    private JLabel lblIdentificadorFase2;
    private JLabel lblIdentificadorFase3;
    
    //ICONES
    private Icon iconTitulo;
    private Icon iconBackground;
    private Icon iconIdentificadorFase1;
    private Icon iconIdentificadorFase2;
    private Icon iconIdentificadorFase3;
    
    private Icon iconBtnEditar, iconBtnEditarPressed;
    private Icon iconBtnLogout, iconBtnLogoutPressed;
    
    //ESTRELAS
    private Icon iconBloqueado;
    private Icon iconEstrelasFase1;
    private Icon iconEstrelasFase2;
    private Icon iconEstrelasFase3;
    
    private Image imagemBloqueado;
    private Image imagemZeroEstrelas;
    private Image imagemUmaEstrela;
    private Image imagemDuasEstrelas;
    private Image imagemTresEstrelas;
    
    //BOOLEANOS PARA EVITAR A FADIGA :b
    private boolean isFaseUmDesbloqueada;
    private boolean isFaseDoisDesbloqueada;
    private boolean isFaseTresDesbloqueada;
    
    private static final int LARGURA_FRAME = 1030;
    private static final int ALTURA_FRAME = 660;
    
    private static final int DISTANCIA_LARGURA = 10;
    private static final int DISTANCIA_ALTURA = 10;
    
    private static final int TAMANHO_FASE = 330;
    private static final int LARGURA_TOTAL_FASES = TAMANHO_FASE * 3 + DISTANCIA_LARGURA * 2;
    
    GerenciamentoFases gs;
    
    public TelaMenuPrincipal (){
    	gs = new GerenciamentoFases();
    	
    	verificaDesbloqueioFases();
        carregarImagens();
    	iniciaComponentes();
    }
    
    public void verificaDesbloqueioFases(){
    	isFaseUmDesbloqueada = gs.isFaseDesbloqueada(1);
    	isFaseDoisDesbloqueada = gs.isFaseDesbloqueada(2);
    	isFaseTresDesbloqueada = gs.isFaseDesbloqueada(3);
    }
    
    public void carregarImagens(){
    	
    	try {
    		imagemZeroEstrelas = ImageIO.read(new File("res/imagens/menus/botoes/botao_zero_estrelas.png"));
    		imagemUmaEstrela = ImageIO.read(new File("res/imagens/menus/botoes/botao_uma_estrelas.png"));
			imagemDuasEstrelas = ImageIO.read(new File("res/imagens/menus/botoes/botao_duas_estrelas.png"));
			imagemTresEstrelas = ImageIO.read(new File("res/imagens/menus/botoes/botao_tres_estrelas.png"));
			imagemBloqueado = ImageIO.read(new File("res/imagens/menus/botoes/botao_locked.png"));    		
			
			//IDENTIFICADORES DE FASES
			Image imagemIdentificadorFase1 = ImageIO.read(new File("res/imagens/menus/labels/label_fase_um.png"));
			iconIdentificadorFase1 = new ImageIcon(imagemIdentificadorFase1.getScaledInstance(70, 70, Image.SCALE_DEFAULT));
			
			Image imagemIdentificadorFase2 = ImageIO.read(new File("res/imagens/menus/labels/label_fase_dois.png"));
			iconIdentificadorFase2 = new ImageIcon(imagemIdentificadorFase2.getScaledInstance(70, 70, Image.SCALE_DEFAULT));
			
			Image imagemIdentificadorFase3 = ImageIO.read(new File("res/imagens/menus/labels/label_fase_tres.png"));
			iconIdentificadorFase3 = new ImageIcon(imagemIdentificadorFase3.getScaledInstance(70, 70, Image.SCALE_DEFAULT));
			
			//TITULO DO FORM
			Image imagemTitulo = ImageIO.read(new File("res/imagens/menus/labels/label_titulo_menu_principal.png"));
			iconTitulo = new ImageIcon(imagemTitulo.getScaledInstance(1030, 125, Image.SCALE_DEFAULT));
			
			//BOTOES "NORMAIS"
			Image imagemEditar = ImageIO.read(new File("res/imagens/menus/botoes/botao_editar_perfi_pressl.png"));
			iconBtnEditar = new ImageIcon(imagemEditar.getScaledInstance(145, 145, Image.SCALE_DEFAULT));
			
			Image imagemEditarPress = ImageIO.read(new File("res/imagens/menus/botoes/botao_editar_perfil.png"));
			iconBtnEditarPressed = new ImageIcon (imagemEditarPress.getScaledInstance(145, 145, Image.SCALE_DEFAULT));
			
			Image imagemLogout = ImageIO.read(new File("res/imagens/menus/botoes/botao_logout.png"));
			iconBtnLogout = new ImageIcon(imagemLogout.getScaledInstance(145, 145, Image.SCALE_DEFAULT));
			
			Image imagemLogoutPress = ImageIO.read(new File("res/imagens/menus/botoes/botao_logout_press.png"));
			iconBtnLogoutPressed = new ImageIcon (imagemLogoutPress.getScaledInstance(145, 145, Image.SCALE_DEFAULT));
			
			//BACKGROUND
			Image imagemBackground  = ImageIO.read(new File("res/imagens/menus/backgrounds/background_menu_principal.png"));
			iconBackground = new ImageIcon(imagemBackground.getScaledInstance(LARGURA_FRAME, ALTURA_FRAME, Image.SCALE_DEFAULT));
			
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
    	
    	//PRIMEIRA FASE
    	if (gs.progressoFase(1) == 1){
    		iconEstrelasFase1 = new ImageIcon(imagemUmaEstrela.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	} else if (gs.progressoFase(1) == 2){
    		iconEstrelasFase1 = new ImageIcon(imagemDuasEstrelas.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	} else if (gs.progressoFase(1) == 3){
    		iconEstrelasFase1 = new ImageIcon(imagemTresEstrelas.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	} else {
    		iconEstrelasFase1 = new ImageIcon(imagemZeroEstrelas.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	}
    	
    	//SEGUNDA FASE
    	if (gs.progressoFase(2) == 1){
    		iconEstrelasFase2 = new ImageIcon(imagemUmaEstrela.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	} else if (gs.progressoFase(2) == 2){
    		iconEstrelasFase2 = new ImageIcon(imagemDuasEstrelas.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	} else if (gs.progressoFase(2) == 3){
    		iconEstrelasFase2 = new ImageIcon(imagemTresEstrelas.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	} else {
    		iconEstrelasFase2 = new ImageIcon(imagemZeroEstrelas.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	}
    	
    	//TERCEIRA FASE
    	if (gs.progressoFase(3) == 1){
    		iconEstrelasFase3 = new ImageIcon(imagemUmaEstrela.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	} else if (gs.progressoFase(3) == 2){
    		iconEstrelasFase3 = new ImageIcon(imagemDuasEstrelas.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	} else if (gs.progressoFase(3) == 3){
    		iconEstrelasFase3 = new ImageIcon(imagemTresEstrelas.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	} else {
    		iconEstrelasFase3 = new ImageIcon(imagemZeroEstrelas.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	}
    	
    	iconBloqueado = new ImageIcon (imagemBloqueado.getScaledInstance(320, 110, Image.SCALE_DEFAULT));
    	
    }
    
    public void iniciaComponentes(){
        
    	//CONFIGURACOES DO FRAME
    	setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setUndecorated(true);
		setSize(LARGURA_FRAME, ALTURA_FRAME); 
		setLocationRelativeTo(null);
		setLayout(null);
		setContentPane(new JLabel(iconBackground));
		
        //PANELS        
        pnlUsuario = new JPanel();
        pnlUsuario.setSize(LARGURA_TOTAL_FASES, TAMANHO_FASE / 2);
        pnlUsuario.setLocation((getWidth() - pnlUsuario.getWidth()) / 2, 
        						getHeight() - pnlUsuario.getHeight() - DISTANCIA_ALTURA);
        pnlUsuario.setLayout(null);
        pnlUsuario.setBackground(new Color(229,229,229,100));
        
        pnlFase1 = new JPanel();
        pnlFase1.setSize(TAMANHO_FASE, TAMANHO_FASE);
        pnlFase1.setLocation((LARGURA_FRAME - LARGURA_TOTAL_FASES)/2, pnlUsuario.getY() - 
        					  TAMANHO_FASE - DISTANCIA_ALTURA);
        pnlFase1.setBackground(new Color(229,229,229,100));
        pnlFase1.setLayout(null);
        
        pnlFase2 = new JPanel();
        pnlFase2.setSize(TAMANHO_FASE, TAMANHO_FASE);
        pnlFase2.setLocation(pnlFase1.getX() + pnlFase1.getWidth() + DISTANCIA_LARGURA, 
        					 pnlFase1.getY());
        pnlFase2.setBackground(new Color(229,229,229,100));
        pnlFase2.setLayout(null);
        
        pnlFase3 = new JPanel();
        pnlFase3.setSize(TAMANHO_FASE, TAMANHO_FASE);
        pnlFase3.setLocation(pnlFase2.getX() + pnlFase2.getWidth() + DISTANCIA_LARGURA, 
        					 pnlFase2.getY());
        pnlFase3.setBackground(new Color(229,229,229,100));
        pnlFase3.setLayout(null);

        //LABEL DE TITULO
        lblTitulo = new JLabel();
        lblTitulo.setSize(LARGURA_FRAME, 125);
        lblTitulo.setLocation(0, 0);
        lblTitulo.setIcon(iconTitulo);
        add(lblTitulo);
        
        //FASE 1
        lblIdentificadorFase1 = new JLabel();
        lblIdentificadorFase1.setSize(70,70);
        lblIdentificadorFase1.setLocation(pnlFase1.getX() + pnlFase1.getWidth() - 60, pnlFase1.getY() - 10);
        lblIdentificadorFase1.setIcon(iconIdentificadorFase1);
        add(lblIdentificadorFase1);
        
        btnFase1 = new JButton();
        btnFase1.setSize(TAMANHO_FASE - DISTANCIA_LARGURA, TAMANHO_FASE / 3);
        btnFase1.setLocation((pnlFase1.getWidth() - btnFase1.getWidth())/2, 
        					  pnlFase1.getHeight() - btnFase1.getHeight() - (DISTANCIA_ALTURA / 2));
        btnFase1.setBorder(null);
        btnFase1.setBorderPainted(false);
        btnFase1.setContentAreaFilled(false);
        btnFase1.setOpaque(false);
        if (isFaseUmDesbloqueada){
        	btnFase1.setIcon(iconEstrelasFase1);
        } else {
        	btnFase1.setIcon(iconBloqueado);
        }  
        
        pnlFase1.add(btnFase1);
        
        //FASE 2
        lblIdentificadorFase2 = new JLabel();
        lblIdentificadorFase2.setSize(70,70);
        lblIdentificadorFase2.setLocation(pnlFase2.getX() + pnlFase2.getWidth() - 60, pnlFase2.getY() - 10);
        lblIdentificadorFase2.setIcon(iconIdentificadorFase2);
        add(lblIdentificadorFase2);
        
        btnFase2 = new JButton();
        btnFase2.setSize(TAMANHO_FASE - DISTANCIA_LARGURA, TAMANHO_FASE / 3);
        btnFase2.setLocation((pnlFase2.getWidth() - btnFase2.getWidth())/2, 
        					  pnlFase2.getHeight() - btnFase2.getHeight() - (DISTANCIA_ALTURA / 2));
        btnFase2.setBorder(null);
        if (isFaseDoisDesbloqueada){
        	btnFase2.setIcon(iconEstrelasFase2);
        } else {
        	btnFase2.setIcon(iconBloqueado);
        }
        pnlFase2.add(btnFase2);
        
        //FASE 3
        lblIdentificadorFase3 = new JLabel();
        lblIdentificadorFase3.setSize(70,70);
        lblIdentificadorFase3.setLocation(pnlFase3.getX() + pnlFase3.getWidth() - 60, pnlFase3.getY() - 10);
        lblIdentificadorFase3.setIcon(iconIdentificadorFase3);
        add(lblIdentificadorFase3);
        
        btnFase3 = new JButton("Bot�o fase 3");
        btnFase3.setSize(TAMANHO_FASE - DISTANCIA_LARGURA, TAMANHO_FASE / 3);
        btnFase3.setLocation((pnlFase3.getWidth() - btnFase3.getWidth())/2, 
        					  pnlFase3.getHeight() - btnFase3.getHeight() - (DISTANCIA_ALTURA / 2));
        btnFase3.setBorder(null);
        if (isFaseTresDesbloqueada){
        	btnFase3.setIcon(iconEstrelasFase3);
        } else {
        	btnFase3.setIcon(iconBloqueado);
        }
        pnlFase3.add(btnFase3);
        
        //BOTOES DA BARRA DE USUARIOS
        btnLogout = new JButton();
        btnLogout.setSize(pnlUsuario.getHeight() - DISTANCIA_ALTURA * 2, pnlUsuario.getHeight() - DISTANCIA_ALTURA * 2);
        btnLogout.setLocation(pnlUsuario.getWidth() - btnLogout.getWidth() - DISTANCIA_LARGURA, DISTANCIA_ALTURA);
        btnLogout.setIcon(iconBtnLogout);
        btnLogout.setPressedIcon(iconBtnLogoutPressed);
        pnlUsuario.add(btnLogout);
        
        btnEditar = new JButton();
        btnEditar.setSize(pnlUsuario.getHeight() - DISTANCIA_ALTURA * 2, pnlUsuario.getHeight() - DISTANCIA_ALTURA * 2);
        btnEditar.setLocation(pnlUsuario.getWidth() - btnLogout.getWidth() * 2 - DISTANCIA_LARGURA * 2, DISTANCIA_ALTURA);
        btnEditar.setContentAreaFilled(false);
        btnEditar.setBorder(null);
        btnEditar.setIcon(iconBtnEditar);
        btnEditar.setPressedIcon(iconBtnEditarPressed);
        pnlUsuario.add(btnEditar);
        
        //MINIMIZAR E FECHAR
        String mensagem = "Do you really want to leave Nexys? D:";
        btnFechar = new BotaoFechar(this, mensagem, BotaoFechar.VERDE, 60, 60, 10, 10);
        add(btnFechar);
        
        btnMinimizar = new JButton("-");
        btnMinimizar.setSize(60, 60);
        btnMinimizar.setLocation(btnFechar.getX() - 75, btnFechar.getY());
        btnMinimizar.setFocusable(false);
        add(btnMinimizar);

        add(pnlFase1);
        add(pnlFase2);
        add(pnlFase3);
        add(pnlUsuario);
        
        setVisible(true);
    }

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		
	}
    
}