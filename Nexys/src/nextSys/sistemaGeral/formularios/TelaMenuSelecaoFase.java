package nextSys.sistemaGeral.formularios;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class TelaMenuSelecaoFase extends JFrame implements ActionListener{

	//BOTOES
	private JButton btnJogar;
	private JButton btnVoltar;
	private JButton btnProximo;
	private JButton btnAnterior;
	
	private BotaoFechar btnFechar;
	private JButton btnMinimizar;
	
	//LABELS
	private JLabel lblTitulo;
	private JLabel lblProgresso;
	private JLabel lblProgressoNum;
	private JLabel lblLegendaSlide;
	
	//JPANELS
	private JPanel pnlBarraInferior;
	private PainelFase pnlFase1;
	private PainelFase pnlFase2;
	private PainelFase pnlFase3;
	
	//IMAGENS
	private ImageIcon iconBtnJogar, iconBtnJogarPress;
	private ImageIcon iconBtnVoltar, iconBtnVoltarPress;
	private ImageIcon iconBtnProximo;
	private ImageIcon iconBtnProximoPress;
	private ImageIcon iconBtnAnterior;
	private ImageIcon iconBtnAnteriorPress;
	
	private ImageIcon iconLegenda1;
	private ImageIcon iconLegenda2;
	private ImageIcon iconLegenda3;
	
	private ImageIcon iconFundo;
	private ImageIcon iconBtnMinimizar;
	private ImageIcon iconBtnMinimizarPress;
	
	//VARIAVEIS
	private static final int LARGURA_FRAME = 1030;
	private static final int ALTURA_FRAME = 660;
	private static final int LARGURA_PANEL_FASE = 700;
	private static final int ALTURA_PANEL_FASE = 320;
	private static final int DISTANCIA = 20;
	private static final int TAMANHO_BOTAO = 75;
	
	private CaixaMensagem cm;
	private List<PainelFase> listaFase;
	private int indexAtual;
	
	public TelaMenuSelecaoFase(){
		cm = new CaixaMensagem();
		listaFase = new ArrayList<PainelFase>();
		carregaImagens();
		iniciaComponentes();
	}
	
	public void carregaImagens(){
		iconFundo = new ImageIcon ("res/imagens/menus/backgrounds/background_menu_principal.png");
		
		//BOTOES DE CANCELAR E JOGAR
		Image imageBtnJogar = new ImageIcon ("res/imagens/menus/botoes/botao_jogar_menu.png").getImage();
		iconBtnJogar = new ImageIcon (imageBtnJogar.getScaledInstance(150, 55, Image.SCALE_DEFAULT));
		
		Image imageBtnJogarPress = new ImageIcon ("res/imagens/menus/botoes/botao_jogar_press_menu.png").getImage();
		iconBtnJogarPress = new ImageIcon (imageBtnJogarPress.getScaledInstance(150, 55, Image.SCALE_DEFAULT));
		
		Image imageBtnVoltar = new ImageIcon ("res/imagens/menus/botoes/botao_voltar_menu.png").getImage();
		iconBtnVoltar = new ImageIcon (imageBtnVoltar.getScaledInstance(150, 55, Image.SCALE_DEFAULT));
		
		Image imageBtnVoltarPress = new ImageIcon ("res/imagens/menus/botoes/botao_voltar_press_menu.png").getImage();
		iconBtnVoltarPress = new ImageIcon (imageBtnVoltarPress.getScaledInstance(150, 55, Image.SCALE_DEFAULT));
		
		//BOTOES DE PROXIMO E ANTERIOR DO SLIDE
		Image imageBtnMinimizar = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_amarelo.png").getImage();
		iconBtnMinimizar = new ImageIcon (imageBtnMinimizar.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		
		Image imageBtnMinimizarPress = new ImageIcon ("res/imagens/menus/botoes/botao_minimizar_amarelo_press.png").getImage();
		iconBtnMinimizarPress = new ImageIcon (imageBtnMinimizarPress.getScaledInstance(40, 40, Image.SCALE_DEFAULT));
		
		Image imageBtnAnt = new ImageIcon ("res/imagens/menus/botoes/botao_previous.png").getImage();
		iconBtnAnterior = new ImageIcon (imageBtnAnt.getScaledInstance(TAMANHO_BOTAO, TAMANHO_BOTAO, Image.SCALE_DEFAULT));
		
		Image imageAntPress = new ImageIcon ("res/imagens/menus/botoes/botao_previous_press.png").getImage();
		iconBtnAnteriorPress = new ImageIcon (imageAntPress.getScaledInstance(TAMANHO_BOTAO, TAMANHO_BOTAO, Image.SCALE_DEFAULT));
		
		Image imageBtnProx = new ImageIcon ("res/imagens/menus/botoes/botao_next.png").getImage();
		iconBtnProximo = new ImageIcon (imageBtnProx.getScaledInstance(TAMANHO_BOTAO, TAMANHO_BOTAO, Image.SCALE_DEFAULT));
		
		Image imageBtnProxPress = new ImageIcon ("res/imagens/menus/botoes/botao_next_press.png").getImage();
		iconBtnProximoPress = new ImageIcon (imageBtnProxPress.getScaledInstance(TAMANHO_BOTAO, TAMANHO_BOTAO, Image.SCALE_DEFAULT));
		
		//LEGENDAS
		Image imagemLegenda1 = new ImageIcon ("res/imagens/menus/labels/legenda_1.png").getImage();
		iconLegenda1 = new ImageIcon (imagemLegenda1.getScaledInstance(100, 25, Image.SCALE_DEFAULT));
		
		Image imagemLegenda2 = new ImageIcon ("res/imagens/menus/labels/legenda_2.png").getImage();
		iconLegenda2 = new ImageIcon (imagemLegenda2.getScaledInstance(100, 25, Image.SCALE_DEFAULT));		
		
		Image imagemLegenda3 = new ImageIcon ("res/imagens/menus/labels/legenda_3.png").getImage();
		iconLegenda3 = new ImageIcon (imagemLegenda3.getScaledInstance(100, 25, Image.SCALE_DEFAULT));
	}
	
	public void iniciaComponentes(){
	
		//FRAME
		super.setDefaultCloseOperation(EXIT_ON_CLOSE);
		super.setResizable(false);
		super.setUndecorated(true);
		super.setSize(LARGURA_FRAME, ALTURA_FRAME);
		super.setLocationRelativeTo(null);
		super.setContentPane(new JLabel(iconFundo));
		
		//BOTOES DE FECHAR E MINIMIZAR
		btnFechar = new BotaoFechar(this, null, BotaoFechar.AMARELO, 40, 40, DISTANCIA, DISTANCIA);
		btnFechar.removeActionListener(btnFechar);
		btnFechar.addActionListener(this);
		super.add(btnFechar);
			
		btnMinimizar = new JButton ();
		btnMinimizar.setSize(btnFechar.getSize());
		btnMinimizar.setLocation(btnFechar.getX() - btnFechar.getWidth() - DISTANCIA, btnFechar.getY());
		btnMinimizar.setIcon(iconBtnMinimizar);
		btnMinimizar.setPressedIcon(iconBtnMinimizarPress);
		btnMinimizar.setContentAreaFilled(false);
		btnMinimizar.setFocusable(false);
		btnMinimizar.setBorder(null);
		btnMinimizar.addActionListener(this);
		super.add(btnMinimizar);
		
		//PANELS DAS FASES
		int xPnlFase = (LARGURA_FRAME - LARGURA_PANEL_FASE)/2;
		int yPnlFase = btnFechar.getY() + btnFechar.getHeight() + DISTANCIA;
		
		pnlFase1 = new PainelFase(xPnlFase, yPnlFase, LARGURA_PANEL_FASE, ALTURA_PANEL_FASE, 1);
		pnlFase1.getBtnDisparador().addActionListener(this);
		listaFase.add(pnlFase1);
		
		pnlFase2 = new PainelFase(xPnlFase, yPnlFase, LARGURA_PANEL_FASE, ALTURA_PANEL_FASE, 2);
		pnlFase2.getBtnDisparador().addActionListener(this);
		listaFase.add(pnlFase2);
		
		pnlFase3 = new PainelFase(xPnlFase, yPnlFase, LARGURA_PANEL_FASE, ALTURA_PANEL_FASE, 3);
		pnlFase3.getBtnDisparador().addActionListener(this);
		listaFase.add(pnlFase3);
		
		indexAtual = 0;
		super.add(listaFase.get(indexAtual));
		
		lblLegendaSlide = new JLabel();
		lblLegendaSlide.setIcon(iconLegenda1);
		lblLegendaSlide.setSize(lblLegendaSlide.getPreferredSize());
		lblLegendaSlide.setLocation(pnlFase1.getX() + ((pnlFase1.getWidth() - lblLegendaSlide.getWidth())/2), 
									pnlFase1.getY() + pnlFase1.getHeight() + lblLegendaSlide.getHeight());
		super.add(lblLegendaSlide);
		
		//BOTOES DE PROXIMO E ANTERIOR
		int xAnterior = pnlFase1.getX() - TAMANHO_BOTAO - DISTANCIA;
		int xProximo = pnlFase1.getX() + pnlFase1.getWidth() + DISTANCIA;
		int yBotoes = pnlFase1.getY() + ((pnlFase1.getHeight() - TAMANHO_BOTAO) /2);
		
		btnAnterior = new JButton();
		btnAnterior.setSize(TAMANHO_BOTAO, TAMANHO_BOTAO);
		btnAnterior.setLocation(xAnterior, yBotoes);
		btnAnterior.setIcon(iconBtnAnterior);
		btnAnterior.setPressedIcon(iconBtnAnteriorPress);
		btnAnterior.setContentAreaFilled(false);
		btnAnterior.setBorder(null);
		btnAnterior.setFocusable(false);
		btnAnterior.addActionListener(this);
		super.add(btnAnterior);
		
		btnProximo = new JButton();
		btnProximo.setSize(TAMANHO_BOTAO, TAMANHO_BOTAO);
		btnProximo.setLocation(xProximo, yBotoes);
		btnProximo.setIcon(iconBtnProximo);
		btnProximo.setPressedIcon(iconBtnProximoPress);
		btnProximo.setContentAreaFilled(false);
		btnProximo.setBorder(null);
		btnProximo.setFocusable(false);
		btnProximo.addActionListener(this);
		super.add(btnProximo);
		
		//BARRA INFERIOR
		pnlBarraInferior = new JPanel();
		pnlBarraInferior.setSize(LARGURA_FRAME - DISTANCIA * 2, 170);
		pnlBarraInferior.setLocation(DISTANCIA, ALTURA_FRAME - pnlBarraInferior.getHeight() - DISTANCIA);
		pnlBarraInferior.setOpaque(false);
		pnlBarraInferior.setLayout(null);
		
		int alturaBotao = (pnlBarraInferior.getHeight() - DISTANCIA * 3)/2; 
		
		btnJogar = new JButton();
		btnJogar.setSize(150, alturaBotao);
		btnJogar.setLocation(pnlBarraInferior.getWidth() - btnJogar.getWidth() - DISTANCIA, DISTANCIA);
		btnJogar.setIcon(iconBtnJogar);
		btnJogar.setPressedIcon(iconBtnJogarPress);
		btnJogar.setBorder(null);
		btnJogar.setContentAreaFilled(false);
		btnJogar.addActionListener(this);
		pnlBarraInferior.add(btnJogar);
				
		btnVoltar = new JButton ();
		btnVoltar.setSize(btnJogar.getSize());
		btnVoltar.setLocation(btnJogar.getX(), btnJogar.getY() + btnJogar.getHeight() + DISTANCIA);
		btnVoltar.setIcon(iconBtnVoltar);
		btnVoltar.setPressedIcon(iconBtnVoltarPress);
		btnVoltar.setBorder(null);
		btnVoltar.setContentAreaFilled(false);
		btnVoltar.addActionListener(this);
		pnlBarraInferior.add(btnVoltar);
		
		super.add(pnlBarraInferior);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == btnFechar){
			TelaMenuInicial tmi = new TelaMenuInicial();
			tmi.setVisible(true);
			super.dispose();
		}
		
		else if (e.getSource() == btnMinimizar){
			super.setState(JFrame.ICONIFIED);
		}
		
		else if (e.getSource() == btnJogar){
			
			if (indexAtual == 0){
				if (pnlFase1.isDesbloqueada()){
					TelaFaseUm t = new TelaFaseUm(1);
					t.setVisible(true);
					super.dispose();
				} else {
					cm.mostrarMensagem("It is locked D:");
				}	
				
			} else if (indexAtual == 1){
				if (pnlFase2.isDesbloqueada()){
					TelaFaseDois t = new TelaFaseDois(2);
					t.setVisible(true);
					super.dispose();
				} else {
					cm.mostrarMensagem("It is locked D:");
				}
				
			} else if (indexAtual == 2){
				if (pnlFase3.isDesbloqueada()){
					TelaFaseTres t = new TelaFaseTres(3);
					t.setVisible(true);
					super.dispose();
				} else {
					cm.mostrarMensagem("It is locked D:");
				}
				
			}
			
		}
		
		else if (e.getSource() == btnVoltar){
			TelaMenuInicial tmi = new TelaMenuInicial();
			tmi.setVisible(true);
			super.dispose();
		}
		
		else if (e.getSource() == btnAnterior){
			trocarFase(false);
		}  
		
		else if (e.getSource() == btnProximo){
			trocarFase(true);
		} 
		
		else if (e.getSource() == pnlFase1.getBtnDisparador()){
			if (pnlFase1.isDesbloqueada()){
				TelaFaseUm t = new TelaFaseUm(1);
				t.setVisible(true);
				super.dispose();
			} else {
				cm.mostrarMensagem("It is locked D:");
			}			
			
		}
		
		else if (e.getSource() == pnlFase2.getBtnDisparador()){
			if (pnlFase2.isDesbloqueada()){
				TelaFaseDois t = new TelaFaseDois(2);
				t.setVisible(true);
				super.dispose();
			} else {
				cm.mostrarMensagem("It is locked D:");
			}
			
		}
		
		else if (e.getSource() == pnlFase3.getBtnDisparador()){
			if (pnlFase3.isDesbloqueada()){
				TelaFaseTres t = new TelaFaseTres(3);
				t.setVisible(true);
				super.dispose();
			} else {
				cm.mostrarMensagem("It is locked D:");
			}
			
		}
		
	}
	
	private void trocarFase(boolean isDireita){
		
		if (isDireita){
			if (indexAtual == listaFase.size() - 1){
				super.remove(listaFase.get(listaFase.size() - 1));
				indexAtual = 0;
				super.add(listaFase.get(indexAtual));
			} else {
				super.remove(listaFase.get(indexAtual));
				indexAtual += 1;
				super.add(listaFase.get(indexAtual));
			}
			
		} else {
			if (indexAtual == 0){
				super.remove(listaFase.get(0));
				indexAtual = listaFase.size() - 1;
				super.add(listaFase.get(indexAtual));
			} else {
				super.remove(listaFase.get(indexAtual));
				indexAtual -= 1;
				super.add(listaFase.get(indexAtual));
			}
		}
		
		atualizarLegenda();
		super.repaint();
	}
	
	private void atualizarLegenda(){
		if (indexAtual == 0){
			lblLegendaSlide.setIcon(iconLegenda1);
		} else if (indexAtual == 1){
			lblLegendaSlide.setIcon(iconLegenda2);
		} else {
			lblLegendaSlide.setIcon(iconLegenda3);
		}
		
		lblLegendaSlide.repaint();
	}
}
