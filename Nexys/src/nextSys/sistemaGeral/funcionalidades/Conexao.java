package nextSys.sistemaGeral.funcionalidades;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Daniel
 */
public class Conexao {
   
    private Connection conexao;
    
    //faz conexão com o banco de dados
    public Connection conectar (){
        
        String servidor = "jdbc:mysql://localhost:3306/bdNextSys";
        String usuario = "root";
        String senha = "123456";
        String driver = "com.mysql.jdbc.Driver";
        
        try {
            Class.forName(driver);
            this.conexao = DriverManager.getConnection(servidor, usuario, senha);
        } catch (Exception e){
            System.out.println (e.getMessage());
        } finally {
            return this.conexao;
        }
    }
    
} // fim da classe de conexão
