package nextSys.sistemaGeral.funcionalidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Daniel
 */
public class DAOUsuario {
    
    //cadastro de usuario
    public boolean cadastrarUsuario (Usuario u){
        
        Connection con = new Conexao().conectar();
        PreparedStatement ps = null;
        
        try {
            ps = con.prepareStatement(
                    "INSERT INTO tbUsuario (nomeUsuario, sobrenomeUsuario, apelidoUsuario, loginUsuario, senhaUsuario, primeiraVezLogado)"
                    + "VALUES (?,?,?,?,?,?)");
            ps.setString(1, u.getNomeUsuario());
            ps.setString(2, u.getSobrenomeUsuario());
            ps.setString(3, u.getApelidoUsuario());
            ps.setString(4, u.getLoginUsuario());
            ps.setString(5, u.getSenhaUsuario());
            ps.setInt(6, 0);
            
            //executando a query
            ps.executeUpdate();
            
            return true;
            
        } catch (SQLException e){
            //System.out.println (e.getMessage());
            return false;
        } finally {
            //fechando a conexao, nao importa o que aconteca
            try {
                ps.close();
                con.close();
            } catch (SQLException e){
                
            }
            
        }
    }
    
    public boolean editarUsuario(Usuario u){
    	
    	Connection con = new Conexao().conectar();
        PreparedStatement ps = null;
        
        try {
            ps = con.prepareStatement( "UPDATE tbUsuario\n" + 
            						   "SET senhaUsuario = ?\n" + 
            						   "WHERE codUsuario = ?");
            
            ps.setString(1, u.getSenhaUsuario());
            ps.setInt(2, u.getIdUsuario());
            
            //executando a query
            ps.executeUpdate();
            
            return true;
            
        } catch (SQLException e){
            //System.out.println (e.getMessage());
            return false;
        } finally {
            //fechando a conexao, nao importa o que aconteca
            try {
                ps.close();
                con.close();
            } catch (SQLException e){
                
            }
            
        }
    }
    
    public boolean selecionarUsuario (String loginUsuario, String senhaUsuario){
        Connection con = new Conexao().conectar();
        PreparedStatement ps = null;
        
        try{
            
            ps = con.prepareStatement("SELECT loginUsuario, senhaUsuario\n" +
                                      "FROM tbUsuario  \n" +
                                      "WHERE loginUsuario = ? AND senhaUsuario = ?");
            ps.setString(1, loginUsuario);
            ps.setString(2, senhaUsuario);
            
            ResultSet rs = ps.executeQuery();
            
            return rs.next();
            
            
        } catch (SQLException e){
            //erro
            return false;
            //System.out.println(e.getMessage());
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException e){
                
            }
        }
    } 
    
    public int verificaPrimeiraVez(String login){
        Connection con = new Conexao().conectar();
        PreparedStatement ps = null;
        
        try{
            ps = con.prepareStatement("SELECT primeiraVezLogado\n" +
                                      "FROM tbUsuario  \n" +
                                      "WHERE loginUsuario = ?");
            ps.setString(1, login);
            
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getInt("primeiraVezLogado");
            
        } catch (SQLException e){
            //erro
            return 0;
            //System.out.println(e.getMessage());
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException e){
                
            }
        }
    }
    
    public void alterandoPrimeiraVez(String login){
        
        if (verificaPrimeiraVez(login) == 0){
        	Connection con = new Conexao().conectar();
            PreparedStatement ps = null;
            PreparedStatement fases = null;
            PreparedStatement jogos = null;
            
            try{
            	//mudando a vari�vel que diz que o usu�rio nunca logou no sistema
                ps = con.prepareStatement("UPDATE tbUsuario\n" +
                                          "SET primeiraVezLogado = 1\n" +
                                          "WHERE loginUsuario = ?");
                ps.setString(1, login);
                ps.executeUpdate();
                
                int codUsuario = InformacoesUsuarioLogado.getInstance().getUsuario().getIdUsuario();
                
                //fases
                fases = con.prepareStatement("INSERT INTO tbFase (descFase, numEstrelas, numEstrelasCon, numEstrelasReq, codUsuario)\n"+
                							 "VALUES(1, 3, 0, 0, ?)\n" + 
                						   		  ",(2, 3, 0, 3, ?)\n" + 
                						   		  ",(3, 3, 0, 6, ?)");
                fases.setInt(1, codUsuario);
                fases.setInt(2, codUsuario);
                fases.setInt(3, codUsuario);
                fases.executeUpdate();
                
                //jogos
                jogos = con.prepareStatement("INSERT INTO tbJogo (codFase, numIdentificador, isConquistado)\n"
                							+"VALUES ((SELECT codFase FROM tbFase WHERE codUsuario = ? AND descFase = 1),1,0)\n"
                								   +",((SELECT codFase FROM tbFase WHERE codUsuario = ? AND descFase = 1),2,0)\n"
                								   +",((SELECT codFase FROM tbFase WHERE codUsuario = ? AND descFase = 1),3,0)\n"
                								   +",((SELECT codFase FROM tbFase WHERE codUsuario = ? AND descFase = 2),1,0)\n"
                								   +",((SELECT codFase FROM tbFase WHERE codUsuario = ? AND descFase = 2),2,0)\n"
                								   +",((SELECT codFase FROM tbFase WHERE codUsuario = ? AND descFase = 2),3,0)\n"
                								   +",((SELECT codFase FROM tbFase WHERE codUsuario = ? AND descFase = 3),1,0)\n"
                								   +",((SELECT codFase FROM tbFase WHERE codUsuario = ? AND descFase = 3),2,0)\n"
                								   +",((SELECT codFase FROM tbFase WHERE codUsuario = ? AND descFase = 3),3,0)\n");
                jogos.setInt(1, codUsuario);
                jogos.setInt(2, codUsuario);
                jogos.setInt(3, codUsuario);
                jogos.setInt(4, codUsuario);
                jogos.setInt(5, codUsuario);
                jogos.setInt(6, codUsuario);
                jogos.setInt(7, codUsuario);
                jogos.setInt(8, codUsuario);
                jogos.setInt(9, codUsuario);
              
                jogos.executeUpdate();	
                
            } catch (SQLException e){
                //erro
                System.out.println(e.getMessage());
                
            } finally {
                try {
                    ps.close();
                    fases.close();
                    jogos.close();
                    con.close();
                } catch (SQLException e){
                	System.out.println(e.getMessage());
                }
            }
        }
        
        
    }
    
    public Usuario pegandoUsuarioCompleto (String login){
        Connection con = new Conexao().conectar();
        PreparedStatement ps = null;
        
        Usuario u;
        
        try{
            
            u = new Usuario ();
            ps = con.prepareStatement("SELECT *\n" +
                                      "FROM tbUsuario  \n" +
                                      "WHERE loginUsuario = ?");
            ps.setString(1, login);
            
            ResultSet rs = ps.executeQuery();
            rs.next();
            u.setIdUsuario(rs.getInt("codUsuario"));
            u.setNomeUsuario(rs.getString("nomeUsuario"));
            u.setSobrenomeUsuario(rs.getString("sobrenomeUsuario"));
            u.setApelidoUsuario(rs.getString("apelidoUsuario"));
            u.setLoginUsuario(rs.getString("loginUsuario"));
            u.setSenhaUsuario(rs.getString("senhaUsuario"));
            
            return u;
            
        } catch (SQLException e){
            //erro
            System.out.println(e.getMessage());
            return null;
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException e){
                
            }
        }
    }
    
    
}
