package nextSys.sistemaGeral.funcionalidades;

public class Fase {

	private String desFase;
	private int numEstrelas;
	private int numEstrelasConq;
	private int numEstrelasReq;
	private boolean isAtiva;
	
	public Fase(){
		
	}
	
	public String getDesFase() {
		return desFase;
	}
	
	public void setDesFase(String desFase) {
		this.desFase = desFase;
	}

	public int getNumEstrelas() {
		return numEstrelas;
	}

	public void setNumEstrelas(int numEstrelas) {
		this.numEstrelas = numEstrelas;
	}

	public int getNumEstrelasConq() {
		return numEstrelasConq;
	}

	public void setNumEstrelasConq(int numEstrelasConq) {
		this.numEstrelasConq = numEstrelasConq;
	}

	public int getNumEstrelasReq() {
		return numEstrelasReq;
	}

	public void setNumEstrelasReq(int numEstrelasReq) {
		this.numEstrelasReq = numEstrelasReq;
	}

	public boolean isAtiva() {
		return isAtiva;
	}

	public void setAtiva(boolean isAtiva) {
		this.isAtiva = isAtiva;
	}

}
