package nextSys.sistemaGeral.funcionalidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GerenciamentoFases {

	/**retorna quantos jogos ja foram solucionados de uma fase especifica */
    public int progressoFase (int descFase){
    	Connection con = new Conexao().conectar();
    	PreparedStatement ps = null;
    	
    	try{
    		ps = con.prepareStatement("SELECT numEstrelasCon\n" +
    								  "FROM tbFase\n"+
    								  "WHERE codUsuario = ? AND descFase = ?");
    		
    		ps.setInt(1, InformacoesUsuarioLogado.getInstance().getUsuario().getIdUsuario());
    		ps.setInt(2, descFase);
    		
    		ResultSet rs = ps.executeQuery();
    		rs.next();
    		
    		return rs.getInt("numEstrelasCon");
    		
    	} catch (SQLException e){
    		System.out.println(e.getMessage());
    		return 0;
    	} finally {
    		try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
    	}
    	
    }
    
    /**retorna verdadeiro se a operacao for bem sucedida*/
    public boolean progredingoFase(int descFase){
    	Connection con = new Conexao().conectar();
    	PreparedStatement ps2 = null; // NUM ESTRELAS
    	PreparedStatement ps3 = null; // UPDATE 
    	int numEstrelasCon = progressoFase(descFase), numEstrelas;
    	
    	try {
    		
    		ps2 = con.prepareStatement("SELECT numEstrelas\n"+
    								   "FROM tbFase\n" + 
    								   "WHERE descFase = ? AND codUsuario = ?");
    		ps2.setInt(1, descFase);
    		ps2.setInt(2, InformacoesUsuarioLogado.getInstance().getUsuario().getIdUsuario());
    		
    		ResultSet rs2 = ps2.executeQuery();
    		rs2.next();
    		numEstrelas = rs2.getInt("numEstrelas");
    		
    		if (numEstrelas == numEstrelasCon){
    			//NADA FAZ SE ELE J� TIVER CONQUISTADO TUDO 
    		} else {
    			
    			ps3 = con.prepareStatement("UPDATE tbFase\n" + 
    									   "SET numEstrelasCon = ?\n" + 
    									   "WHERE descFase = ? AND codUsuario = ?");
    			ps3.setInt(1, numEstrelasCon + 1);
    			ps3.setInt(2, descFase);
    			ps3.setInt(3, InformacoesUsuarioLogado.getInstance().getUsuario().getIdUsuario());
    			
    			int afetadas = ps3.executeUpdate();
    			if (afetadas == 0){
    				return false; 
    			} else {
    				return true;
    			}

    		}
    		
    		
    	} catch (SQLException e){
    		System.out.println(e.getMessage());
    		System.out.println("Erro no progredingo Fase");
    		return false;
    	} finally {
    		try {
				con.close();
				ps2.close();
				if (ps3 == null){
					
				} else {
					ps3.close();
				}
			} catch (SQLException e) { }
    		
    	}
    	//SE CHEGAR AQUI, ALGO DEU MUITO ERRADO :b
    	return false;
    	
    }
    
    /**retorna verdadeiro se o jogo passado como par�metro ja tiver sido concluido*/
    public boolean isJogoConcluido(int descFase, int numIdentificador){
    	Connection con = new Conexao().conectar();
    	PreparedStatement ps = null;
    	
    	try {
    		
    		ps = con.prepareStatement("SELECT isConquistado\n" + 
    								  "FROM tbJogo\n" +
    								  "INNER JOIN tbFase\n" +
    								  "		ON tbJogo.codFase = tbFase.codFase\n" +
    								  "WHERE numIdentificador = ? AND descFase = ? AND codUsuario = ?");
    		ps.setInt(1, numIdentificador);
    		ps.setInt(2, descFase);
    		ps.setInt(3, InformacoesUsuarioLogado.getInstance().getUsuario().getIdUsuario());
    		
    		ResultSet rs = ps.executeQuery();
    		rs.next();
    		
    		if (rs.getInt("isConquistado") == 0){
    			return false;
    		} else {
    			return true;
    		}
    		
    	} catch (SQLException e){
    		System.out.println(e.getMessage());
    		return false;
    	} finally {
    		try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
    	}
    }
    
    /**o retorno booleano atinge verdadeiro quando uma ou mais colunas forem afetadas*/
    public boolean mudancaStatusJogo (int descFase, int numIdentificador){
    	Connection con = new Conexao().conectar();
    	PreparedStatement ps = null;
    	
    	try{
    		ps = con.prepareStatement("UPDATE tbJogo\n" + 
    								  "INNER JOIN tbFase\n" +
    								  		"ON tbJogo.codFase = tbFase.codFase\n" +
    								  "SET isConquistado = 1\n" + 
    								  "WHERE tbJogo.numIdentificador = ? AND tbFase.descFase = ? AND tbFase.codUsuario = ?");
    		
//    		System.out.println(numIdentificador);
//    		System.out.println(descFase);
//    		System.out.println(InformacoesUsuarioLogado.getInstance().getUsuario().getIdUsuario());
    		
    		ps.setInt(1, numIdentificador);
    		ps.setInt(2, descFase);
    		ps.setInt(3, InformacoesUsuarioLogado.getInstance().getUsuario().getIdUsuario());
    		
    		int afetado = ps.executeUpdate();
    		if (afetado == 0){
    			return false;
    		} else {
    			return true;
    		}
    		
    	} catch (SQLException e){
    		System.out.println(e.getMessage());
    		return false;
    	} finally {
    		try {
    			con.close();
    			ps.close();
    		} catch (SQLException e){
    			System.out.println(e.getMessage());
    		}
    	}
    }
    
    /**metodo responsavel por dizer se a fase passada por parametro esta bloqueada ou nao*/
    public boolean isFaseDesbloqueada(int descFase){
    	Connection con = new Conexao().conectar();
    	PreparedStatement ps = null;
    	
    	try {
    		
    		ps = con.prepareStatement("SELECT numEstrelasReq\n" + 
    								  "FROM tbFase\n" + 
    								  "WHERE codUsuario = ? AND descFase = ?");
    		
    		ps.setInt(1, InformacoesUsuarioLogado.getInstance().getUsuario().getIdUsuario());
    		ps.setInt(2, descFase);
    		
    		ResultSet rs = ps.executeQuery();
    		rs.next();
    		
    		if(rs.getInt("numEstrelasReq") <= numEstrelasConGeral()){
    			return true;
    		} else {
    			return false;
    		}
    		
    	} catch (SQLException e){
    		System.out.println(e.getMessage());
    		return false;
    	} finally {
    		try {
				ps.close();
				con.close();
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
    	}
    }
    
    public int numEstrelasConGeral(){
    	Connection con = new Conexao().conectar();
        PreparedStatement ps = null;
        
        try{
            ps = con.prepareStatement("SELECT SUM(numEstrelasCon)\n" +
                                      "FROM tbFase \n" +
                                      "WHERE codUsuario = ?");
            ps.setInt(1, InformacoesUsuarioLogado.getInstance().getUsuario().getIdUsuario());
            
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getInt(1);
            
        } catch (SQLException e){
            //erro
            return -1;
            //System.out.println(e.getMessage());
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException e){
                
            }
        }
    }
    
    /**retorna verdadeiro se os status forem alterados*/
    public boolean terminoJogo(int descFase, int numIdentificador){
    	
    	if (!isJogoConcluido(descFase, numIdentificador)){
    		if (mudancaStatusJogo(descFase, numIdentificador) && progredingoFase(descFase)){
        		return true;
        	} else {
        		return false;
        	}
    	}
    	
    	return false;
    	
    }
    
}

