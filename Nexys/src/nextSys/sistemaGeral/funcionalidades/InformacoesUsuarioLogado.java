package nextSys.sistemaGeral.funcionalidades;

/**
 *
 * @author Daniel
 */

//controla que usuario que esta logado no sistema
public class InformacoesUsuarioLogado {

    private static InformacoesUsuarioLogado info;
    
    private Usuario u; //o usuario logado no sistema
    public Usuario getUsuario(){
        return this.u;
    }
    public void setUsuario(Usuario u){
        this.u = u;
    }
    
    //por ser uma classe singleton˛
    private InformacoesUsuarioLogado (){
        
    }
    //˛
    public static InformacoesUsuarioLogado getInstance (){
        if (info == null){
            info = new InformacoesUsuarioLogado();
        } 
        
        return info;
    } 
    
    
}
