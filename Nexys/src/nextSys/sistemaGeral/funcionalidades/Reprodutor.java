package nextSys.sistemaGeral.funcionalidades;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.UnsupportedAudioFileException;


/**
  * 
  * @author Daniel 
  * 
  **/
public class Reprodutor {

    private static Mixer mixer = null;
    private List<Clip> listaClip; //para n�o se limitar a apenas um som
    private float volume;
    
    //controle de volume
    //FloatControl controle = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
    //controle.setValue((float)-10);

	public Reprodutor(){
    	listaClip = new ArrayList<Clip>();
    	volume = 0f;
    }
    
    public Reprodutor (float volume){
    	this.volume = volume;
    	listaClip = new ArrayList<Clip>();
    }
    
    public void setVolume(float volume) {
		this.volume = volume;
	}
        
    /**Somente utilizado pela presente classe. Executado como aux�lio em outros m�todos*/
    private void carregar(URL urlSom){
    	Clip clip = null;
    	
    	Mixer.Info [] mixInfos = AudioSystem.getMixerInfo();
        mixer = AudioSystem.getMixer(mixInfos [0]);
        
        DataLine.Info dataInfo = new DataLine.Info (Clip.class, null);
        try { 
        	clip = (Clip)mixer.getLine(dataInfo);
        	listaClip.add(clip);
        } catch (LineUnavailableException lue){ 
        	lue.printStackTrace(); 
        }
        
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(urlSom);
            clip.open(audioStream);
        } 
        catch (LineUnavailableException lue){ lue.printStackTrace(); }
        catch (UnsupportedAudioFileException uae) {uae.printStackTrace();}
        catch (IOException ioe) { ioe.printStackTrace();}
        
    }
    
    /**Sobrecarga para carregar arquivos do tipo File*/
    private void carregar(File arquivoSom){
    	//pegando o caminho do arquivo mandado por par�metro
    	URL urlSom = null;
    	Clip clip = null;
    	try {
			urlSom = arquivoSom.toURL();
		} catch (MalformedURLException e) { e.printStackTrace(); } 
    	
    	Mixer.Info [] mixInfos = AudioSystem.getMixerInfo();
        mixer = AudioSystem.getMixer(mixInfos [0]);
        
        DataLine.Info dataInfo = new DataLine.Info (Clip.class, null);
        try { 
        	clip = (Clip)mixer.getLine(dataInfo);
        	listaClip.add(clip);
        } catch (LineUnavailableException lue){ 
        	lue.printStackTrace(); 
        }
        
        try {
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(urlSom);
            clip.open(audioStream);
        } 
        catch (LineUnavailableException lue){ lue.printStackTrace(); }
        catch (UnsupportedAudioFileException uae) {uae.printStackTrace();}
        catch (IOException ioe) { ioe.printStackTrace();}
    }
        
    /**Reprodu��o �nica atrav�s de uma URl*/
    public void reproduzirSom (URL urlSom){
    	carregar(urlSom);
    	
    	Clip clip = listaClip.get(listaClip.size() - 1);
    	FloatControl controle = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
        controle.setValue(volume);//setando o volume
        
    	clip.start();
    	
        do {
            try {Thread.sleep(10);}
            catch (InterruptedException ie) {ie.printStackTrace();}
        } while (clip.isActive());
    }

/**Reprodu��o �nica atrav�s de um File*/
    public void reproduzirSom(File arquivoSom){
    	carregar(arquivoSom);
    	
    	Clip clip = listaClip.get(listaClip.size() - 1);
    	FloatControl controle = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
        controle.setValue(volume);
        
    	clip.start();
        
        do {
            try {Thread.sleep(10);}
            catch (InterruptedException ie) {ie.printStackTrace();}
        } while (clip.isActive());
    	
    }
    
    /**Reprodu��o em loop atrav�s de uma URL*/
    public void reproduzirLoop(URL urlSom){
    	carregar(urlSom);
         
    	Clip clip = listaClip.get(listaClip.size() - 1);
    	FloatControl controle = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
        controle.setValue(volume);
        
        clip.start();
        clip.loop(clip.LOOP_CONTINUOUSLY);
        
        do {
            try {Thread.sleep(10);}
            catch (InterruptedException ie) {ie.printStackTrace();}
        } while (clip.isActive());
    }
    
    /**Reprodu��o em loop atrav�s de um File*/
    public void reproduzirLoop(File arquivoSom){
    	    	
    	carregar(arquivoSom);
    	
    	Clip clip = listaClip.get(listaClip.size() - 1);
    	FloatControl controle = (FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN);
        controle.setValue(volume);
        
        clip.start();
        clip.loop(clip.LOOP_CONTINUOUSLY);
        
//        do {
//            try {}
//            catch (InterruptedException ie) {ie.printStackTrace();}
//        } while (clip.isActive());
    }
    
    
}//fim da classe do Reprodutor

