package nextSys.sistemaGeral.funcionalidades;

/**
 *
 * @author Daniel
 */
public class Usuario {
    
    private int idUsuario;
    private String nomeUsuario;
    private String sobrenomeUsuario;
    private String apelidoUsuario;
    private String loginUsuario;
    private String senhaUsuario;

    /**
     * @return the idUsuario
     */
    public int getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the nomeUsuario
     */
    public String getNomeUsuario() {
        return nomeUsuario;
    }

    /**
     * @param nomeUsuario the nomeUsuario to set
     */
    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    /**
     * @return the sobrenomeUsuario
     */
    public String getSobrenomeUsuario() {
        return sobrenomeUsuario;
    }

    /**
     * @param sobrenomeUsuario the sobrenomeUsuario to set
     */
    public void setSobrenomeUsuario(String sobrenomeUsuario) {
        this.sobrenomeUsuario = sobrenomeUsuario;
    }

    /**
     * @return the apelidoUsuario
     */
    public String getApelidoUsuario() {
        return apelidoUsuario;
    }

    /**
     * @param apelidoUsuario the apelidoUsuario to set
     */
    public void setApelidoUsuario(String apelidoUsuario) {
        this.apelidoUsuario = apelidoUsuario;
    }

    /**
     * @return the loginUsuario
     */
    public String getLoginUsuario() {
        return loginUsuario;
    }

    /**
     * @param loginUsuario the loginUsuario to set
     */
    public void setLoginUsuario(String loginUsuario) {
        this.loginUsuario = loginUsuario;
    }

    /**
     * @return the senhaUsuario
     */
    public String getSenhaUsuario() {
        return senhaUsuario;
    }

    /**
     * @param senhaUsuario the senhaUsuario to set
     */
    public void setSenhaUsuario(String senhaUsuario) {
        this.senhaUsuario = senhaUsuario;
    }
    
    
}
