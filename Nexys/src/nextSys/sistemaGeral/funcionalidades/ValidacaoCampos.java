﻿package nextSys.sistemaGeral.funcionalidades;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Daniel
 */
public class ValidacaoCampos {
 
    // para que este campo seja válido:
    // - o texto deve conter mais de um dígito
    public boolean validarNome(String textoNome){
        if (textoNome.length() > 0){
            return true;
        } else {
            return false;
        }
    }
    
    //para que esse campo seja válido:
    // - o texto deve conter mais de um digito
    public boolean validarSobrenome(String textoSobrenome){
        if (textoSobrenome.length() > 0){
            return true;
        } else {
            return false;
        }
        
    }
    
    // para que este campo seja válido:
    // - o texto deve ser maior do que 6 digitos
    // - o texto não pode ser um já cadastrado
    // --------------------------------------- FAZER
    public boolean validarLogin(String textoLogin){
        if (textoLogin.length() >= 6 && !verificarExistencia(textoLogin)){
            return true;
        } else {
            return false;
        }
    }
    
    // para que este campo seja válido:
    // - o texto deve conter mais de 6 digitos;
    // - o texto não pode ser o nome da pessoa, por ser muito fraca
    public boolean validarSenha(String textoSenha, String textoNome){
        if (textoSenha.length() >= 6 && !textoSenha.equals(textoNome)){
            return true;
        } else{
            return false;
        }
    }
    
    // para que este campo seja válido:
    // - o texto do campo senha precisa ser igual ao texto desse campo 
    public boolean validarRepeticaoSenha(String textoRepeticaoSenha, String senha){
        if (textoRepeticaoSenha.equals(senha)){
            return true;
        } else {
            return false;
        }
    }
    
    public boolean verificarExistencia (String login){
        Connection con = new Conexao().conectar();
        PreparedStatement ps = null;
        
        try{
            
            ps = con.prepareStatement("SELECT loginUsuario \n" +
                                      "FROM tbUsuario \n" +
                                      "WHERE loginUsuario = ?");
            ps.setString(1, login);
            
            ResultSet rs = ps.executeQuery();
            
            return rs.next();
            
            
        } catch (SQLException e){
            //erro
            return false;
            
        } finally {
            try {
                ps.close();
                con.close();
            } catch (SQLException e){
                
            }
        }
    }
        
} // fim da classe de validação de campos
